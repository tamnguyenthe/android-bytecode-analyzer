# Android Bytecode Analyzer

## Introduction
**Android Bytecode Analyzer** (ABA), an Android bytecode analyzer for reverse engineering Android bytecode, and learning API usage patterns. Not only is ABA one of the first tools to reverse engineer the bytecode of Android apps, the tool also does it at scale. For example, it is used to analyze the bytecode of over 200,000 apps from Google Play Store, with 60 millions methods and nearly 1.8 billions bytecode instructions. ABA also has a component to train API usage models (based on Hidden Markov Models) from the extracted data.  The models are lightweight and easy to use for downstream applications. For more information about this tool, please see [this paper](https://dl.acm.org/doi/10.1145/2884781.2884873) below. 

The corresponding paper for this tool is pubished at: 

Tam The Nguyen; Hung Viet Pham; Phong Minh Vu; Tung Thanh Nguyen

Learning API Usages from Bytecode: A Statistical Approach

2016 IEEE/ACM 38th International Conference on Software Engineering (ICSE)

https://ieeexplore.ieee.org/document/7886922


## Installation
1. Clone this project to your local machine
2. Open the project in Intellij IDEA

## Contact

Please contact me at tamtng6@gmail.com if you have any questions.
