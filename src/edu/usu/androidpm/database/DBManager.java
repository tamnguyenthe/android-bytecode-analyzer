package edu.usu.androidpm.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.Document;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;

public class DBManager {
	private HashMap<String, Integer> objectIDMap;
	private HashMap<String, Integer> methodIDMap;
	
	private MongoClient mongoClient;
	private MongoDatabase mongoDatabase;
	private MongoCollection<Document> sequencesCollection;
	private MongoCollection<Document> objectIDCollection;
	private MongoCollection<Document> methodIDCollection;
	
	public DBManager() {
		objectIDMap = new HashMap<>();
		methodIDMap = new HashMap<>();
		
		mongoClient = new MongoClient();
		mongoDatabase = mongoClient.getDatabase("androidpm");
		//mongoDatabase.createCollection("sequences");
		//mongoDatabase.createCollection("objectids");
		//mongoDatabase.createCollection("methodids");
		objectIDCollection = mongoDatabase.getCollection("objectids");
		methodIDCollection = mongoDatabase.getCollection("methodids");
		sequencesCollection = mongoDatabase.getCollection("sequences");
		sequencesCollection.dropCollection();
		objectIDCollection.dropCollection();
		methodIDCollection.dropCollection();
		sequencesCollection.createIndex(new BasicDBObject("objectID", 1).append("seqHash", 1));
		
	}
	
	public void saveSequence(String objectName, List<String> methodSequence) {
		if (!objectIDMap.containsKey(objectName)) objectIDMap.put(objectName, objectIDMap.size());
		int objectID = objectIDMap.get(objectName);
		BasicDBList methodIdSequence = new BasicDBList();
		for (String methodName: methodSequence) {
			if (!methodIDMap.containsKey(methodName)) methodIDMap.put(methodName, methodIDMap.size());
			methodIdSequence.add(methodIDMap.get(methodName));
		}
		BasicDBObject filter = new BasicDBObject("objectID", objectID).append("seqHash", methodIdSequence.hashCode());
		BasicDBObject update = new BasicDBObject("$inc", new BasicDBObject("count", 1)).append("$setOnInsert", new BasicDBObject("sequence", methodIdSequence));
		UpdateOptions updateOptions = new UpdateOptions().upsert(true);
		sequencesCollection.updateOne(filter, update, updateOptions);
	}
	
	public void saveNames() {
		//System.out.println(objectIDMap);
		//System.out.println(methodIDMap);
		List<Document> documents = new ArrayList<>();
		for (Map.Entry<String, Integer> entry: objectIDMap.entrySet()) {
			documents.add(new Document("objectID", entry.getValue()).append("objectName", entry.getKey()));
		}
		objectIDCollection.insertMany(documents);
		documents.clear();
		for (Map.Entry<String, Integer> entry: methodIDMap.entrySet()) {
			documents.add(new Document("methodID", entry.getValue()).append("methodName", entry.getKey()));
		}
		methodIDCollection.insertMany(documents);
	}
	
}
