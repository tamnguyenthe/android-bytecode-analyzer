package edu.usu.androidpm.database;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.Pair;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableList;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import edu.usu.androidpm.util.Constants;

public class DatabaseMerger {

	private BiMap<Integer, String> objectsBiMap; 							// object ID map
	private BiMap<Integer, String> methodNamesBiMap; 						// method ID map
	private BiMap<Integer, ImmutableList<Integer>> multipleObjectsBiMap;	// object set ID map
	private File directory;													// database directory
	private File destDirectory;												// final database directory
	private Set<String> invalidObjectTypeNames;								// invalid object types
	private Set<String> invalidMethodNames;									// invalid method names
	private BiMap<Integer, String> finalObjectsBiMap;						// final object ID map
	private BiMap<Integer, ImmutableList<Integer>> finalMultipleObjectsBiMap; // final object set ID map

	public BiMap<Integer, String> getObjectsBiMap() {
		return objectsBiMap;
	}

	public BiMap<Integer, String> getMethodNamesBiMap() {
		return methodNamesBiMap;
	}

	public BiMap<Integer, ImmutableList<Integer>> getMultipleObjectsBiMap() {
		return multipleObjectsBiMap;
	}

	public File getDirectory() {
		return directory;
	}
	
	public Set<String> getInvalidObjectTypeNames() {
		return invalidObjectTypeNames;
	}

	public Set<String> getInvalidMethodNames() {
		return invalidMethodNames;
	}

	
	// OK
	public DatabaseMerger() throws Exception {
		// initialize database directory
		directory = new File("SequenceDB");
		if (!directory.exists()) System.out.println("There is a problem when locating data directory");

		// initialized final database directory
		destDirectory = new File(directory, "DB");
		if (!destDirectory.exists()) destDirectory.mkdirs();
		FileUtils.cleanDirectory(destDirectory);

		// initialize the final map folder
		File finalMapFolder = new File(directory, "Final");
		if (!finalMapFolder.exists()) System.out.println("There is a problem when locating final maps folder");

		String[] entries;
		// load the object id map
		objectsBiMap = HashBiMap.create();
		CSVReader objectsCsvReader = new CSVReader(new FileReader(new File(finalMapFolder, "Objects.csv")), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		while ((entries = objectsCsvReader.readNext()) != null) {
			objectsBiMap.put(Integer.valueOf(entries[1]), entries[0]);
		}
		objectsCsvReader.close();

		// load the method name id map
		methodNamesBiMap = HashBiMap.create();
		CSVReader methodNamesCsvReader = new CSVReader(new FileReader(new File(finalMapFolder, "MethodNames.csv")), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		while ((entries = methodNamesCsvReader.readNext()) != null) {
			methodNamesBiMap.put(Integer.valueOf(entries[1]), entries[0]);
		}
		methodNamesCsvReader.close();

		// load the multiple object id map
		ImmutableList<Integer> multipleObjectsIDs;
		multipleObjectsBiMap = HashBiMap.create();
		CSVReader multipleObjectsCsvReader = new CSVReader(new FileReader(new File(finalMapFolder, "MultipleObjects.csv")), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		while ((entries = multipleObjectsCsvReader.readNext()) != null) {
			multipleObjectsIDs = getListFromString(entries[0]);
			multipleObjectsBiMap.put(Integer.valueOf(entries[1]), multipleObjectsIDs);
		}
		multipleObjectsCsvReader.close();

		//create invalid object types
		setInvalidObjectTypeNames();
		setInvalidMethodNames();
	}
	
	// OK
	private ImmutableList<Integer> getListFromString(String strArr) {
		String[] parts = strArr.substring(1, strArr.length()-1).split(", ");
		List<Integer> result = new ArrayList<>(parts.length);
		for (int i = 0; i < parts.length; i++) result.add(Integer.valueOf(parts[i]));
		return ImmutableList.copyOf(result);
	}
	
	// OK
	private void setInvalidObjectTypeNames() {
		String alphabet = "abcdefghijklmnopqrstuvwxyz";
		String capAlphabet = alphabet.toUpperCase();

		invalidObjectTypeNames = new HashSet<>();

		for (int i = 0; i < alphabet.length(); i++) {
			invalidObjectTypeNames.add(String.valueOf(alphabet.charAt(i)));
			invalidObjectTypeNames.add(String.valueOf(capAlphabet.charAt(i)));

			for (int j = 0; j < alphabet.length(); j++) {
				invalidObjectTypeNames.add(String.valueOf(alphabet.charAt(i)) + String.valueOf(alphabet.charAt(j)));
				for (int k  = 0; k < alphabet.length(); k++) {
					invalidObjectTypeNames.add(String.valueOf(alphabet.charAt(i)) + String.valueOf(alphabet.charAt(j)) + alphabet.charAt(k));			
				}
			}
		}

		//add some rare invalid names 
		invalidObjectTypeNames.add("DCC");
		invalidObjectTypeNames.add("GHH");
		invalidObjectTypeNames.add("Hop");
	}

	// OK
	private boolean checkObjectType(String objectType) {
		String[] parts = objectType.split("[/$;]");
		if (parts[parts.length-1].length() <= 2) return false;
		if (parts[parts.length-1].length() > 50) return false;
		if (invalidObjectTypeNames.contains(parts[parts.length-1])) return false;
		return true;
	}

	// OK
	private boolean checkMultipleObjectType(ImmutableList<Integer> value) {
		for (Integer objectTypeID: value)
			if (!finalObjectsBiMap.containsKey(objectTypeID)) return false;
		return true;
	}
	
	// OK
	private void setInvalidMethodNames() {
		String alphabet = "abcdefghijklmnopqrstuvwxyz";
		String capAlphabet = alphabet.toUpperCase();
		invalidMethodNames = new HashSet<>();
		
		for (int i = 0; i < alphabet.length(); i++) {
			invalidMethodNames.add(String.valueOf(alphabet.charAt(i)));
			invalidMethodNames.add(String.valueOf(capAlphabet.charAt(i)));
			
			for (int j = 0; j < alphabet.length(); j++) {
				invalidMethodNames.add(String.valueOf(alphabet.charAt(i)) + String.valueOf(alphabet.charAt(j)));
				invalidMethodNames.add(String.valueOf(alphabet.charAt(i)) + String.valueOf(capAlphabet.charAt(j)));
				invalidMethodNames.add(String.valueOf(capAlphabet.charAt(i)) + String.valueOf(capAlphabet.charAt(j)));
				invalidMethodNames.add(String.valueOf(capAlphabet.charAt(i)) + String.valueOf(alphabet.charAt(j)));
			}
		}
		
		// there are still some 2-character valid names
		invalidMethodNames.remove("go");
		invalidMethodNames.remove("of");
		invalidMethodNames.remove("on");
	}
	
	// OK
	private boolean needToRemoveMethod(String methodString) {
		if (methodString.startsWith("L")) return false;
		return true;
	}
	
	// OK
	private boolean checkMethodName(String methodString) {
		String[] parts = methodString.split("(->)|\\(|\\)");
		if (parts[1].length() < 2) return false;
		if (invalidMethodNames.contains(parts[1])) return false;
		return true;
	}
	
	// OK
	public void filterAndSaveObjectIDs() throws Exception {
		//filter all object types that the names are too short !
		//in both object map and multiple object map
		finalObjectsBiMap = HashBiMap.create();
		finalMultipleObjectsBiMap = HashBiMap.create();

		for (Map.Entry<Integer, String> entry: objectsBiMap.entrySet()) {
			if (checkObjectType(entry.getValue())) 
				finalObjectsBiMap.put(entry.getKey(), entry.getValue());
			//			else 
			//				System.out.println(entry.getValue());
		}

		for (Map.Entry<Integer, ImmutableList<Integer>> entry: multipleObjectsBiMap.entrySet()) {
			if (checkMultipleObjectType(entry.getValue()))
				finalMultipleObjectsBiMap.put(entry.getKey(), entry.getValue());
			//			else System.out.println(entry.getValue());
		}

		//		System.out.println(finalObjectsBiMap.size());
		//		System.out.println(finalMultipleObjectsBiMap.size());
		//save final maps
		String[] entries = new String[2];
		//save object names map
		CSVWriter objectsCsvWriter = new CSVWriter(new FileWriter(new File(destDirectory, "Objects.csv")), ',', '"');
		for (Map.Entry<Integer, String> entry: finalObjectsBiMap.entrySet()) {
			entries[0] = entry.getValue();
			entries[1] = Integer.toString(entry.getKey());
			objectsCsvWriter.writeNext(entries);
		}
		objectsCsvWriter.close();

		//save method names map
		CSVWriter methodsCsvWriter = new CSVWriter(new FileWriter(new File(destDirectory, "MethodNames.csv")), ',', '"');
		for (Map.Entry<Integer, String> entry: methodNamesBiMap.entrySet()) {
			entries[0] = entry.getValue();
			entries[1] = Integer.toString(entry.getKey()); 
			methodsCsvWriter.writeNext(entries);
		}
		methodsCsvWriter.close();

		//save multiple objects id map
		CSVWriter multipleObjectsCsvWriter = new CSVWriter(new FileWriter(new File(destDirectory, "MultipleObjects.csv")), ',', '"');

		for (Map.Entry<Integer, ImmutableList<Integer>> entry: finalMultipleObjectsBiMap.entrySet()) {
			entries[0] = entry.getValue().toString();
			entries[1] = Integer.toString(entry.getKey());
			multipleObjectsCsvWriter.writeNext(entries);
		}
		multipleObjectsCsvWriter.close();
	}
	
	// OK
	public void mergeDatasets() throws Exception {
		// string array for csv writers
		String[] entries = new String[2];

		// single objects
		List<File> singleObjectFolders = new ArrayList<>();
		int index = 0;
		File tempFile = new File(directory, String.valueOf(index));
		while (tempFile.exists()) {
			singleObjectFolders.add(new File(tempFile, "SingleObjectUsages"));
			index++;
			tempFile = new File(directory, String.valueOf(index));
		}

		// final single object usages
		File finalObjectUsagesFolder = new File(destDirectory, "SingleObjectUsages");
		if (!finalObjectUsagesFolder.exists()) finalObjectUsagesFolder.mkdirs();
		FileUtils.cleanDirectory(finalObjectUsagesFolder);

		// merge
		List<Pair<ImmutableList<Integer>, Integer>> sequences;
//		List<Integer> newMethodSequence;
//		ImmutableList<Integer> newMethodSequenceIList;
		boolean valid;
		Map<ImmutableList<Integer>, Integer> finalSequences;
		for (Map.Entry<Integer, String> entry: finalObjectsBiMap.entrySet()) {
			finalSequences = new HashMap<>();
			for (File path: singleObjectFolders) {
				sequences = loadDatasetForObject(entry.getKey(), path.getAbsolutePath());
				if (sequences != null) {
					for (Pair<ImmutableList<Integer>, Integer> pair: sequences) {
//						newMethodSequence = new ArrayList<>();
						valid = true;
						for (Integer methodID: pair.getKey()) {
							if (!needToRemoveMethod(methodNamesBiMap.get(methodID))) {
								if (!checkMethodName(methodNamesBiMap.get(methodID))) valid = false;
//								newMethodSequence.add(methodID);
							}
						}
						if (!valid) continue;
//						newMethodSequenceIList = ImmutableList.copyOf(newMethodSequence);
						Integer currentCount = finalSequences.get(pair.getLeft());
						if (currentCount == null) finalSequences.put(pair.getLeft(), pair.getRight());
						else finalSequences.put(pair.getLeft(), pair.getRight() + currentCount);
					}
				}
			}
			CSVWriter singleObjectUsageCsvWriter = new CSVWriter(new FileWriter(finalObjectUsagesFolder.getAbsolutePath() + File.separator + entry.getKey() + ".csv"), ',', '"');
			for (Map.Entry<ImmutableList<Integer>, Integer> entry2: finalSequences.entrySet()) {
				entries[0] = entry2.getKey().toString();
				entries[1] = entry2.getValue().toString();
				singleObjectUsageCsvWriter.writeNext(entries);
			}
			singleObjectUsageCsvWriter.close();
		}


		// multiple objects
		List<File> objectSetFolders = new ArrayList<>();
		index = 0;
		tempFile = new File(directory, String.valueOf(index));
		while (tempFile.exists()) {
			objectSetFolders.add(new File(tempFile, "MultipleObjectsUsages"));
			index++;
			tempFile = new File(directory, String.valueOf(index));
		}

		// final single object usages
		File finalObjectSetFolder = new File(destDirectory, "MultipleObjectsUsages");
		if (!finalObjectSetFolder.exists()) finalObjectSetFolder.mkdirs();
		FileUtils.cleanDirectory(finalObjectSetFolder);

		for (Map.Entry<Integer, ImmutableList<Integer>> entry: finalMultipleObjectsBiMap.entrySet()) {
			finalSequences = new HashMap<>();
			for (File path: objectSetFolders) {
				sequences = loadDatasetForObject(entry.getKey(), path.getAbsolutePath());
				if (sequences != null) {
					for (Pair<ImmutableList<Integer>, Integer> pair: sequences) {
//						newMethodSequence = new ArrayList<>();
						valid = true;
						for (Integer methodID: pair.getKey()) {
							if (!needToRemoveMethod(methodNamesBiMap.get(methodID))) {
								if (!checkMethodName(methodNamesBiMap.get(methodID))) valid = false;
//								newMethodSequence.add(methodID);
							}
						}
						if (!valid) continue;
//						newMethodSequenceIList = ImmutableList.copyOf(newMethodSequence);
						Integer currentCount = finalSequences.get(pair.getLeft());
						if (currentCount == null) finalSequences.put(pair.getLeft(), pair.getRight());
						else finalSequences.put(pair.getLeft(), pair.getRight() + currentCount);
					}
				}
			}

			CSVWriter multipleObjectsUsageCsvWriter = new CSVWriter(new FileWriter(finalObjectSetFolder.getAbsolutePath() + File.separator + entry.getKey() + ".csv"), ',', '"');
			for (Map.Entry<ImmutableList<Integer>, Integer> entry2: finalSequences.entrySet()) {
				entries[0] = entry2.getKey().toString();
				entries[1] = entry2.getValue().toString();
				multipleObjectsUsageCsvWriter.writeNext(entries);
			}
			multipleObjectsUsageCsvWriter.close();
		}

	}
	
	// OK
	List<Pair<ImmutableList<Integer>, Integer>> loadDatasetForObject(int objectID, String path) throws Exception {
		String filePath = path + File.separator + objectID + ".csv";
		File file  = new File(filePath);
		if (!file.exists()) return null;
		List<Pair<ImmutableList<Integer>, Integer>> dataset = new ArrayList<>();
		CSVReader singleObjectUsageCsvReader = new CSVReader(new FileReader(file), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		String[] entries;
		ImmutableList<Integer> sequence;
		Integer count;

		while ((entries = singleObjectUsageCsvReader.readNext()) != null) {
			sequence = getListFromString(entries[0]);
			count = Integer.valueOf(entries[1]);
			dataset.add(Pair.of(sequence, count));
		}
		singleObjectUsageCsvReader.close();
		return dataset;
	}

	// OK
	public static void main(String[] args) throws Exception {
		DatabaseMerger databaseMerger = new DatabaseMerger();
		System.out.println("number of objects: " + databaseMerger.getObjectsBiMap().size());
		System.out.println("number of object sets: " + databaseMerger.getMultipleObjectsBiMap().size());
		System.out.println("number of methods: " + databaseMerger.getMethodNamesBiMap().size());

		databaseMerger.filterAndSaveObjectIDs();

		System.out.println("final number of objects: " + databaseMerger.finalObjectsBiMap.size());
		System.out.println("final number of object sets: " + databaseMerger.finalMultipleObjectsBiMap.size());
		databaseMerger.mergeDatasets();
	}
}
