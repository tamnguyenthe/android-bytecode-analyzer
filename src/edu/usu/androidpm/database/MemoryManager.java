package edu.usu.androidpm.database;

import gnu.trove.impl.Constants;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TObjectIntHashMap;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.google.common.collect.ImmutableList;
import com.opencsv.CSVWriter;

import cern.colt.list.IntArrayList;
import cern.colt.map.AbstractIntObjectMap;
import cern.colt.map.OpenIntObjectHashMap;

public class MemoryManager {
	private static final int TYPE_INIT_SIZE = 20000;
	private static final int MUL_OBJ_INIT_SIZE = 40000;
	private static final int METHODNAME_INIT_SIZE = 60000;
	private static final int SEQUENCE_INIT_SIZE = 128;
	private static final float MIN_LOAD_FACTOR = 0.25f;
	private static final float MAX_LOAD_FACTOR = 0.5f;
	private TObjectIntMap<String> typeMap;
	private TObjectIntMap<String> methodNameMap;
	private TObjectIntMap<ImmutableList<Integer>> mulObjMap;
	
	private AbstractIntObjectMap usageMap;
	private AbstractIntObjectMap usageMulObjMap;
	
	private File directory;

	public MemoryManager() {
		typeMap = new TObjectIntHashMap<>(TYPE_INIT_SIZE);
		methodNameMap = new TObjectIntHashMap<>(METHODNAME_INIT_SIZE);
		mulObjMap = new TObjectIntHashMap<>(MUL_OBJ_INIT_SIZE);
		usageMap = new OpenIntObjectHashMap(MUL_OBJ_INIT_SIZE, MIN_LOAD_FACTOR, MAX_LOAD_FACTOR);
		usageMulObjMap = new OpenIntObjectHashMap(MUL_OBJ_INIT_SIZE, MIN_LOAD_FACTOR, MAX_LOAD_FACTOR);
		
		directory = new File("SequenceDB");
		if (!directory.exists()) directory.mkdirs();
		try {
			FileUtils.cleanDirectory(directory);
		} catch (IOException e) {
			e.printStackTrace();
		}
		/*
		String[] entries;
		CSVReader objectsCsvReader = new CSVReader(new FileReader("Objects.csv"), edu.usu.androidpm.util.Constants.COMMA_CHAR, edu.usu.androidpm.util.Constants.QUOTE_CHAR);
		while ((entries = objectsCsvReader.readNext()) != null) {
			typeMap.put(entries[0], Integer.parseInt(entries[1]));
		}
		objectsCsvReader.close();
		
		CSVReader methodNamesCsvReader = new CSVReader(new FileReader("MethodNames.csv"), edu.usu.androidpm.util.Constants.COMMA_CHAR, edu.usu.androidpm.util.Constants.QUOTE_CHAR);
		while ((entries = methodNamesCsvReader.readNext()) != null) {
			methodNameMap.put(entries[0], Integer.parseInt(entries[1]));
		}
		methodNamesCsvReader.close();
		
		ImmutableList<Integer> multipleObjectsIDs;
		CSVReader multipleObjectsCsvReader = new CSVReader(new FileReader("MultipleObjects.csv"), edu.usu.androidpm.util.Constants.COMMA_CHAR, edu.usu.androidpm.util.Constants.QUOTE_CHAR);
		while ((entries = multipleObjectsCsvReader.readNext()) != null) {
			multipleObjectsIDs = getListFromString(entries[0]);
			mulObjMap.put(multipleObjectsIDs, Integer.parseInt(entries[1]));
		}
		multipleObjectsCsvReader.close();
		*/
		
	}
	
	/*
	private ImmutableList<Integer> getListFromString(String strArr) {
		String[] parts = strArr.substring(1, strArr.length()-1).split(", ");
		List<Integer> result = new ArrayList<>(parts.length);
		for (int i = 0; i < parts.length; i++) result.add(Integer.valueOf(parts[i]));
		return ImmutableList.copyOf(result);
	}
	*/

	public void put(String objType, List<String> sequence) {
		if (!typeMap.containsKey(objType)) typeMap.put(objType, typeMap.size()+1);
		int objectTypeID = typeMap.get(objType);
		if (!usageMap.containsKey(objectTypeID)) usageMap.put(objectTypeID, new TObjectIntHashMap<ImmutableList<Integer>>(SEQUENCE_INIT_SIZE, MAX_LOAD_FACTOR));
		
		String methodName;
		List<Integer> methodIDSequence = new ArrayList<>();
		for (int i = 0; i < sequence.size();i++) {
			methodName = sequence.get(i);
			if (!methodNameMap.containsKey(methodName)) methodNameMap.put(methodName, methodNameMap.size()+1);
			methodIDSequence.add(methodNameMap.get(methodName));
		}
		ImmutableList<Integer> idImmutableList = ImmutableList.copyOf(methodIDSequence);
		
		TObjectIntHashMap<ImmutableList<Integer>> objectUsageMap = (TObjectIntHashMap<ImmutableList<Integer>>)usageMap.get(objectTypeID);
		int count;
		if ((count = objectUsageMap.get(idImmutableList)) == Constants.DEFAULT_INT_NO_ENTRY_VALUE)
			objectUsageMap.put(idImmutableList, 1);
		else objectUsageMap.put(idImmutableList, count+1);
	}

	public void put(List<String> objectTypes, List<String> sequence) {
		List<Integer> objectIDs = new ArrayList<>();
		for (String objectType: objectTypes) {
			if (!typeMap.containsKey(objectType)) typeMap.put(objectType, typeMap.size()+1);
			objectIDs.add(typeMap.get(objectType));
		}
		Collections.sort(objectIDs);
		ImmutableList<Integer> objectIDImmutableList = ImmutableList.copyOf(objectIDs);
		
		int mulObjID;
		if ((mulObjID = mulObjMap.get(objectIDImmutableList)) == Constants.DEFAULT_INT_NO_ENTRY_VALUE) {
			mulObjID = mulObjMap.size()+1;
			mulObjMap.put(objectIDImmutableList, mulObjID);
		}
		if (!usageMulObjMap.containsKey(mulObjID)) usageMulObjMap.put(mulObjID, new TObjectIntHashMap<ImmutableList<Integer>>(SEQUENCE_INIT_SIZE, MAX_LOAD_FACTOR));
		
		String methodName;
		List<Integer> methodIDSequence = new ArrayList<>();
		for (int i = 0; i < sequence.size();i++) {
			methodName = sequence.get(i);
			if (!methodNameMap.containsKey(methodName)) methodNameMap.put(methodName, methodNameMap.size()+1);
			methodIDSequence.add(methodNameMap.get(methodName));
		}
		ImmutableList<Integer> idImmutableList = ImmutableList.copyOf(methodIDSequence);
		
		TObjectIntHashMap<ImmutableList<Integer>> objectUsageMap = (TObjectIntHashMap<ImmutableList<Integer>>)usageMulObjMap.get(mulObjID);
		int count;
		if ((count = objectUsageMap.get(idImmutableList)) == Constants.DEFAULT_INT_NO_ENTRY_VALUE)
			objectUsageMap.put(idImmutableList, 1);
		else objectUsageMap.put(idImmutableList, count+1);
	}
	
	public void save(int partIndex) throws Exception {
		File partDirectory = new File(directory, String.valueOf(partIndex));
		if (!partDirectory.exists()) partDirectory.mkdirs();
		FileUtils.cleanDirectory(partDirectory);
		
		String[] entries = new String[2];
		//save object names map
		CSVWriter objectsCsvWriter = new CSVWriter(new FileWriter(partDirectory.getAbsolutePath() + File.separator + "Objects.csv"), ',', '"');
		Object[] objectNames = typeMap.keys();
		for (int i = 0; i < objectNames.length; i++) {
			entries[0] = (String)objectNames[i];
			entries[1] = Integer.toString(typeMap.get(objectNames[i]));
			objectsCsvWriter.writeNext(entries);
		}
		objectsCsvWriter.close();
		
		//save method names map
		CSVWriter methodsCsvWriter = new CSVWriter(new FileWriter(partDirectory.getAbsolutePath() + File.separator + "MethodNames.csv"), ',', '"');
		Object[] methodNames = methodNameMap.keys();
		for (int i = 0; i < methodNames.length; i++) {
			entries[0] = (String)methodNames[i];
			entries[1] = Integer.toString(methodNameMap.get(methodNames[i]));
			methodsCsvWriter.writeNext(entries);
		}
		methodsCsvWriter.close();
		
		//save multiple objects id map
		CSVWriter multipleObjectsCsvWriter = new CSVWriter(new FileWriter(partDirectory.getAbsolutePath() + File.separator + "MultipleObjects.csv"), ',', '"');
		Object[] multipleObjectsIDs = mulObjMap.keys();
		for (int i = 0; i < multipleObjectsIDs.length; i++) {
			entries[0] = multipleObjectsIDs[i].toString();
			entries[1] = Integer.toString(mulObjMap.get(multipleObjectsIDs[i]));
			multipleObjectsCsvWriter.writeNext(entries);
		}
		multipleObjectsCsvWriter.close();
		
		//save usages for 1 object
		//first create the folder: SingleObjectUsages and clean it
		File singleObjectUsagesDir = new File(partDirectory.getAbsolutePath() + File.separator + "SingleObjectUsages");
		if (!singleObjectUsagesDir.exists()) singleObjectUsagesDir.mkdir();
		FileUtils.cleanDirectory(singleObjectUsagesDir);
		
		IntArrayList singleObjectUsageKeys = usageMap.keys();
		for (int i = 0; i < singleObjectUsageKeys.size(); i++) {
			String fileName = singleObjectUsagesDir.getAbsolutePath() + File.separator + singleObjectUsageKeys.get(i) + ".csv";
			CSVWriter singleObjectUsageCsvWriter = new CSVWriter(new FileWriter(fileName), ',', '"');
			TObjectIntMap<ImmutableList<Integer>> singleObjectUsage = (TObjectIntMap<ImmutableList<Integer>>)usageMap.get(singleObjectUsageKeys.get(i));
			Object[] sequences = singleObjectUsage.keys();
			for (int j = 0; j < sequences.length; j++) {
				entries[0] = sequences[j].toString();
				entries[1] = Integer.toString(singleObjectUsage.get(sequences[j]));
				singleObjectUsageCsvWriter.writeNext(entries);
			}
			singleObjectUsageCsvWriter.close();
		}

		
		//save usages for multiple objects
		//first create the folder: MultipleObjectsUsages and clear it
		File multipleObjectsUsagesFir = new File(partDirectory.getAbsolutePath() + File.separator + "MultipleObjectsUsages");
		if (!multipleObjectsUsagesFir.exists()) multipleObjectsUsagesFir.mkdir();
		FileUtils.cleanDirectory(multipleObjectsUsagesFir);
		
		IntArrayList multipleObjectUsageKeys = usageMulObjMap.keys();
		for (int i = 0; i < multipleObjectUsageKeys.size(); i++) {
			String fileName = multipleObjectsUsagesFir.getAbsolutePath() + File.separator + multipleObjectUsageKeys.get(i) + ".csv";
			CSVWriter multipleObjectUsageCsvWriter = new CSVWriter(new FileWriter(fileName), ',', '"');
			TObjectIntMap<ImmutableList<Integer>> multipleObjectUsage = (TObjectIntMap<ImmutableList<Integer>>)usageMulObjMap.get(multipleObjectUsageKeys.get(i));
			Object[] sequences = multipleObjectUsage.keys();
			for (int j = 0; j < sequences.length; j++) {
				entries[0] = sequences[j].toString();
				entries[1] = Integer.toString(multipleObjectUsage.get(sequences[j]));
				multipleObjectUsageCsvWriter.writeNext(entries);
			}
			multipleObjectUsageCsvWriter.close();
		}
	}

	public void saveFinalMaps() throws IOException {
		File partDirectory = new File(directory, "Final");
		if (!partDirectory.exists()) partDirectory.mkdirs();
		try {
			FileUtils.cleanDirectory(partDirectory);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String[] entries = new String[2];
		//save object names map
		CSVWriter objectsCsvWriter = new CSVWriter(new FileWriter(partDirectory.getAbsolutePath() + File.separator + "Objects.csv"), ',', '"');
		Object[] objectNames = typeMap.keys();
		for (int i = 0; i < objectNames.length; i++) {
			entries[0] = (String)objectNames[i];
			entries[1] = Integer.toString(typeMap.get(objectNames[i]));
			objectsCsvWriter.writeNext(entries);
		}
		objectsCsvWriter.close();
		
		//save method names map
		CSVWriter methodsCsvWriter = new CSVWriter(new FileWriter(partDirectory.getAbsolutePath() + File.separator + "MethodNames.csv"), ',', '"');
		Object[] methodNames = methodNameMap.keys();
		for (int i = 0; i < methodNames.length; i++) {
			entries[0] = (String)methodNames[i];
			entries[1] = Integer.toString(methodNameMap.get(methodNames[i]));
			methodsCsvWriter.writeNext(entries);
		}
		methodsCsvWriter.close();
		
		//save multiple objects id map
		CSVWriter multipleObjectsCsvWriter = new CSVWriter(new FileWriter(partDirectory.getAbsolutePath() + File.separator + "MultipleObjects.csv"), ',', '"');
		Object[] multipleObjectsIDs = mulObjMap.keys();
		for (int i = 0; i < multipleObjectsIDs.length; i++) {
			entries[0] = multipleObjectsIDs[i].toString();
			entries[1] = Integer.toString(mulObjMap.get(multipleObjectsIDs[i]));
			multipleObjectsCsvWriter.writeNext(entries);
		}
		multipleObjectsCsvWriter.close();
	}

	public void clearUsageMaps() {
		usageMap = new OpenIntObjectHashMap(MUL_OBJ_INIT_SIZE, MIN_LOAD_FACTOR, MAX_LOAD_FACTOR);
		usageMulObjMap = new OpenIntObjectHashMap(MUL_OBJ_INIT_SIZE, MIN_LOAD_FACTOR, MAX_LOAD_FACTOR);
	}
}
