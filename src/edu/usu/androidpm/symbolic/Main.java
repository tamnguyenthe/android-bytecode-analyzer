package edu.usu.androidpm.symbolic;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;

import org.jf.dexlib2.DexFileFactory;
import org.jf.dexlib2.dexbacked.DexBackedClassDef;
import org.jf.dexlib2.dexbacked.DexBackedDexFile;
import org.jf.dexlib2.dexbacked.DexBackedMethod;
import org.jf.dexlib2.iface.Method;
import org.jf.dexlib2.iface.instruction.Instruction;

import com.google.common.base.Stopwatch;

import edu.usu.androidpm.database.MemoryManager;
import edu.usu.androidpm.util.Constants;
import edu.usu.androidpm.util.PackageUtil;

public class Main {

	public static final int APPS_SINGLE_RUN = 8000;
	private static final long MEGABYTE = 1024L * 1024L;

	private static MemoryManager memoryManager = null;
	private static HashSet<Long> classesHashCodeSet = null;

	public static MemoryManager getMemoryManager() {
		if (memoryManager == null) memoryManager = new MemoryManager();
		return memoryManager;
	}

	public static HashSet<Long> getClassesHashCodeSet() {
		return classesHashCodeSet;
	}

	// adapted from String.hashCode()
	public static long hash(String string) {
		long h = 1125899906842597L; // prime
		int len = string.length();

		for (int i = 0; i < len; i++) {
			h = 31*h + string.charAt(i);
		}
		return h;
	}

	public static long bytesToMegabytes(long bytes) {
		return bytes / MEGABYTE;
	}


	public static void main(String[] args) throws Exception {
		// create a stopwatch to track time
		Stopwatch stopwatch = Stopwatch.createStarted();
		classesHashCodeSet = new HashSet<>(16000000);
		// load folder that contains all dex files
		File appDirectory = new File("/home/useallab2/Documents/androidapps/apps");
		if (!appDirectory.isDirectory()) {
			System.err.println("There is a problem with the app directory");
			return;
		}
		System.out.println("Folder: " + appDirectory.getAbsolutePath());
		File[] appFiles = appDirectory.listFiles();
		Arrays.sort(appFiles, new Comparator<File>() {
			public int compare(File a, File b) {
				return a.getName().compareTo(b.getName());
			}
		});
		int totalApps = appFiles.length;
		System.out.println("The number of dex files: " + totalApps);
		
		// process apps
		int nParts = totalApps / APPS_SINGLE_RUN;
		for (int partIndex = 0; partIndex <= nParts; partIndex++) {
			getMemoryManager().clearUsageMaps();
			processSinglePart(appFiles, partIndex);
			getMemoryManager().save(partIndex);
		}
		
		// save final maps
		getMemoryManager().saveFinalMaps();

		// calculate the used memory
		Runtime runtime = Runtime.getRuntime();
		runtime.gc();
		long memory = runtime.totalMemory() - runtime.freeMemory();
		System.out.println("Used memory is bytes: " + memory);
		System.out.println("Used memory is megabytes: " + bytesToMegabytes(memory));

		// calculate the running time
		stopwatch.stop();
		System.out.println("Processing time: " + stopwatch.toString());
	}

	private static void processSinglePart(File[] appFiles, int partIndex) {
		int from = partIndex * APPS_SINGLE_RUN;
		int to = partIndex == appFiles.length / APPS_SINGLE_RUN ? appFiles.length : (partIndex + 1) * APPS_SINGLE_RUN;
		DexBackedDexFile dexFile;
		for (int index = from; index < to; index++) {
			if (!appFiles[index].getName().endsWith(".dex")) continue;
			System.out.println(index + " Processing " + appFiles[index].getName() + " ...");
			try {
				dexFile = DexFileFactory.loadDexFile(appFiles[index], Constants.API);
			} catch (IOException e) {
				e.printStackTrace();
				continue;
			}
			processDexFile(dexFile);
		}
	}

	private static void processDexFile(DexBackedDexFile dexFile) {
		for (DexBackedClassDef classDef: dexFile.getClasses()) {
			//we do not process class belongs to the Android Library
			if (!PackageUtil.isSystemClass(classDef)) {
				long clsHash = hash(classDef.getType());
				if (!classesHashCodeSet.contains(clsHash)) {
					classesHashCodeSet.add(clsHash);
					for (DexBackedMethod method: classDef.getMethods()) {
						if (checkMethod(method)) {
							try {
								processMethod(classDef, method);
							} catch (Exception e) {
								e.printStackTrace();
								continue;
							}
						}
					}
				}
			}
		}
	}

	private static void processMethod(DexBackedClassDef classDef,
			DexBackedMethod method) {
		ProgramExecutor programExecutor = new ProgramExecutor(method);
		programExecutor.execute();
	}

	private static boolean checkMethod(Method method) {
		if (method.getName().length() < Constants.MIN_METHOD_NAME_LENGHT) return false;
		if (method.getImplementation() == null) return false;
		int numberOfIfNode = 0; 
		boolean hasSwitch = false;
		int size = 0;
		for (Instruction instruction: method.getImplementation().getInstructions()) {
			switch (instruction.getOpcode()) {
			case IF_EQ:
			case IF_NE:
			case IF_LT:
			case IF_GE:
			case IF_GT:
			case IF_LE:
			case IF_EQZ:
			case IF_NEZ:
			case IF_LTZ:
			case IF_GEZ:
			case IF_GTZ:
			case IF_LEZ:
				numberOfIfNode++;
				break;
			case PACKED_SWITCH:
			case SPARSE_SWITCH:
				hasSwitch = true;
				break;
			default:
				break;
			}
			size++;
		}
		if (numberOfIfNode > 10) return false;
		if (hasSwitch) return false;
		if (size > 1000) return false;
		if (size < 6) return false;
		return true;
	}

}
