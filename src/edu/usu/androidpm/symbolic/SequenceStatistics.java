package edu.usu.androidpm.symbolic;

import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableList;
import com.opencsv.CSVReader;

import edu.usu.androidpm.util.Constants;

public class SequenceStatistics {
	private BiMap<Integer, String> objectsBiMap;
	private BiMap<Integer, String> methodNamesBiMap;
	private BiMap<Integer, ImmutableList<Integer>> multipleObjectsBiMap;
	
	public BiMap<Integer, String> getObjectsBiMap() {
		return objectsBiMap;
	}

	public BiMap<Integer, String> getMethodNamesBiMap() {
		return methodNamesBiMap;
	}

	public BiMap<Integer, ImmutableList<Integer>> getMultipleObjectsBiMap() {
		return multipleObjectsBiMap;
	}
	
	private ImmutableList<Integer> getListFromString(String strArr) {
		String[] parts = strArr.substring(1, strArr.length()-1).split(", ");
		List<Integer> result = new ArrayList<>(parts.length);
		for (int i = 0; i < parts.length; i++) result.add(Integer.valueOf(parts[i]));
		return ImmutableList.copyOf(result);
	}
	
	public SequenceStatistics() throws Exception {
		String[] entries;
		
		//load the object id map
		objectsBiMap = HashBiMap.create();
		CSVReader objectsCsvReader = new CSVReader(new FileReader(Constants.OBJECTS_FILE), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		while ((entries = objectsCsvReader.readNext()) != null) {
			objectsBiMap.put(Integer.valueOf(entries[1]), entries[0]);
		}
		objectsCsvReader.close();
		
		//load the method name id map
		methodNamesBiMap = HashBiMap.create();
		CSVReader methodNamesCsvReader = new CSVReader(new FileReader(Constants.METHOD_NAMES_FILE), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		while ((entries = methodNamesCsvReader.readNext()) != null) {
			methodNamesBiMap.put(Integer.valueOf(entries[1]), entries[0]);
		}
		methodNamesCsvReader.close();
		
		//load the multiple objecs id map
		ImmutableList<Integer> multipleObjectsIDs;
		multipleObjectsBiMap = HashBiMap.create();
		CSVReader multipleObjectsCsvReader = new CSVReader(new FileReader(Constants.MULTIPLE_OBJECTS_FILE), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		while ((entries = multipleObjectsCsvReader.readNext()) != null) {
			multipleObjectsIDs = getListFromString(entries[0]);
			multipleObjectsBiMap.put(Integer.valueOf(entries[1]), multipleObjectsIDs);
		}
		multipleObjectsCsvReader.close();
	}
	
	
	public List<MutablePair<ImmutableList<Integer>, Integer>> loadDatasetForObject(int objectID) throws Exception {
		Integer loadID = methodNamesBiMap.inverse().get(Constants.LOAD);
		if (loadID == null) loadID = -1;
		List<MutablePair<ImmutableList<Integer>, Integer>> dataset = new ArrayList<>();
		String fileName = Constants.SINGLE_OBJECT_USAGE_DIR + File.separator + objectID + ".csv";
		File file = new File(fileName);
		if (!file.exists()) return null;
		CSVReader singleObjectUsageCsvReader = new CSVReader(new FileReader(fileName), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		String[] entries;
		ImmutableList<Integer> sequence;
		Integer count;
		while ((entries = singleObjectUsageCsvReader.readNext()) != null) {
			sequence = getListFromString(entries[0]);
			if (sequence.get(0) == loadID) {
				sequence = sequence.subList(1, sequence.size());
			}
			if (sequence.size() > 1) {
				count = Integer.valueOf(entries[1]);
				dataset.add(MutablePair.of(sequence, count));
			}
			
		}
		singleObjectUsageCsvReader.close();
		return dataset;
	}
	
	public List<MutablePair<ImmutableList<Integer>, Integer>> loadDatasetForMultipleObjects(int multipleObjectsID) throws Exception {
		Integer loadID = methodNamesBiMap.inverse().get(Constants.LOAD);
		if (loadID == null) loadID = -1;
		List<MutablePair<ImmutableList<Integer>, Integer>> dataset = new ArrayList<>();
		String fileName = Constants.MULTIPLE_OBJECTS_USAGE_DIR + File.separator + multipleObjectsID + ".csv";
		File file = new File(fileName);
		if (!file.exists()) return null;
		CSVReader multipleObjectsUsageCsvReader = new CSVReader(new FileReader(fileName), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		String[] entries;
		ImmutableList<Integer> sequence;
		Integer count;
		while ((entries = multipleObjectsUsageCsvReader.readNext()) != null) {
			sequence = getListFromString(entries[0]);
			if (sequence.get(0) == loadID) {
				sequence = sequence.subList(1, sequence.size());
			}
			if (sequence.size() > 1) {					
				count = Integer.valueOf(entries[1]);
				dataset.add(MutablePair.of(sequence, count));
			}
		}
		multipleObjectsUsageCsvReader.close();
		return dataset;
	}
	
	
	public void singleObjectStatistics() throws Exception {
		List<MutablePair<ImmutableList<Integer>, Integer>> sequences;
		List<Integer> numberOfMethodSequences = new ArrayList<>();
		
		PrintWriter numPrintWriter = new PrintWriter("num-single.txt");
		PrintWriter lenPrintWriter = new PrintWriter("len-single.txt");
		for (Map.Entry<Integer, String> entry: objectsBiMap.entrySet()) {
			sequences = loadDatasetForObject(entry.getKey());
			if (sequences == null) continue;
			int sum = 0;
			int len = 0;
			for (Pair<ImmutableList<Integer>, Integer> pair: sequences) {
				sum += pair.getRight();
				len += pair.getLeft().size() * pair.getRight();
			}
			numPrintWriter.println(sum);
			lenPrintWriter.println(len);
			numberOfMethodSequences.add(sum);
		}
		
		//for (Integer num: numberOfMethodSequences) numPrintWriter.println(num);
		numPrintWriter.close();
		lenPrintWriter.close();
	}
	
	public void multipleObjectStatistics() throws Exception {
		List<MutablePair<ImmutableList<Integer>, Integer>> sequences;
		List<Integer> numberOfMethodSequences = new ArrayList<>();
		
		PrintWriter numPrintWriter = new PrintWriter("num-mul.txt");
		PrintWriter lenPrintWriter = new PrintWriter("len-mul.txt");

		for (Map.Entry<Integer, ImmutableList<Integer>> entry: multipleObjectsBiMap.entrySet()) {
			sequences = loadDatasetForMultipleObjects(entry.getKey());
			if (sequences == null) continue;
			int sum = 0;
			int len = 0;
			for (Pair<ImmutableList<Integer>, Integer> pair: sequences) {
				sum += pair.getRight();
				len += pair.getLeft().size() * pair.getRight();
			}
			numPrintWriter.println(sum);
			lenPrintWriter.println(len);
			numberOfMethodSequences.add(sum);
		}
		
		//for (Integer num: numberOfMethodSequences) numPrintWriter.println(num);
		numPrintWriter.close();
		lenPrintWriter.close();
	}
	
	public static void main(String[] args) throws Exception {
		SequenceStatistics sequenceStatistics = new SequenceStatistics();
		System.out.println("number of objects: " + sequenceStatistics.getObjectsBiMap().size());
		System.out.println("number of object sets: " + sequenceStatistics.getMultipleObjectsBiMap().size());
		sequenceStatistics.singleObjectStatistics();
		sequenceStatistics.multipleObjectStatistics();
	}

}
