package edu.usu.androidpm.symbolic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;

import org.apache.commons.lang3.tuple.Pair;
import org.jf.dexlib2.dexbacked.DexBackedMethod;
import org.jf.dexlib2.iface.instruction.ReferenceInstruction;
import org.jf.dexlib2.iface.reference.FieldReference;
import org.jf.dexlib2.iface.reference.MethodReference;
import org.jf.dexlib2.iface.reference.Reference;
import org.jf.dexlib2.util.MethodUtil;
import org.jf.dexlib2.util.ReferenceUtil;
import org.jf.dexlib2.util.TypeUtils;

import com.google.common.collect.ImmutableList;

import edu.usu.androidpm.cfg.InstructionGraph;
import edu.usu.androidpm.cfg.InstructionNode;
import edu.usu.androidpm.cfg.node.MethodNode;
import edu.usu.androidpm.cfg.node.Node;
import edu.usu.androidpm.cfg.node.ObjectNode;
import edu.usu.androidpm.cfg.node.PrimitiveNode;
import edu.usu.androidpm.symbolic.instruction.InstructionExecutorFactory;
import edu.usu.androidpm.util.Constants;
import edu.usu.androidpm.util.PackageUtil;

public class ProgramExecutor {
	private final DexBackedMethod method;
	private final InstructionGraph instructionGraph;
	private ProgramState startState;	

	private Map<Integer, Pair<String,Set<ImmutableList<Integer>>>> usages;
	private Map<ImmutableList<Integer>, Pair<List<String>, Set<ImmutableList<Integer>>>> mulObjsUsages;

	public ProgramExecutor(DexBackedMethod method) {
		this.method = method;
		instructionGraph = new InstructionGraph(ImmutableList.copyOf(method.getImplementation().getInstructions()));
		initializeProgramEnvironment();
		usages = new HashMap<>();
		mulObjsUsages = new HashMap<>();
	}

	private void initializeProgramEnvironment() {
		startState = new ProgramState(0, new ProgramEnvironment());
		int baseRegister = method.getImplementation().getRegisterCount() - MethodUtil.getParameterRegisterCount(method);
		if (!MethodUtil.isStatic(method)) {
			ObjectNode objectNode = new ObjectNode(startState.getEnv().getNodes().size(), -1, method.getDefiningClass(), baseRegister);		
			startState.getEnv().addDataNode(objectNode);
			baseRegister++;
		}
		for (String parameterType: method.getParameterTypes()) {
			if (TypeUtils.isPrimitiveType(parameterType)) {
				PrimitiveNode primitiveNode = new PrimitiveNode(startState.getEnv().getNodes().size(), -1, parameterType, baseRegister);
				startState.getEnv().addDataNode(primitiveNode);
			} else {
				ObjectNode objectNode = new ObjectNode(startState.getEnv().getNodes().size(), -1 , parameterType, baseRegister);
				startState.getEnv().addDataNode(objectNode);
			}
			baseRegister++;
			if (TypeUtils.isWideType(parameterType)) {
				baseRegister++;
			}
		}
		assert baseRegister == this.method.getImplementation().getRegisterCount();
	}

	public void execute() {
		InstructionExecutorFactory instructionExecutorFactory = new InstructionExecutorFactory();
		Stack<ProgramState> frontier = new Stack<>();
		frontier.push(startState);
		while (!frontier.empty()) {
			ProgramState currentState = frontier.pop();
			currentState.getEnv().getExploredPaths().add(currentState.getStartOffset());
			InstructionNode instructionNode = instructionGraph.getInstructionNodeAtCodeOffset(currentState.getStartOffset());
			while (!instructionNode.isControlNode()) {
				instructionExecutorFactory.getInstructionExecutorMap().get(instructionNode.getInstruction().getOpcode()).execute(instructionNode, currentState.getEnv());
				instructionNode = instructionGraph.getInstructionNodeAtCodeOffset(instructionNode.getNextNodes().get(0));
			}
			if (instructionNode.getNextNodes().size() == 0) {
				//currentState.getEnv().saveToGraphViz();
				currentState.getEnv().addDataBackEdges();
				collectUsages(currentState.getEnv());
				//collectSingleObjUsages(currentState.getEnv());
				//collectMultipleObjUsages(currentState.getEnv());
			} else {
				for (int offset: instructionNode.getNextNodes()) {
					if (!currentState.getEnv().getExploredPaths().contains(offset)){
						ProgramEnvironment programEnvironment = currentState.getEnv().getCopy();
						frontier.push(new ProgramState(offset, programEnvironment));
					} 
				}
			}
		}
		saveUsages();
	}
	
	private void collectUsages(ProgramEnvironment env) {
		Set<Integer> offsetSet;
		List<Integer> idList;
		Map<ImmutableList<Integer>, List<Integer>> idsMap = new HashMap<>();
		Set<String> typeSet;
		for (Node node: env.getNodes()) {
			//single object
			if (node instanceof ObjectNode) {
				ObjectNode objectNode = (ObjectNode)node;
				if (PackageUtil.isSystemObject(objectNode.getType())) {
					int objOffset = objectNode.getOffset();
					if (!usages.containsKey(objOffset)) {
						Set<ImmutableList<Integer>> tempSet = new HashSet<>();
						usages.put(objOffset, Pair.of(objectNode.getType(), tempSet));
					}
					List<Integer> sequence = env.getMethodSequences(objectNode);
					if (sequence.size() > 1) {
						usages.get(objOffset).getRight().add(ImmutableList.copyOf(sequence));
					}
				}
			//multiple objects
			} else if (node instanceof MethodNode) {
				MethodNode methodNode = (MethodNode)node;
				if (PackageUtil.isSystemObject(methodNode.getReference().getDefiningClass())) {
					offsetSet = new TreeSet<>();
					idList = new ArrayList<>();
					typeSet = new HashSet<>();
					//collect object nodes that have data dependency with this method
					for (Integer dataNodeID: env.listOfDataNodes(methodNode.getID())) {
						Node tempNode = env.getNodes().get(dataNodeID);
						if (tempNode instanceof ObjectNode) {
							ObjectNode objectNode = (ObjectNode)tempNode;
							if (PackageUtil.isSystemObject(objectNode.getType())) {
								if (!typeSet.contains(objectNode.getType())) {
									typeSet.add(objectNode.getType());
									offsetSet.add(objectNode.getOffset());
									idList.add(objectNode.getID());
								}
							}
						}
					}
					if (offsetSet.size() >= 2) idsMap.put(ImmutableList.copyOf(offsetSet), idList);
				}
			}
		}
		List<String> typeList;
		List<Integer> sequence;
		for (Map.Entry<ImmutableList<Integer>, List<Integer>> entry: idsMap.entrySet()) {
			typeList = new ArrayList<>();
			for (Integer id: entry.getValue()) {
				typeList.add(((ObjectNode)env.getNodes().get(id)).getType());
			}
			Collections.sort(typeList);
			if (!mulObjsUsages.containsKey(entry.getKey())) {
				Set<ImmutableList<Integer>> tempSet = new HashSet<>();
				mulObjsUsages.put(entry.getKey(), Pair.of(typeList, tempSet ));
			}
			sequence = env.getMethodSequences(entry.getValue());
			if (sequence.size() > 1) {
				//System.out.println(sequence);
				mulObjsUsages.get(entry.getKey()).getRight().add(ImmutableList.copyOf(sequence));
			}
		}
	}
	
	private void saveUsages() {
		for (Map.Entry<Integer, Pair<String, Set<ImmutableList<Integer>>>> entry: usages.entrySet()) {
			for (List<Integer> offsetSequence: entry.getValue().getRight()) {
				List<String> sequence = new ArrayList<>();
				for (Integer i: offsetSequence) {
					if (i == -1) sequence.add(Constants.LOAD);
					else {
						InstructionNode instructionNode = instructionGraph.getInstructionNodeAtCodeOffset(i);
						ReferenceInstruction instruction = (ReferenceInstruction)instructionNode.getInstruction();
						Reference reference = instruction.getReference();
						if (reference instanceof MethodReference) sequence.add(ReferenceUtil.getReferenceString(instruction.getReference()));						
						else if (reference instanceof FieldReference) sequence.add(instruction.getOpcode().name + "->" + ReferenceUtil.getReferenceString(instruction.getReference()));
					}	
				}
				if (sequence.size() > 1) {
					//Main.dbManager.saveSequence(entry.getKey().getRight(), sequence);
					Main.getMemoryManager().put(entry.getValue().getLeft(), sequence);
				}
			}
		}
		for (Pair<List<String>, Set<ImmutableList<Integer>>> pair: mulObjsUsages.values()) {
			for (List<Integer> offsetSequence: pair.getRight()) {
				List<String> sequence = new ArrayList<>();
				for (Integer i: offsetSequence) {
					if (i == -1) sequence.add(Constants.LOAD);
					else {
						InstructionNode instructionNode = instructionGraph.getInstructionNodeAtCodeOffset(i);
						ReferenceInstruction instruction = (ReferenceInstruction)instructionNode.getInstruction();
						Reference reference = instruction.getReference();
						if (reference instanceof MethodReference) sequence.add(ReferenceUtil.getReferenceString(instruction.getReference()));						
						else if (reference instanceof FieldReference) sequence.add(instruction.getOpcode().name + "->" + ReferenceUtil.getReferenceString(instruction.getReference()));				
					}
				}
				if (sequence.size() > 1) {
					Main.getMemoryManager().put(pair.getLeft(), sequence);
				}
			}
		}
	}
	
//
//	private void collectMultipleObjUsages(ProgramEnvironment env) {
//		Set<Integer> offsetSet;
//		List<Integer> idList;
//		Map<Integer, List<Integer>> idsMap = new HashMap<>();
//		for (Node node: env.getNodes()) {
//			if (node instanceof MethodNode) {
//				MethodNode methodNode = (MethodNode)node;
//				if (PackageUtil.isSystemObject(methodNode.getReference().getDefiningClass())) {
//					
//					offsetSet = new HashSet<>();
//					idList = new ArrayList<>();
//					
//					//collect object nodes that have data dependency with this method
//					for (Integer dataNodeID: env.listOfDataNodes(methodNode.getID())) {
//						Node tempNode = env.getNodes().get(dataNodeID);
//						if (tempNode instanceof ObjectNode) {
//							ObjectNode objectNode = (ObjectNode)tempNode;
//							if (PackageUtil.isSystemObject(objectNode.getType())) {
//								if (!offsetSet.contains(objectNode.getOffset())) {
//									offsetSet.add(objectNode.getOffset());
//									idList.add(dataNodeID);
//								}
//							}
//						}
//					}
//					
//					if (offsetSet.size() >= 2) {
//						idsMap.put(offsetSet.hashCode(), idList);
//					}
//				}
//			}
//		}
//		List<String> typeList;
//		List<Integer> sequence;
//		for (Map.Entry<Integer, List<Integer>> entry: idsMap.entrySet()) {
//			typeList = new ArrayList<>();
//			for (Integer id: entry.getValue()) {
//				typeList.add(((ObjectNode)env.getNodes().get(id)).getType());
//			}
//			Collections.sort(typeList);
//			if (!mulObjsUsages.containsKey(entry.getKey())) {
//				Map<Integer, List<Integer>> tempMap = new HashMap<Integer, List<Integer>>();
//				mulObjsUsages.put(entry.getKey(), Pair.of(typeList,tempMap ));
//			}
//			sequence = new ArrayList<>();
//			for (int i : env.listOfActionNodes(entry.getValue())) {
//				if (env.getNodes().get(i) instanceof MethodNode) {
//					MethodNode methodNode = (MethodNode)env.getNodes().get(i);
//					if (PackageUtil.isSystemObject(methodNode.getReference().getDefiningClass())) {
//						sequence.add(methodNode.getOffset());
//					}
//				} else if (env.getNodes().get(i) instanceof AccessorNode) {
//					AccessorNode setterGetterNode = (AccessorNode)env.getNodes().get(i);
//					if (PackageUtil.isSystemObject(setterGetterNode.getReference().getDefiningClass())) {
//						sequence.add(setterGetterNode.getOffset());
//					}
//				}
//			}
//			if (sequence.size() > 1) {
//				//System.out.println(sequence);
//				mulObjsUsages.get(entry.getKey()).getRight().put(sequence.hashCode(), sequence);
//			}
//		}
//		
//	}
	

	

//	private void collectSingleObjUsages(ProgramEnvironment env) {
//		List<Integer> sequence;
//		for (Node node: env.getNodes()) {
//			if (node instanceof ObjectNode) {
//				ObjectNode objectNode = (ObjectNode)node;
//				if (PackageUtil.isSystemObject(objectNode.getType()) && objectNode.getActionNodeID() != -1 ) {//&& objectNode.getType().equals("Ljava/io/BufferedReader;")) {
//					
//					Pair<Integer, String> pair = Pair.of(objectNode.getOffset(), objectNode.getType());
//
//					if (!usages.containsKey(pair)) usages.put(pair, new HashMap<Integer, List<Integer>>());
//					sequence = new ArrayList<>();
//					for (int i : env.listOfActionNodes(objectNode.getID())) {
//						if (env.getNodes().get(i) instanceof MethodNode) {
//							MethodNode methodNode = (MethodNode)env.getNodes().get(i);
//							if (PackageUtil.isSystemObject(methodNode.getReference().getDefiningClass())) {
//								sequence.add(methodNode.getOffset());
//							}
//						} else if (env.getNodes().get(i) instanceof AccessorNode) {
//							AccessorNode setterGetterNode = (AccessorNode)env.getNodes().get(i);
//							if (PackageUtil.isSystemObject(setterGetterNode.getReference().getDefiningClass())) {
//								sequence.add(setterGetterNode.getOffset());
//							}
//						}
//					}
//					if (sequence.size() > 1) {
//						usages.get(pair).put(sequence.hashCode(), sequence);
//					}
//
//					//					for (Integer i: sequence) {
//					//						try {
//					//							InstructionNode instructionNode = instructionGraph.getInstructionNodeAtCodeOffset(i);
//					//							ReferenceInstruction instruction = (ReferenceInstruction)instructionNode.getInstruction();
//					//							System.out.print(ReferenceUtil.getReferenceString(instruction.getReference()) + " | ");
//					//						} catch (Exception e) {
//					//							continue;
//					//						}			
//					//					}
//					//					System.out.println();
//				}
//			}
//		}
//	}


}