package edu.usu.androidpm.symbolic;

public class ProgramState {
	private int startOffset;
	private ProgramEnvironment env;
	
	public ProgramState(int startOffset, ProgramEnvironment env) {
		setStartOffset(startOffset);
		setEnv(env);
	}
	
	public int getStartOffset() {
		return startOffset;
	}
	public void setStartOffset(int startOffset) {
		this.startOffset = startOffset;
	}
	
	public ProgramEnvironment getEnv() {
		return env;
	}
	
	public void setEnv(ProgramEnvironment env) {
		this.env = env;
	}
}
