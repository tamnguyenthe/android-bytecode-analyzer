package edu.usu.androidpm.symbolic;

import java.io.File;

import org.jf.dexlib2.DexFileFactory;
import org.jf.dexlib2.dexbacked.DexBackedClassDef;
import org.jf.dexlib2.dexbacked.DexBackedDexFile;
import org.jf.dexlib2.dexbacked.DexBackedMethod;
import org.jf.dexlib2.iface.instruction.Instruction;

import com.google.common.collect.ImmutableList;

import edu.usu.androidpm.cfg.InstructionGraph;
import edu.usu.androidpm.cfg.InstructionNode;
import edu.usu.androidpm.util.Constants;

public class Test {
	
	
	public static void main(String[] args) throws Throwable {
		File appDirectory = new File("examples");
		if (!appDirectory.isDirectory()) {
			System.err.println("There is a problem with the app directory");
			return;
		}
		System.out.println("Folder: " + appDirectory.getAbsolutePath());
		String[] appNames = appDirectory.list();
		for (String appName: appNames) {
			if (!appName.endsWith(".dex")) continue;
			System.out.println("Processing " + appName + " ...");
			processDexFile(DexFileFactory.loadDexFile(appDirectory.getAbsolutePath() + "/" + appName, Constants.API));
		}
		
	}

	private static void processDexFile(DexBackedDexFile loadDexFile) {
		for (DexBackedClassDef classDef: loadDexFile.getClasses()) {
			for (DexBackedMethod method: classDef.getMethods()) {
				if (!method.getName().equals("main")) continue;
				ImmutableList<Instruction> instructions = ImmutableList.copyOf(method.getImplementation().getInstructions());
				InstructionGraph instructionGraph = new InstructionGraph(instructions);
				for (InstructionNode instructionNode: instructionGraph.getNodes()) {
					StringBuilder stringBuilder = new StringBuilder();
					stringBuilder.append(Integer.toHexString(instructionNode.getOffset()));
					stringBuilder.append(" ");
					stringBuilder.append(instructionNode.getInstruction().getOpcode());
					stringBuilder.append("\t\t\t" + instructionNode.isControlNode() + " ");
					stringBuilder.append(instructionNode.getNextNodes());
					System.out.println(stringBuilder.toString());
				}
			}
		}
	}

}
