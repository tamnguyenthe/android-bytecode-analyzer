package edu.usu.androidpm.symbolic.instruction;

import org.jf.dexlib2.iface.instruction.formats.Instruction21c;
import org.jf.dexlib2.iface.reference.Reference;
import org.jf.dexlib2.util.ReferenceUtil;

import edu.usu.androidpm.cfg.InstructionNode;
import edu.usu.androidpm.cfg.node.ClassNode;
import edu.usu.androidpm.cfg.node.OperationNode;
import edu.usu.androidpm.symbolic.ProgramEnvironment;

public class ConstClassInstruction extends InstructionExecutor {

	@Override
	public void execute(InstructionNode instructionNode, ProgramEnvironment env) {
		//obtain the instruction information
		Instruction21c instruction = (Instruction21c)instructionNode.getInstruction();
		int vA = instruction.getRegisterA();
		Reference reference = instruction.getReference();

		//create a operation node
		OperationNode operationNode = new OperationNode(env.getNodes().size(), instructionNode.getOffset(), instruction.getOpcode().value);
		operationNode.setOutputRegister(vA);
		operationNode.setOutputType(ReferenceUtil.getReferenceString(reference));
		env.addActionNode(operationNode);


		//create a primitive node
		ClassNode classNode = new ClassNode(env.getNodes().size(), instructionNode.getOffset(), operationNode.getOutputType() , operationNode.getOutputRegister(), operationNode.getID());
		env.addDataNode(classNode);

		//add the data edge
		operationNode.getDataEdges().add(classNode.getID());
	}

}
