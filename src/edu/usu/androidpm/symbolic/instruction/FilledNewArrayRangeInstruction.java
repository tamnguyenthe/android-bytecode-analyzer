package edu.usu.androidpm.symbolic.instruction;

import org.jf.dexlib2.iface.instruction.formats.Instruction3rc;
import org.jf.dexlib2.iface.reference.Reference;
import org.jf.dexlib2.util.ReferenceUtil;

import edu.usu.androidpm.cfg.InstructionNode;
import edu.usu.androidpm.cfg.node.OperationNode;
import edu.usu.androidpm.symbolic.ProgramEnvironment;

public class FilledNewArrayRangeInstruction extends InstructionExecutor {

	@Override
	public void execute(InstructionNode instructionNode, ProgramEnvironment env) {
		//obtain the instruction information
		Instruction3rc instruction = (Instruction3rc)instructionNode.getInstruction();
		int registerCount = instruction.getRegisterCount();
		int startRegister = instruction.getStartRegister();
		Reference reference = instruction.getReference();
		
		//create a new operation node
		OperationNode operationNode = new OperationNode(env.getNodes().size(), instructionNode.getOffset(), instruction.getOpcode().value);
		for (int i = 0; i < registerCount; i++) {
			operationNode.getInputRegisters().add(i+startRegister);
		}
		String type = "[" + ReferenceUtil.getReferenceString(reference);
		operationNode.setOutputType(type);;
		env.addActionNode(operationNode);
	}

}
