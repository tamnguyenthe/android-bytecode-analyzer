package edu.usu.androidpm.symbolic.instruction;

import org.jf.dexlib2.iface.instruction.formats.Instruction23x;

import edu.usu.androidpm.cfg.InstructionNode;
import edu.usu.androidpm.cfg.node.OperationNode;
import edu.usu.androidpm.symbolic.ProgramEnvironment;
import edu.usu.androidpm.util.PrimitiveType;

public class AputInstruction extends InstructionExecutor {

	@Override
	public void execute(InstructionNode instructionNode, ProgramEnvironment env) {
		//obtain the instruction information
		Instruction23x instruction = (Instruction23x)instructionNode.getInstruction();
		int vA = instruction.getRegisterA();
		int vB = instruction.getRegisterB();
		int vC = instruction.getRegisterC();
		
		//create a new operation node
		OperationNode operationNode = new OperationNode(env.getNodes().size(), instructionNode.getOffset(), instruction.getOpcode().value);
		operationNode.getInputRegisters().add(vA);
		operationNode.getInputRegisters().add(vB);
		operationNode.getInputRegisters().add(vC);
		int hostObjectID = env.getRegisterMap().get(vB);
		operationNode.setHostObjectID(hostObjectID);
		operationNode.setOutputType(PrimitiveType.VOID);
		env.addActionNode(operationNode);		
		
		//add data edge
		operationNode.getDataEdges().add(hostObjectID);
	}

}
