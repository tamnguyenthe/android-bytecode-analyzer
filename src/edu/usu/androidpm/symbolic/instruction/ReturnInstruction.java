package edu.usu.androidpm.symbolic.instruction;

import org.jf.dexlib2.Opcode;
import org.jf.dexlib2.iface.instruction.OneRegisterInstruction;

import edu.usu.androidpm.cfg.InstructionNode;
import edu.usu.androidpm.cfg.node.ControlNode;
import edu.usu.androidpm.cfg.node.DataNode;
import edu.usu.androidpm.symbolic.ProgramEnvironment;

public class ReturnInstruction extends InstructionExecutor {

	@Override
	public void execute(InstructionNode instructionNode, ProgramEnvironment env) {
		//create a control node and add to the cfg
		ControlNode controlNode = new ControlNode(env.getNodes().size(), instructionNode.getOffset(), instructionNode.getInstruction().getOpcode().value);
		env.addControlNode(controlNode);

		//add a data edge from the return object to new control node
		if (instructionNode.getInstruction().getOpcode() != Opcode.RETURN_VOID) {
			OneRegisterInstruction instruction = (OneRegisterInstruction)instructionNode.getInstruction();
			DataNode dataNode = (DataNode)env.getNodes().get(env.getRegisterMap().get(instruction.getRegisterA()));
			dataNode.getDataEdges().add(controlNode.getID());
		}
	}

}
