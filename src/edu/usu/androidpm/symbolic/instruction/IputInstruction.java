package edu.usu.androidpm.symbolic.instruction;

import org.jf.dexlib2.iface.instruction.formats.Instruction22c;
import org.jf.dexlib2.iface.reference.FieldReference;

import edu.usu.androidpm.cfg.InstructionNode;
import edu.usu.androidpm.cfg.node.AccessorNode;
import edu.usu.androidpm.symbolic.ProgramEnvironment;
import edu.usu.androidpm.util.PrimitiveType;

public class IputInstruction extends InstructionExecutor {

	@Override
	public void execute(InstructionNode instructionNode, ProgramEnvironment env) {
		//obtain the instruction information
		Instruction22c instruction = (Instruction22c)instructionNode.getInstruction();
		int vA = instruction.getRegisterA();
		int vB = instruction.getRegisterB();
		FieldReference reference = (FieldReference)instruction.getReference();

		//create a new accessor node
		AccessorNode accessorNode = new AccessorNode(env.getNodes().size(), instructionNode.getOffset(), instruction.getOpcode().value, reference);
		accessorNode.getInputRegisters().add(vA);
		int hostObjectID = env.getRegisterMap().get(vB);
		accessorNode.setHostObjectID(hostObjectID);
		accessorNode.setOutputType(PrimitiveType.VOID);
		env.addActionNode(accessorNode);
		
		//add data edge
		accessorNode.getDataEdges().add(hostObjectID);
	}

}
