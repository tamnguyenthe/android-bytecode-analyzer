package edu.usu.androidpm.symbolic.instruction;

import org.jf.dexlib2.iface.instruction.formats.Instruction21t;

import edu.usu.androidpm.cfg.InstructionNode;
import edu.usu.androidpm.cfg.node.ControlNode;
import edu.usu.androidpm.symbolic.ProgramEnvironment;

public class IfZInstruction extends InstructionExecutor {

	@Override
	public void execute(InstructionNode instructionNode, ProgramEnvironment env) {
		//obtain the instruction information
		Instruction21t instruction = (Instruction21t)instructionNode.getInstruction();
		int vA = instruction.getRegisterA();

		//create a new control node
		ControlNode controlNode = new ControlNode(env.getNodes().size(), instructionNode.getOffset(), instruction.getOpcode().value);
		controlNode.getInputRegisters().add(vA);
		env.addControlNode(controlNode);
	}

}
