package edu.usu.androidpm.symbolic.instruction;

import org.jf.dexlib2.iface.instruction.OneRegisterInstruction;

import edu.usu.androidpm.cfg.InstructionNode;
import edu.usu.androidpm.cfg.node.OperationNode;
import edu.usu.androidpm.cfg.node.PrimitiveNode;
import edu.usu.androidpm.symbolic.ProgramEnvironment;
import edu.usu.androidpm.util.PrimitiveType;

public class ConstStringInstruction extends InstructionExecutor {

	@Override
	public void execute(InstructionNode instructionNode, ProgramEnvironment env) {
		//obtain the instruction information
		OneRegisterInstruction instruction = (OneRegisterInstruction)instructionNode.getInstruction();
		int vA = instruction.getRegisterA();

		//create a operation node
		OperationNode operationNode = new OperationNode(env.getNodes().size(), instructionNode.getOffset(), instruction.getOpcode().value);
		operationNode.setOutputRegister(vA);
		operationNode.setOutputType(PrimitiveType.getOutputTypeForOpcode(instruction.getOpcode()));
		env.addActionNode(operationNode);

		//create a primitive node
		PrimitiveNode primitiveNode =  new PrimitiveNode(env.getNodes().size(), instructionNode.getOffset(), operationNode.getOutputType(), operationNode.getOutputRegister(), operationNode.getID(), operationNode.getHostObjectID());
		env.addDataNode(primitiveNode);

		//add the data edge
		operationNode.getDataEdges().add(primitiveNode.getID());		
	}

}
