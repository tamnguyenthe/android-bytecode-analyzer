package edu.usu.androidpm.symbolic.instruction;

import org.jf.dexlib2.iface.instruction.formats.Instruction21c;
import org.jf.dexlib2.iface.reference.Reference;
import org.jf.dexlib2.util.ReferenceUtil;

import edu.usu.androidpm.cfg.InstructionNode;
import edu.usu.androidpm.cfg.node.DataNode;
import edu.usu.androidpm.cfg.node.OperationNode;
import edu.usu.androidpm.symbolic.ProgramEnvironment;

public class CheckCastInstruction extends InstructionExecutor {

	@Override
	public void execute(InstructionNode instructionNode, ProgramEnvironment env) {
		//obtain the instruction information
		Instruction21c instruction = (Instruction21c)instructionNode.getInstruction();
		int vA = instruction.getRegisterA();
		Reference reference = instruction.getReference();

		//create a operation node
		OperationNode operationNode = new OperationNode(env.getNodes().size(), instructionNode.getOffset(), instruction.getOpcode().value);
		env.addActionNode(operationNode);

		//update type
		DataNode dataNode = (DataNode)env.getNodes().get(env.getRegisterMap().get(vA));
		String outputType = ReferenceUtil.getReferenceString(reference);
		operationNode.setOutputType(dataNode.getType() + "->" + outputType);
		dataNode.setType(outputType);

		//add data edge
		operationNode.getDataEdges().add(dataNode.getID());
	}

}
