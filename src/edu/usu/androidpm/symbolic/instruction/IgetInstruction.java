package edu.usu.androidpm.symbolic.instruction;

import org.jf.dexlib2.Opcode;
import org.jf.dexlib2.iface.instruction.formats.Instruction22c;
import org.jf.dexlib2.iface.reference.FieldReference;
import edu.usu.androidpm.cfg.InstructionNode;
import edu.usu.androidpm.cfg.node.AccessorNode;
import edu.usu.androidpm.cfg.node.DataNode;
import edu.usu.androidpm.cfg.node.ObjectNode;
import edu.usu.androidpm.cfg.node.PrimitiveNode;
import edu.usu.androidpm.symbolic.ProgramEnvironment;

public class IgetInstruction extends InstructionExecutor {

	@Override
	public void execute(InstructionNode instructionNode, ProgramEnvironment env) {
		//obtain the instruction information
		Instruction22c instruction = (Instruction22c)instructionNode.getInstruction();
		int vA = instruction.getRegisterA();
		int vB = instruction.getRegisterB();
		FieldReference reference = (FieldReference)instruction.getReference();		

		//create a new accessor node
		AccessorNode accessorNode = new AccessorNode(env.getNodes().size(), instructionNode.getOffset(), instruction.getOpcode().value, reference);
		accessorNode.getInputRegisters().add(vB);
		accessorNode.setOutputRegister(vA);
		accessorNode.setOutputType(reference.getType());
		int hostObjectID = env.getRegisterMap().get(vB);
		accessorNode.setHostObjectID(hostObjectID);
		env.addActionNode(accessorNode);

		if (instruction.getOpcode() == Opcode.IGET_OBJECT) {
			ObjectNode objectNode = new ObjectNode(env.getNodes().size(), instructionNode.getOffset(), accessorNode.getOutputType(), accessorNode.getOutputRegister(), accessorNode.getID(), accessorNode.getHostObjectID());
			boolean isDuplicate = false;
			for (Integer i: env.getRegisterMultimap().get(accessorNode.getOutputRegister())) {
				if (env.objectNodesEqual(objectNode, (DataNode)env.getNodes().get(i))) {
					accessorNode.getDataEdges().add(i);
					env.getRegisterMap().put(accessorNode.getOutputRegister(), i);
					isDuplicate = true;
					break;
				}
			}
			if (!isDuplicate) {
				env.addDataNode(objectNode);
				accessorNode.getDataEdges().add(objectNode.getID());
			}
		} else {
			PrimitiveNode primitiveNode = new PrimitiveNode(env.getNodes().size(), instructionNode.getOffset(), accessorNode.getOutputType(), accessorNode.getOutputRegister(), accessorNode.getID(), accessorNode.getHostObjectID());
			env.addDataNode(primitiveNode);
			accessorNode.getDataEdges().add(primitiveNode.getID());
		}
	}

}
