package edu.usu.androidpm.symbolic.instruction;

import org.jf.dexlib2.Opcode;
import org.jf.dexlib2.iface.instruction.formats.Instruction23x;

import edu.usu.androidpm.cfg.InstructionNode;
import edu.usu.androidpm.cfg.node.DataNode;
import edu.usu.androidpm.cfg.node.ObjectNode;
import edu.usu.androidpm.cfg.node.OperationNode;
import edu.usu.androidpm.cfg.node.PrimitiveNode;
import edu.usu.androidpm.symbolic.ProgramEnvironment;

public class AgetInstruction extends InstructionExecutor {

	@Override
	public void execute(InstructionNode instructionNode, ProgramEnvironment env) {
		//obtain the instruction information
		Instruction23x instruction = (Instruction23x)instructionNode.getInstruction();
		int vA = instruction.getRegisterA();
		int vB = instruction.getRegisterB();
		int vC = instruction.getRegisterC();

		//create a new operation node
		OperationNode operationNode = new OperationNode(env.getNodes().size(), instructionNode.getOffset(), instruction.getOpcode().value);
		operationNode.getInputRegisters().add(vB);
		operationNode.getInputRegisters().add(vC);
		operationNode.setOutputRegister(vA);
		int hostObjectID = env.getRegisterMap().get(vB);
		operationNode.setHostObjectID(hostObjectID);
		DataNode dataNode = (DataNode)env.getNodes().get(hostObjectID);
		if (dataNode.getType().length() == 1) {
			operationNode.setOutputType(dataNode.getType());
		} else {
			operationNode.setOutputType(dataNode.getType().substring(1));
		}
		env.addActionNode(operationNode);

		if (instruction.getOpcode() == Opcode.AGET_OBJECT) {
			//if the type is an object type
			ObjectNode objectNode = new ObjectNode(env.getNodes().size(), instructionNode.getOffset(), operationNode.getOutputType(), operationNode.getOutputRegister(), operationNode.getID(), operationNode.getHostObjectID());
			boolean isDuplicate = false;
			for (Integer i: env.getRegisterMultimap().get(operationNode.getOutputRegister())) {
				if (env.objectNodesEqual(objectNode, (DataNode)env.getNodes().get(i))) {
					operationNode.getDataEdges().add(i);
					env.getRegisterMap().put(operationNode.getOutputRegister(), i);
					isDuplicate = true;
					break;
				}
			}
			if (!isDuplicate) {
				env.addDataNode(objectNode);
				operationNode.getDataEdges().add(objectNode.getID());
			}

		} else {
			//if the type is an primitive type
			PrimitiveNode primitiveNode = new PrimitiveNode(env.getNodes().size(), instructionNode.getOffset(), operationNode.getOutputType(), operationNode.getOutputRegister(), operationNode.getID());
			env.addDataNode(primitiveNode);
			operationNode.getDataEdges().add(primitiveNode.getID());
		}
	}

}
