package edu.usu.androidpm.symbolic.instruction;

import org.jf.dexlib2.iface.instruction.TwoRegisterInstruction;

import edu.usu.androidpm.cfg.InstructionNode;
import edu.usu.androidpm.cfg.node.DataNode;
import edu.usu.androidpm.cfg.node.OperationNode;
import edu.usu.androidpm.symbolic.ProgramEnvironment;

public class MoveInstruction extends InstructionExecutor {

	@Override
	public void execute(InstructionNode instructionNode, ProgramEnvironment env) {
		//obtain the instruction information
		TwoRegisterInstruction instruction = (TwoRegisterInstruction)instructionNode.getInstruction();
		int vA = instruction.getRegisterA();
		int vB = instruction.getRegisterB();
		
		//create an operation node and add to the program environment
		OperationNode operationNode = new OperationNode(env.getNodes().size(), instructionNode.getOffset(), instruction.getOpcode().value);
		env.getNodes().add(operationNode);
		
		//update the register map and the register multimap
		env.getRegisterMap().put(vA, env.getRegisterMap().get(vB));
		env.getRegisterMultimap().put(vA, env.getRegisterMap().get(vB));
		
		//update registers for the data node associates with vB 
		DataNode dataNode = (DataNode)env.getNodes().get(env.getRegisterMap().get(vB));
		dataNode.getRegisters().add(vA);
	}

}
