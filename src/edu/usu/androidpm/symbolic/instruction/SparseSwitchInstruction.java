package edu.usu.androidpm.symbolic.instruction;

import org.jf.dexlib2.iface.instruction.formats.Instruction31t;

import edu.usu.androidpm.cfg.InstructionNode;
import edu.usu.androidpm.cfg.node.ControlNode;
import edu.usu.androidpm.symbolic.ProgramEnvironment;

public class SparseSwitchInstruction extends InstructionExecutor {

	@Override
	public void execute(InstructionNode instructionNode, ProgramEnvironment env) {
		//obtain the instruction information
		Instruction31t instruction = (Instruction31t)instructionNode.getInstruction();
		int vA = instruction.getRegisterA();
		
		//create a new control node
		ControlNode controlNode = new ControlNode(env.getNodes().size(), instructionNode.getOffset(), instruction.getOpcode().value);
		controlNode.getInputRegisters().add(vA);
		env.addControlNode(controlNode);		
	}

}
