package edu.usu.androidpm.symbolic.instruction;

import org.jf.dexlib2.iface.instruction.OneRegisterInstruction;

import edu.usu.androidpm.cfg.InstructionNode;
import edu.usu.androidpm.cfg.node.ActionNode;
import edu.usu.androidpm.cfg.node.DataNode;
import edu.usu.androidpm.cfg.node.MethodNode;
import edu.usu.androidpm.cfg.node.ObjectNode;
import edu.usu.androidpm.symbolic.ProgramEnvironment;

public class MoveResultObjectInstruction extends InstructionExecutor {

	@Override
	public void execute(InstructionNode instructionNode, ProgramEnvironment env) {
		//obtain the instruction information
		OneRegisterInstruction instruction = (OneRegisterInstruction)instructionNode.getInstruction();
		int vA = instruction.getRegisterA();

		//update the output register of the action node before this instruction
		ActionNode actionNode = (ActionNode)env.getNodes().get(env.getLastNodeID());
		actionNode.setOutputRegister(vA);

		//check whether the return object is the same as the host object
		//in this situation, we don't create a new object.
		if (actionNode instanceof MethodNode) {
			MethodNode methodNode = (MethodNode) actionNode;
			if (methodNode.getHostObjectID() != -1) {
				if (methodNode.getInputRegisters().get(0) == methodNode.getOutputRegister()) {
					DataNode objectNode = (DataNode)env.getNodes().get(env.getRegisterMap().get(methodNode.getInputRegisters().get(0)));
					if (objectNode.getType().equals(methodNode.getOutputType())) {
						methodNode.getDataEdges().add(objectNode.getID());
						return;
					}
				}
			}
		}
		
		//create a new object node if needed
		ObjectNode objectNode = new ObjectNode(env.getNodes().size(), instructionNode.getOffset(), actionNode.getOutputType(), actionNode.getOutputRegister(), actionNode.getID(), actionNode.getHostObjectID());
		boolean isDuplicate = false;
		for (Integer i: env.getRegisterMultimap().get(actionNode.getOutputRegister())) {
			if (env.objectNodesEqual(objectNode, (DataNode)env.getNodes().get(i))) {
				actionNode.getDataEdges().add(i);
				env.getRegisterMap().put(actionNode.getOutputRegister(), i);
				isDuplicate = true;
				break;
			}
		}
		if (!isDuplicate) {
			env.addDataNode(objectNode);
			actionNode.getDataEdges().add(objectNode.getID());
		}

	}

}
