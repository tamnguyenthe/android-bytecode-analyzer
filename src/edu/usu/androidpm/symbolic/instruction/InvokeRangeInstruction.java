package edu.usu.androidpm.symbolic.instruction;

import org.jf.dexlib2.iface.instruction.formats.Instruction3rc;
import org.jf.dexlib2.iface.reference.MethodReference;
import org.jf.dexlib2.util.InstructionUtil;

import edu.usu.androidpm.cfg.InstructionNode;
import edu.usu.androidpm.cfg.node.MethodNode;
import edu.usu.androidpm.symbolic.ProgramEnvironment;

public class InvokeRangeInstruction extends InstructionExecutor {

	@Override
	public void execute(InstructionNode instructionNode, ProgramEnvironment env) {
		Instruction3rc instruction = (Instruction3rc)instructionNode.getInstruction();
		int registerCount = instruction.getRegisterCount();
		int startRegister = instruction.getStartRegister();
		MethodReference methodReference = (MethodReference)instruction.getReference();

		//create a new operation node
		MethodNode methodNode = new MethodNode(env.getNodes().size(), instructionNode.getOffset(), instruction.getOpcode().value, methodReference);
		for (int i = 0; i < registerCount; i++) {
			methodNode.getInputRegisters().add(i+startRegister);
		}
		methodNode.setOutputType(methodReference.getReturnType());
		if (!InstructionUtil.isInvokeStatic(instruction.getOpcode())) {
			methodNode.setHostObjectID(env.getRegisterMap().get(methodNode.getInputRegisters().get(0)));
		}
		env.addActionNode(methodNode);	
	}

}
