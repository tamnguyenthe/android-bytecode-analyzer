package edu.usu.androidpm.symbolic.instruction;

import edu.usu.androidpm.cfg.InstructionNode;
import edu.usu.androidpm.symbolic.ProgramEnvironment;

public abstract class InstructionExecutor {
	public abstract void execute(InstructionNode instructionNode, ProgramEnvironment env);
}
