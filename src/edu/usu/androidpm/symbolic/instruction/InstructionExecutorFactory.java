package edu.usu.androidpm.symbolic.instruction;

import java.util.EnumMap;

import org.jf.dexlib2.Opcode;

public class InstructionExecutorFactory {
	private EnumMap<Opcode, InstructionExecutor> instructionExecutorMap;
	public EnumMap<Opcode, InstructionExecutor> getInstructionExecutorMap() {
		return instructionExecutorMap;
	}
	public InstructionExecutorFactory() {
		instructionExecutorMap = new EnumMap<>(Opcode.class);
		MoveInstruction moveInstruction = new MoveInstruction();
		MoveWideInstruction moveWideInstruction = new MoveWideInstruction();
		MoveResultInstruction moveResultInstruction = new MoveResultInstruction();
		MoveResultObjectInstruction moveResultObjectInstruction = new MoveResultObjectInstruction();
		MoveExceptionInstruction moveExceptionInstruction = new MoveExceptionInstruction();
		ReturnInstruction returnInstruction = new ReturnInstruction();
		ConstInstruction constInstruction = new ConstInstruction();
		ConstStringInstruction constStringInstruction = new ConstStringInstruction();
		ConstClassInstruction constClassInstruction = new ConstClassInstruction();
		CheckCastInstruction checkCastInstruction = new CheckCastInstruction();
		InstanceOfInstruction instanceOfInstruction = new InstanceOfInstruction();
		ArrayLengthInstruction arrayLengthInstruction = new ArrayLengthInstruction();
		NewInstanceInstruction newInstanceInstruction = new NewInstanceInstruction();
		NewArrayInstruction newArrayInstruction = new NewArrayInstruction();
		FilledNewArrayInstruction filledArrayInstruction = new FilledNewArrayInstruction();
		FilledNewArrayRangeInstruction filledArrayRangeInstruction = new FilledNewArrayRangeInstruction();
		FillArrayDataInstruction fillArrayDataInstruction = new FillArrayDataInstruction();
		GotoInstruction gotoInstruction = new GotoInstruction();
		PackedSwitchInstruction packedSwitchInstruction = new PackedSwitchInstruction();
		SparseSwitchInstruction sparseSwitchInstruction = new SparseSwitchInstruction();
		CompareInstruction compareInstruction = new CompareInstruction();
		IfInstruction ifInstruction = new IfInstruction();
		IfZInstruction ifZInstruction = new IfZInstruction();
		AgetInstruction arrayGetInstruction = new AgetInstruction();
		AputInstruction arraPutInstruction = new AputInstruction();
		IgetInstruction instanceGetInstruction = new IgetInstruction();
		IputInstruction instancePutInstruction = new IputInstruction();
		SgetInstruction staticGetInstruction = new SgetInstruction();
		SputInstruction staticPutInstruction = new SputInstruction();
		InvokeInstruction invokeInstruction = new InvokeInstruction();
		InvokeRangeInstruction invokeRangeInstruction = new InvokeRangeInstruction();
		UnopInstruction unopInstruction = new UnopInstruction();
		BinopInstruction binopInstruction = new BinopInstruction();
		Binop2AddrInstruction binop2addrInstruction = new Binop2AddrInstruction();
		BinopLitInstruction binopLitInstruction = new BinopLitInstruction();
		
		//nop
		instructionExecutorMap.put(Opcode.NOP, gotoInstruction); //does noting like goto
		
		//move
		instructionExecutorMap.put(Opcode.MOVE, moveInstruction);
		instructionExecutorMap.put(Opcode.MOVE_FROM16, moveInstruction);
		instructionExecutorMap.put(Opcode.MOVE_16, moveInstruction);
		instructionExecutorMap.put(Opcode.MOVE_WIDE, moveWideInstruction);
		instructionExecutorMap.put(Opcode.MOVE_WIDE_FROM16, moveWideInstruction);
		instructionExecutorMap.put(Opcode.MOVE_WIDE_16, moveWideInstruction);
		instructionExecutorMap.put(Opcode.MOVE_OBJECT, moveInstruction);
		instructionExecutorMap.put(Opcode.MOVE_OBJECT_FROM16, moveInstruction);
		instructionExecutorMap.put(Opcode.MOVE_OBJECT_16, moveInstruction);
		
		//move-result
		instructionExecutorMap.put(Opcode.MOVE_RESULT, moveResultInstruction);
		instructionExecutorMap.put(Opcode.MOVE_RESULT_WIDE, moveResultInstruction);
		instructionExecutorMap.put(Opcode.MOVE_RESULT_OBJECT, moveResultObjectInstruction);
		
		//move-exception
		instructionExecutorMap.put(Opcode.MOVE_EXCEPTION, moveExceptionInstruction);
		
		//return
		instructionExecutorMap.put(Opcode.RETURN_VOID, returnInstruction);
		instructionExecutorMap.put(Opcode.RETURN, returnInstruction);
		instructionExecutorMap.put(Opcode.RETURN_WIDE, returnInstruction);
		instructionExecutorMap.put(Opcode.RETURN_OBJECT, returnInstruction);
		
		//const
		instructionExecutorMap.put(Opcode.CONST_4,constInstruction);
		instructionExecutorMap.put(Opcode.CONST_16,constInstruction);
		instructionExecutorMap.put(Opcode.CONST,constInstruction);
		instructionExecutorMap.put(Opcode.CONST_HIGH16,constInstruction);
		instructionExecutorMap.put(Opcode.CONST_WIDE_16,constInstruction);
		instructionExecutorMap.put(Opcode.CONST_WIDE_32,constInstruction);
		instructionExecutorMap.put(Opcode.CONST_WIDE,constInstruction);
		instructionExecutorMap.put(Opcode.CONST_WIDE_HIGH16,constInstruction);
		
		instructionExecutorMap.put(Opcode.CONST_STRING, constStringInstruction);
		instructionExecutorMap.put(Opcode.CONST_STRING_JUMBO, constStringInstruction);
		
		//const-class
		instructionExecutorMap.put(Opcode.CONST_CLASS, constClassInstruction);
		//monitor-enter
		instructionExecutorMap.put(Opcode.MONITOR_ENTER, gotoInstruction);
		instructionExecutorMap.put(Opcode.MONITOR_EXIT, gotoInstruction);
		//check-cast
		instructionExecutorMap.put(Opcode.CHECK_CAST, checkCastInstruction);
		//instance-of
		instructionExecutorMap.put(Opcode.INSTANCE_OF, instanceOfInstruction);
		//array-length
		instructionExecutorMap.put(Opcode.ARRAY_LENGTH, arrayLengthInstruction);
		
		instructionExecutorMap.put(Opcode.NEW_INSTANCE, newInstanceInstruction);
		
		//new-array
		instructionExecutorMap.put(Opcode.NEW_ARRAY, newArrayInstruction);
		
		
		//filled-new-array
		instructionExecutorMap.put(Opcode.FILLED_NEW_ARRAY, filledArrayInstruction);
		//filled-new-array/range
		instructionExecutorMap.put(Opcode.FILLED_NEW_ARRAY_RANGE, filledArrayRangeInstruction);
		//fill-array-data
		instructionExecutorMap.put(Opcode.FILL_ARRAY_DATA, fillArrayDataInstruction);
		
		
		//throw
		instructionExecutorMap.put(Opcode.THROW, gotoInstruction);
		
		instructionExecutorMap.put(Opcode.GOTO, gotoInstruction);
		instructionExecutorMap.put(Opcode.GOTO_16, gotoInstruction);
		instructionExecutorMap.put(Opcode.GOTO_32, gotoInstruction);
		
		//packed-switch
		instructionExecutorMap.put(Opcode.PACKED_SWITCH, packedSwitchInstruction);
		//sparse-switch
		instructionExecutorMap.put(Opcode.SPARSE_SWITCH, sparseSwitchInstruction);
		
		instructionExecutorMap.put(Opcode.CMPL_FLOAT, compareInstruction);
		instructionExecutorMap.put(Opcode.CMPG_FLOAT, compareInstruction);
		instructionExecutorMap.put(Opcode.CMPL_DOUBLE, compareInstruction);
		instructionExecutorMap.put(Opcode.CMPG_DOUBLE, compareInstruction);
		instructionExecutorMap.put(Opcode.CMP_LONG, compareInstruction);
		
		instructionExecutorMap.put(Opcode.IF_EQ, ifInstruction);
		instructionExecutorMap.put(Opcode.IF_NE, ifInstruction);
		instructionExecutorMap.put(Opcode.IF_LT, ifInstruction);
		instructionExecutorMap.put(Opcode.IF_GE, ifInstruction);
		instructionExecutorMap.put(Opcode.IF_GT, ifInstruction);
		instructionExecutorMap.put(Opcode.IF_LE, ifInstruction);
		
		instructionExecutorMap.put(Opcode.IF_EQZ, ifZInstruction);
		instructionExecutorMap.put(Opcode.IF_NEZ, ifZInstruction);
		instructionExecutorMap.put(Opcode.IF_LTZ, ifZInstruction);
		instructionExecutorMap.put(Opcode.IF_GEZ, ifZInstruction);
		instructionExecutorMap.put(Opcode.IF_GTZ, ifZInstruction);
		instructionExecutorMap.put(Opcode.IF_LEZ, ifZInstruction);
		
		//aget...
		instructionExecutorMap.put(Opcode.AGET, arrayGetInstruction);
		instructionExecutorMap.put(Opcode.AGET_WIDE, arrayGetInstruction);
		instructionExecutorMap.put(Opcode.AGET_OBJECT, arrayGetInstruction);
		instructionExecutorMap.put(Opcode.AGET_BOOLEAN, arrayGetInstruction);
		instructionExecutorMap.put(Opcode.AGET_BYTE, arrayGetInstruction);
		instructionExecutorMap.put(Opcode.AGET_CHAR, arrayGetInstruction);
		instructionExecutorMap.put(Opcode.AGET_SHORT, arrayGetInstruction);
		//aput...
		instructionExecutorMap.put(Opcode.APUT, arraPutInstruction);
		instructionExecutorMap.put(Opcode.APUT_WIDE, arraPutInstruction);
		instructionExecutorMap.put(Opcode.APUT_OBJECT, arraPutInstruction);
		instructionExecutorMap.put(Opcode.APUT_BOOLEAN, arraPutInstruction);
		instructionExecutorMap.put(Opcode.APUT_BYTE, arraPutInstruction);
		instructionExecutorMap.put(Opcode.APUT_CHAR, arraPutInstruction);
		instructionExecutorMap.put(Opcode.APUT_SHORT, arraPutInstruction);
		
		
		instructionExecutorMap.put(Opcode.IGET, instanceGetInstruction);
		instructionExecutorMap.put(Opcode.IGET_WIDE, instanceGetInstruction);
		instructionExecutorMap.put(Opcode.IGET_OBJECT, instanceGetInstruction);
		instructionExecutorMap.put(Opcode.IGET_BOOLEAN, instanceGetInstruction);
		instructionExecutorMap.put(Opcode.IGET_BYTE, instanceGetInstruction);
		instructionExecutorMap.put(Opcode.IGET_CHAR, instanceGetInstruction);
		instructionExecutorMap.put(Opcode.IGET_SHORT, instanceGetInstruction);
		
		instructionExecutorMap.put(Opcode.IPUT, instancePutInstruction);
		instructionExecutorMap.put(Opcode.IPUT_WIDE, instancePutInstruction);
		instructionExecutorMap.put(Opcode.IPUT_OBJECT, instancePutInstruction);
		instructionExecutorMap.put(Opcode.IPUT_BOOLEAN, instancePutInstruction);
		instructionExecutorMap.put(Opcode.IPUT_BYTE, instancePutInstruction);
		instructionExecutorMap.put(Opcode.IPUT_CHAR, instancePutInstruction);
		instructionExecutorMap.put(Opcode.IPUT_SHORT, instancePutInstruction);
		
		instructionExecutorMap.put(Opcode.SGET, staticGetInstruction);
		instructionExecutorMap.put(Opcode.SGET_WIDE, staticGetInstruction);
		instructionExecutorMap.put(Opcode.SGET_OBJECT, staticGetInstruction);
		instructionExecutorMap.put(Opcode.SGET_BOOLEAN, staticGetInstruction);
		instructionExecutorMap.put(Opcode.SGET_BYTE, staticGetInstruction);
		instructionExecutorMap.put(Opcode.SGET_CHAR, staticGetInstruction);
		instructionExecutorMap.put(Opcode.SGET_SHORT, staticGetInstruction);
		
		instructionExecutorMap.put(Opcode.SPUT, staticPutInstruction);
		instructionExecutorMap.put(Opcode.SPUT_WIDE, staticPutInstruction);
		instructionExecutorMap.put(Opcode.SPUT_OBJECT, staticPutInstruction);
		instructionExecutorMap.put(Opcode.SPUT_BOOLEAN, staticPutInstruction);
		instructionExecutorMap.put(Opcode.SPUT_BYTE, staticPutInstruction);
		instructionExecutorMap.put(Opcode.SPUT_CHAR, staticPutInstruction);
		instructionExecutorMap.put(Opcode.SPUT_SHORT, staticPutInstruction);
		
		instructionExecutorMap.put(Opcode.INVOKE_VIRTUAL, invokeInstruction);
		instructionExecutorMap.put(Opcode.INVOKE_SUPER, invokeInstruction);
		instructionExecutorMap.put(Opcode.INVOKE_DIRECT, invokeInstruction);
		instructionExecutorMap.put(Opcode.INVOKE_STATIC, invokeInstruction);
		instructionExecutorMap.put(Opcode.INVOKE_INTERFACE, invokeInstruction);
		
		instructionExecutorMap.put(Opcode.INVOKE_VIRTUAL_RANGE, invokeRangeInstruction);
		instructionExecutorMap.put(Opcode.INVOKE_SUPER_RANGE, invokeRangeInstruction);
		instructionExecutorMap.put(Opcode.INVOKE_DIRECT_RANGE, invokeRangeInstruction);
		instructionExecutorMap.put(Opcode.INVOKE_STATIC_RANGE, invokeRangeInstruction);
		instructionExecutorMap.put(Opcode.INVOKE_INTERFACE_RANGE, invokeRangeInstruction);
		
		instructionExecutorMap.put(Opcode.NEG_INT, unopInstruction);
		instructionExecutorMap.put(Opcode.NOT_INT, unopInstruction);
		instructionExecutorMap.put(Opcode.NEG_LONG, unopInstruction);
		instructionExecutorMap.put(Opcode.NOT_LONG, unopInstruction);
		instructionExecutorMap.put(Opcode.NEG_FLOAT, unopInstruction);
		instructionExecutorMap.put(Opcode.NEG_DOUBLE, unopInstruction);
		instructionExecutorMap.put(Opcode.INT_TO_LONG, unopInstruction);
		instructionExecutorMap.put(Opcode.INT_TO_FLOAT, unopInstruction);
		instructionExecutorMap.put(Opcode.INT_TO_DOUBLE, unopInstruction);
		instructionExecutorMap.put(Opcode.LONG_TO_INT, unopInstruction);
		instructionExecutorMap.put(Opcode.LONG_TO_FLOAT, unopInstruction);
		instructionExecutorMap.put(Opcode.LONG_TO_DOUBLE, unopInstruction);
		instructionExecutorMap.put(Opcode.FLOAT_TO_INT, unopInstruction);
		instructionExecutorMap.put(Opcode.FLOAT_TO_LONG, unopInstruction);
		instructionExecutorMap.put(Opcode.FLOAT_TO_DOUBLE, unopInstruction);
		instructionExecutorMap.put(Opcode.DOUBLE_TO_INT, unopInstruction);
		instructionExecutorMap.put(Opcode.DOUBLE_TO_LONG, unopInstruction);
		instructionExecutorMap.put(Opcode.DOUBLE_TO_FLOAT, unopInstruction);
		instructionExecutorMap.put(Opcode.INT_TO_BYTE, unopInstruction);
		instructionExecutorMap.put(Opcode.INT_TO_CHAR, unopInstruction);
		instructionExecutorMap.put(Opcode.INT_TO_SHORT, unopInstruction);
		
		instructionExecutorMap.put(Opcode.ADD_INT, binopInstruction);
		instructionExecutorMap.put(Opcode.SUB_INT, binopInstruction);
		instructionExecutorMap.put(Opcode.MUL_INT, binopInstruction);
		instructionExecutorMap.put(Opcode.DIV_INT, binopInstruction);
		instructionExecutorMap.put(Opcode.REM_INT, binopInstruction);
		instructionExecutorMap.put(Opcode.AND_INT, binopInstruction);
		instructionExecutorMap.put(Opcode.OR_INT, binopInstruction);
		instructionExecutorMap.put(Opcode.XOR_INT, binopInstruction);
		instructionExecutorMap.put(Opcode.SHL_INT, binopInstruction);
		instructionExecutorMap.put(Opcode.SHR_INT, binopInstruction);
		instructionExecutorMap.put(Opcode.USHR_INT, binopInstruction);
		instructionExecutorMap.put(Opcode.ADD_LONG, binopInstruction);
		instructionExecutorMap.put(Opcode.SUB_LONG, binopInstruction);
		instructionExecutorMap.put(Opcode.MUL_LONG, binopInstruction);
		instructionExecutorMap.put(Opcode.DIV_LONG, binopInstruction);
		instructionExecutorMap.put(Opcode.REM_LONG, binopInstruction);
		instructionExecutorMap.put(Opcode.AND_LONG, binopInstruction);
		instructionExecutorMap.put(Opcode.OR_LONG, binopInstruction);
		instructionExecutorMap.put(Opcode.XOR_LONG, binopInstruction);
		instructionExecutorMap.put(Opcode.SHL_LONG, binopInstruction);
		instructionExecutorMap.put(Opcode.SHR_LONG, binopInstruction);
		instructionExecutorMap.put(Opcode.USHR_LONG, binopInstruction);
		instructionExecutorMap.put(Opcode.ADD_FLOAT, binopInstruction);
		instructionExecutorMap.put(Opcode.SUB_FLOAT, binopInstruction);
		instructionExecutorMap.put(Opcode.MUL_FLOAT, binopInstruction);
		instructionExecutorMap.put(Opcode.DIV_FLOAT, binopInstruction);
		instructionExecutorMap.put(Opcode.REM_FLOAT, binopInstruction);
		instructionExecutorMap.put(Opcode.ADD_DOUBLE, binopInstruction);
		instructionExecutorMap.put(Opcode.SUB_DOUBLE, binopInstruction);
		instructionExecutorMap.put(Opcode.MUL_DOUBLE, binopInstruction);
		instructionExecutorMap.put(Opcode.DIV_DOUBLE, binopInstruction);
		instructionExecutorMap.put(Opcode.REM_DOUBLE, binopInstruction);
		
		instructionExecutorMap.put(Opcode.ADD_INT_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.SUB_INT_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.MUL_INT_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.DIV_INT_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.REM_INT_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.AND_INT_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.OR_INT_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.XOR_INT_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.SHL_INT_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.SHR_INT_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.USHR_INT_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.ADD_LONG_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.SUB_LONG_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.MUL_LONG_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.DIV_LONG_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.REM_LONG_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.AND_LONG_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.OR_LONG_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.XOR_LONG_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.SHL_LONG_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.SHR_LONG_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.USHR_LONG_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.ADD_FLOAT_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.SUB_FLOAT_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.MUL_FLOAT_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.DIV_FLOAT_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.REM_FLOAT_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.ADD_DOUBLE_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.SUB_DOUBLE_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.MUL_DOUBLE_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.DIV_DOUBLE_2ADDR, binop2addrInstruction);
		instructionExecutorMap.put(Opcode.REM_DOUBLE_2ADDR, binop2addrInstruction);
		
		instructionExecutorMap.put(Opcode.ADD_INT_LIT16, binopLitInstruction);
		instructionExecutorMap.put(Opcode.RSUB_INT, binopLitInstruction);
		instructionExecutorMap.put(Opcode.MUL_INT_LIT16, binopLitInstruction);
		instructionExecutorMap.put(Opcode.DIV_INT_LIT16, binopLitInstruction);
		instructionExecutorMap.put(Opcode.REM_INT_LIT16, binopLitInstruction);
		instructionExecutorMap.put(Opcode.AND_INT_LIT16, binopLitInstruction);
		instructionExecutorMap.put(Opcode.OR_INT_LIT16, binopLitInstruction);
		instructionExecutorMap.put(Opcode.XOR_INT_LIT16, binopLitInstruction);
		
		instructionExecutorMap.put(Opcode.ADD_INT_LIT8, binopLitInstruction);
		instructionExecutorMap.put(Opcode.RSUB_INT_LIT8, binopLitInstruction);
		instructionExecutorMap.put(Opcode.MUL_INT_LIT8, binopLitInstruction);
		instructionExecutorMap.put(Opcode.DIV_INT_LIT8, binopLitInstruction);
		instructionExecutorMap.put(Opcode.REM_INT_LIT8, binopLitInstruction);
		instructionExecutorMap.put(Opcode.AND_INT_LIT8, binopLitInstruction);
		instructionExecutorMap.put(Opcode.OR_INT_LIT8, binopLitInstruction);
		instructionExecutorMap.put(Opcode.XOR_INT_LIT8, binopLitInstruction);
		instructionExecutorMap.put(Opcode.SHL_INT_LIT8, binopLitInstruction);
		instructionExecutorMap.put(Opcode.SHR_INT_LIT8, binopLitInstruction);
		instructionExecutorMap.put(Opcode.USHR_INT_LIT8, binopLitInstruction);
		
	}

}
