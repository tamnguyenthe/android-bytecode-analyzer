package edu.usu.androidpm.symbolic.instruction;

import org.jf.dexlib2.iface.instruction.formats.Instruction21c;
import org.jf.dexlib2.iface.reference.Reference;
import org.jf.dexlib2.util.ReferenceUtil;

import edu.usu.androidpm.cfg.InstructionNode;
import edu.usu.androidpm.cfg.node.ObjectNode;
import edu.usu.androidpm.cfg.node.OperationNode;
import edu.usu.androidpm.symbolic.ProgramEnvironment;

public class NewInstanceInstruction extends InstructionExecutor {

	@Override
	public void execute(InstructionNode instructionNode, ProgramEnvironment env) {
		//obtain the instruction information
		Instruction21c instruction = (Instruction21c)instructionNode.getInstruction();
		int vA = instruction.getRegisterA();
		Reference reference = instruction.getReference();
		
		//create a new operation node
		OperationNode operationNode = new OperationNode(env.getNodes().size(), instructionNode.getOffset(), instruction.getOpcode().value);
		operationNode.setOutputRegister(vA);
		operationNode.setOutputType(ReferenceUtil.getReferenceString(reference));
		//env.addActionNode(operationNode);

		//create a new object node
		ObjectNode objectNode = new ObjectNode(env.getNodes().size(), instructionNode.getOffset(), operationNode.getOutputType(), operationNode.getOutputRegister(), operationNode.getID());
		env.addDataNode(objectNode);

		//add data edge
		//operationNode.getDataEdges().add(objectNode.getID());		
	}

}
