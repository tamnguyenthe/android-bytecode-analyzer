package edu.usu.androidpm.symbolic.instruction;

import org.jf.dexlib2.iface.instruction.formats.Instruction35c;
import org.jf.dexlib2.iface.reference.MethodReference;
import org.jf.dexlib2.util.InstructionUtil;
import edu.usu.androidpm.cfg.InstructionNode;
import edu.usu.androidpm.cfg.node.MethodNode;
import edu.usu.androidpm.symbolic.ProgramEnvironment;

public class InvokeInstruction extends InstructionExecutor {

	@Override
	public void execute(InstructionNode instructionNode, ProgramEnvironment env) {
		Instruction35c instruction = (Instruction35c)instructionNode.getInstruction();
		int registerCount = instruction.getRegisterCount();
		MethodReference methodReference = (MethodReference)instruction.getReference();

		//create a new operation node
		MethodNode methodNode = new MethodNode(env.getNodes().size(), instructionNode.getOffset(), instruction.getOpcode().value, methodReference);
		switch (registerCount) {
		case 1:
			methodNode.getInputRegisters().add(instruction.getRegisterC());
			break;
		case 2: 
			methodNode.getInputRegisters().add(instruction.getRegisterC());
			methodNode.getInputRegisters().add(instruction.getRegisterD());
			break;
		case 3: 
			methodNode.getInputRegisters().add(instruction.getRegisterC());
			methodNode.getInputRegisters().add(instruction.getRegisterD());
			methodNode.getInputRegisters().add(instruction.getRegisterE());
			break;
		case 4: 
			methodNode.getInputRegisters().add(instruction.getRegisterC());
			methodNode.getInputRegisters().add(instruction.getRegisterD());
			methodNode.getInputRegisters().add(instruction.getRegisterE());
			methodNode.getInputRegisters().add(instruction.getRegisterF());
			break;
		case 5: 
			methodNode.getInputRegisters().add(instruction.getRegisterC());
			methodNode.getInputRegisters().add(instruction.getRegisterD());
			methodNode.getInputRegisters().add(instruction.getRegisterE());
			methodNode.getInputRegisters().add(instruction.getRegisterF());
			methodNode.getInputRegisters().add(instruction.getRegisterG());
			break;
		default:
			break;
		}
		methodNode.setOutputType(methodReference.getReturnType());
		if (!InstructionUtil.isInvokeStatic(instruction.getOpcode())) {
			methodNode.setHostObjectID(env.getRegisterMap().get(methodNode.getInputRegisters().get(0)));
		}
		env.addActionNode(methodNode);
	}

}
