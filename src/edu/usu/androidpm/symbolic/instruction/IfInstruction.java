package edu.usu.androidpm.symbolic.instruction;

import org.jf.dexlib2.iface.instruction.formats.Instruction22t;

import edu.usu.androidpm.cfg.InstructionNode;
import edu.usu.androidpm.cfg.node.ControlNode;
import edu.usu.androidpm.symbolic.ProgramEnvironment;

public class IfInstruction extends InstructionExecutor {

	@Override
	public void execute(InstructionNode instructionNode, ProgramEnvironment env) {
		//obtain the instruction information
		Instruction22t instruction = (Instruction22t)instructionNode.getInstruction();
		int vA = instruction.getRegisterA();
		int vB = instruction.getRegisterB();

		//create a new control node
		ControlNode controlNode = new ControlNode(env.getNodes().size(), instructionNode.getOffset(), instruction.getOpcode().value);
		controlNode.getInputRegisters().add(vA);
		controlNode.getInputRegisters().add(vB);
		env.addControlNode(controlNode);
	}

}
