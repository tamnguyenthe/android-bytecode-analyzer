package edu.usu.androidpm.symbolic.instruction;

import org.jf.dexlib2.iface.instruction.formats.Instruction22c;

import edu.usu.androidpm.cfg.InstructionNode;
import edu.usu.androidpm.cfg.node.OperationNode;
import edu.usu.androidpm.cfg.node.PrimitiveNode;
import edu.usu.androidpm.symbolic.ProgramEnvironment;
import edu.usu.androidpm.util.PrimitiveType;

public class InstanceOfInstruction extends InstructionExecutor {

	@Override
	public void execute(InstructionNode instructionNode, ProgramEnvironment env) {
		//obtain the instruction information
		Instruction22c instruction = (Instruction22c)instructionNode.getInstruction();
		int vA = instruction.getRegisterA();
		int vB = instruction.getRegisterB();
		//Reference reference = instruction.getReference();

		//create a operation node
		OperationNode operationNode = new OperationNode(env.getNodes().size(), instructionNode.getOffset(), instruction.getOpcode().value);
		operationNode.getInputRegisters().add(vB);
		operationNode.setOutputRegister(vA);
		operationNode.setOutputType(PrimitiveType.INT);
		env.addActionNode(operationNode);

		//create a primitive node
		PrimitiveNode primitiveNode =  new PrimitiveNode(env.getNodes().size(), instructionNode.getOffset(), operationNode.getOutputType(), operationNode.getOutputRegister(), operationNode.getID());
		env.addDataNode(primitiveNode);

		//add the data edge
		operationNode.getDataEdges().add(primitiveNode.getID());		
	}

}
