package edu.usu.androidpm.symbolic.instruction;

import org.jf.dexlib2.iface.instruction.TwoRegisterInstruction;

import edu.usu.androidpm.cfg.InstructionNode;
import edu.usu.androidpm.cfg.node.OperationNode;
import edu.usu.androidpm.cfg.node.PrimitiveNode;
import edu.usu.androidpm.symbolic.ProgramEnvironment;
import edu.usu.androidpm.util.PrimitiveType;

public class BinopLitInstruction extends InstructionExecutor {

	@Override
	public void execute(InstructionNode instructionNode, ProgramEnvironment env) {
		//obtain the instruction information
		TwoRegisterInstruction instruction = (TwoRegisterInstruction)instructionNode.getInstruction();
		int vA = instruction.getRegisterA();
		int vB = instruction.getRegisterB();

		//create a new operation node
		OperationNode operationNode = new OperationNode(env.getNodes().size(), instructionNode.getOffset(), instruction.getOpcode().value);
		operationNode.getInputRegisters().add(vB);
		operationNode.setOutputRegister(vA);
		operationNode.setOutputType(PrimitiveType.getOutputTypeForOpcode(instruction.getOpcode()));
		env.addActionNode(operationNode);

		//create a new primitive node
		PrimitiveNode primitiveNode = new PrimitiveNode(env.getNodes().size(), instructionNode.getOffset(), operationNode.getOutputType(), operationNode.getOutputRegister(), operationNode.getID());
		env.addDataNode(primitiveNode);

		//add data edge
		operationNode.getDataEdges().add(primitiveNode.getID());		
	}

}
