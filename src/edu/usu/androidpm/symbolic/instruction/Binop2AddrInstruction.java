package edu.usu.androidpm.symbolic.instruction;

import org.jf.dexlib2.iface.instruction.formats.Instruction12x;

import edu.usu.androidpm.cfg.InstructionNode;
import edu.usu.androidpm.cfg.node.OperationNode;
import edu.usu.androidpm.cfg.node.PrimitiveNode;
import edu.usu.androidpm.symbolic.ProgramEnvironment;
import edu.usu.androidpm.util.PrimitiveType;

public class Binop2AddrInstruction extends InstructionExecutor {

	@Override
	public void execute(InstructionNode instructionNode, ProgramEnvironment env) {
		Instruction12x instruction = (Instruction12x)instructionNode.getInstruction();

		//create a new operation node
		OperationNode operationNode = new OperationNode(env.getNodes().size(), instructionNode.getOffset(), instruction.getOpcode().value);
		operationNode.getInputRegisters().add(instruction.getRegisterB());
		operationNode.getInputRegisters().add(instruction.getRegisterA());
		operationNode.setOutputRegister(instruction.getRegisterA());
		operationNode.setOutputType(PrimitiveType.getOutputTypeForOpcode(instruction.getOpcode()));
		env.addActionNode(operationNode);

		//create a new primitive node
		PrimitiveNode primitiveNode = new PrimitiveNode(env.getNodes().size(), instructionNode.getOffset(), operationNode.getOutputType(), operationNode.getOutputRegister(), operationNode.getID());
		env.addDataNode(primitiveNode);

		//add data edge
		operationNode.getDataEdges().add(primitiveNode.getID());				
	}

}
