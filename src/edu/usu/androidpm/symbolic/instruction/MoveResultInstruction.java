package edu.usu.androidpm.symbolic.instruction;

import org.jf.dexlib2.iface.instruction.OneRegisterInstruction;

import edu.usu.androidpm.cfg.InstructionNode;
import edu.usu.androidpm.cfg.node.ActionNode;
import edu.usu.androidpm.cfg.node.PrimitiveNode;
import edu.usu.androidpm.symbolic.ProgramEnvironment;

public class MoveResultInstruction extends InstructionExecutor {

	@Override
	public void execute(InstructionNode instructionNode, ProgramEnvironment env) {
		//obtain the instruction information
		OneRegisterInstruction instruction = (OneRegisterInstruction)instructionNode.getInstruction();
		int vA = instruction.getRegisterA();
		
		//update the output register of the action node before this instruction
		ActionNode actionNode = (ActionNode)env.getNodes().get(env.getLastNodeID());
		actionNode.setOutputRegister(vA);

		//create a new primitive node
		PrimitiveNode primitiveNode = new PrimitiveNode(env.getNodes().size(), instructionNode.getOffset(), 
				actionNode.getOutputType(), actionNode.getOutputRegister(), actionNode.getID(), actionNode.getHostObjectID());
		env.addDataNode(primitiveNode);
		
		//add data edges
		actionNode.getDataEdges().add(primitiveNode.getID());
	}

}
