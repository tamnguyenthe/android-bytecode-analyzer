package edu.usu.androidpm.symbolic.instruction;

import org.jf.dexlib2.iface.instruction.formats.Instruction35c;
import org.jf.dexlib2.iface.reference.Reference;
import org.jf.dexlib2.util.ReferenceUtil;

import edu.usu.androidpm.cfg.InstructionNode;
import edu.usu.androidpm.cfg.node.OperationNode;
import edu.usu.androidpm.symbolic.ProgramEnvironment;

public class FilledNewArrayInstruction extends InstructionExecutor {

	@Override
	public void execute(InstructionNode instructionNode, ProgramEnvironment env) {
		//obtain the instruction information
		Instruction35c instruction = (Instruction35c)instructionNode.getInstruction();
		Reference reference = instruction.getReference();
		
		//create a new operation node
		OperationNode operationNode = new OperationNode(env.getNodes().size(), instructionNode.getOffset(), instruction.getOpcode().value);
		int registerCount = instruction.getRegisterCount();
		switch (registerCount) {
		case 1:
			operationNode.getInputRegisters().add(instruction.getRegisterC());
			break;
		case 2: 
			operationNode.getInputRegisters().add(instruction.getRegisterC());
			operationNode.getInputRegisters().add(instruction.getRegisterD());
			break;
		case 3: 
			operationNode.getInputRegisters().add(instruction.getRegisterC());
			operationNode.getInputRegisters().add(instruction.getRegisterD());
			operationNode.getInputRegisters().add(instruction.getRegisterE());
			break;
		case 4: 
			operationNode.getInputRegisters().add(instruction.getRegisterC());
			operationNode.getInputRegisters().add(instruction.getRegisterD());
			operationNode.getInputRegisters().add(instruction.getRegisterE());
			operationNode.getInputRegisters().add(instruction.getRegisterF());
			break;
		case 5: 
			operationNode.getInputRegisters().add(instruction.getRegisterC());
			operationNode.getInputRegisters().add(instruction.getRegisterD());
			operationNode.getInputRegisters().add(instruction.getRegisterE());
			operationNode.getInputRegisters().add(instruction.getRegisterF());
			operationNode.getInputRegisters().add(instruction.getRegisterG());
			break;
		default:
			break;
		}
		String type = "["+ ReferenceUtil.getReferenceString(reference);
		operationNode.setOutputType(type);
		env.addActionNode(operationNode);		
	}

}
