package edu.usu.androidpm.symbolic.instruction;

import org.jf.dexlib2.iface.instruction.formats.Instruction21c;
import org.jf.dexlib2.iface.reference.FieldReference;
import edu.usu.androidpm.cfg.InstructionNode;
import edu.usu.androidpm.cfg.node.AccessorNode;
import edu.usu.androidpm.symbolic.ProgramEnvironment;
import edu.usu.androidpm.util.PrimitiveType;

public class SputInstruction extends InstructionExecutor {

	@Override
	public void execute(InstructionNode instructionNode, ProgramEnvironment env) {
		//obtain the instruction information
		Instruction21c instruction = (Instruction21c)instructionNode.getInstruction();
		int vA = instruction.getRegisterA();
		FieldReference reference = (FieldReference)instruction.getReference();

		//create a new accessor node
		AccessorNode accessorNode = new AccessorNode(env.getNodes().size(), instructionNode.getOffset(), instruction.getOpcode().value, reference);
		accessorNode.getInputRegisters().add(vA);
		accessorNode.setOutputType(PrimitiveType.VOID);
		env.addActionNode(accessorNode);		
	}

}
