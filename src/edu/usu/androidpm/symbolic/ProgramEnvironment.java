package edu.usu.androidpm.symbolic;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.jf.dexlib2.util.TypeUtils;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import edu.usu.androidpm.cfg.node.AccessorNode;
import edu.usu.androidpm.cfg.node.ActionNode;
import edu.usu.androidpm.cfg.node.ControlNode;
import edu.usu.androidpm.cfg.node.DataNode;
import edu.usu.androidpm.cfg.node.MethodNode;
import edu.usu.androidpm.cfg.node.Node;
import edu.usu.androidpm.cfg.node.ObjectNode;
import edu.usu.androidpm.util.Constants;
import edu.usu.androidpm.util.GraphViz;
import edu.usu.androidpm.util.PackageUtil;

public class ProgramEnvironment {
	private List<Node> nodes;
	private Map<Integer, Integer> registerMap;
	private Multimap<Integer, Integer> registerMultimap;
	private int lastNodeID;
	private Set<Integer> exploredPaths;

	public ProgramEnvironment() {
		nodes = new ArrayList<>();
		registerMap = new HashMap<>();
		registerMultimap = HashMultimap.create();
		lastNodeID = -1;
		exploredPaths = new HashSet<>();
	}

	public List<Node> getNodes() {
		return nodes;
	}

	public Map<Integer, Integer> getRegisterMap() {
		return registerMap;
	}

	public Multimap<Integer, Integer> getRegisterMultimap() {
		return registerMultimap;
	}

	public Set<Integer> getExploredPaths() {
		return exploredPaths;
	}

	public int getLastNodeID() {
		return lastNodeID;
	}

	public void setLastNodeID(int lastNodeID) {
		this.lastNodeID = lastNodeID;
	}

	public ProgramEnvironment getCopy() {
		ProgramEnvironment programEnvironment = new ProgramEnvironment();
		for (Node node: nodes) programEnvironment.getNodes().add(node.getCopy());
		programEnvironment.getRegisterMap().putAll(getRegisterMap());
		programEnvironment.getRegisterMultimap().putAll(getRegisterMultimap());
		programEnvironment.setLastNodeID(getLastNodeID());
		programEnvironment.getExploredPaths().addAll(getExploredPaths());
		return programEnvironment;
	}

	public boolean objectNodesEqual(DataNode objectNode1, DataNode objectNode2) {
		if (!(objectNode1 instanceof ObjectNode)) return false;
		if (!(objectNode2 instanceof ObjectNode)) return false;
		if (objectNode1 == objectNode2) return true;
		if (objectNode1.getActionNodeID() == -1 && objectNode2.getActionNodeID() == -1) {
			return objectNode1.equals(objectNode2);
		} else if (objectNode1.getActionNodeID() != -1 && objectNode2.getActionNodeID() != -1) {
			return objectNode1.equals(objectNode2) && 
					this.nodes.get(objectNode1.getActionNodeID()).equals(this.nodes.get(objectNode2.getActionNodeID()));
		} else {
			return false;
		}
	}

	public void addDataNode(DataNode dataNode) {
		nodes.add(dataNode);

		//update register maps
		for (int register: dataNode.getRegisters().toArray()) {
			this.registerMap.put(register, dataNode.getID());
			this.registerMultimap.put(register, dataNode.getID());
			if (TypeUtils.isWideType(dataNode.getType())) {
				this.registerMap.put(register+1, dataNode.getID());
				this.registerMultimap.put(register+1, dataNode.getID());
			}
		}

	}

	public void addActionNode(ActionNode actionNode) {
		nodes.add(actionNode);

		if (actionNode instanceof MethodNode) {
			MethodNode methodNode = (MethodNode)actionNode;
			if (methodNode.getReference().getName().equals(Constants.INIT)) {
				int register = methodNode.getInputRegisters().get(0);
				if (registerMap.containsKey(register)) {
					methodNode.getDataEdges().add(registerMap.get(register));
				}
				//add data edges
				for (int i = 1; i < actionNode.getInputRegisters().size(); i++) {
					register = actionNode.getInputRegisters().get(i);
					if (this.registerMap.containsKey(register)) {
						nodes.get(this.registerMap.get(register)).getDataEdges().add(actionNode.getID());
					}
				}
			} else {
				//add data edges
				for (int i = 0; i < actionNode.getInputRegisters().size(); i++) {
					int register = actionNode.getInputRegisters().get(i);
					if (this.registerMap.containsKey(register)) {
						nodes.get(this.registerMap.get(register)).getDataEdges().add(actionNode.getID());
					}
				}
			}
		} else {
			//add data edges
			for (int i = 0; i < actionNode.getInputRegisters().size(); i++) {
				int register = actionNode.getInputRegisters().get(i);
				if (this.registerMap.containsKey(register)) {
					nodes.get(this.registerMap.get(register)).getDataEdges().add(actionNode.getID());
				}
			}
		}

		//add control edges
		if (this.lastNodeID != -1) {
			nodes.get(this.lastNodeID).getControlEdges().add(actionNode.getID());
		}

		//update the last control/action node id
		this.lastNodeID = actionNode.getID();
	}

	public void addControlNode(ControlNode controlNode) {
		nodes.add(controlNode);

		//add data edges
		for (int i = 0; i < controlNode.getInputRegisters().size(); i++) {
			int register = controlNode.getInputRegisters().get(i);
			if (this.registerMap.containsKey(register)) {
				nodes.get(this.registerMap.get(register)).getDataEdges().add(controlNode.getID());
			}
		}

		//add control edges
		if (this.lastNodeID != -1) {
			nodes.get(this.lastNodeID).getControlEdges().add(controlNode.getID());
		}

		//update the last control/action node id
		this.lastNodeID = controlNode.getID();
	}

	public void addDataBackEdges() {
		for (Node node: nodes) {
			if (node instanceof ActionNode) {
				for (int i = 0; i < node.getDataEdges().size();i++) {
					DataNode dataNode = (DataNode) nodes.get(node.getDataEdges().get(i));
					dataNode.getDataBackEdges().add(node.getID());
				}
			}
			if (node instanceof DataNode) {
				for (int i = 0; i < node.getDataEdges().size();i++) {
					Node n = nodes.get(node.getDataEdges().get(i));
					n.getDataBackEdges().add(node.getID());
				}
			}
		}
	}

	public List<Integer> listOfActionNodes(int objectNodeID) {
		DataNode dataNode = (DataNode)nodes.get(objectNodeID);
		Set<Integer> retSet = new TreeSet<>();
		for (int i = 0; i < dataNode.getDataEdges().size();i++) retSet.add(dataNode.getDataEdges().get(i));
		for (int i = 0; i < dataNode.getDataBackEdges().size();i++) retSet.add(dataNode.getDataBackEdges().get(i));
		return new ArrayList<>(retSet);
	}

	public List<Integer> listOfDataNodes(int actionNodeID) {
		MethodNode methodNode = (MethodNode)nodes.get(actionNodeID);
		Set<Integer> retSet = new TreeSet<>();
		for (int i = 0; i < methodNode.getDataEdges().size();i++) retSet.add(methodNode.getDataEdges().get(i));
		for (int i = 0; i < methodNode.getDataBackEdges().size();i++) retSet.add(methodNode.getDataBackEdges().get(i));
		return new ArrayList<>(retSet);
	}

	public List<Integer> listOfActionNodes(List<Integer> objectNodeSet) {
		Set<Integer> retSet = new TreeSet<>();
		for (Integer i: objectNodeSet) {
			retSet.addAll(listOfActionNodes(i));
		}
		return new ArrayList<>(retSet);
	}


	public List<Integer> getMethodSequences(List<Integer> objectNodeIDs) {
		List<Integer> sequence = new ArrayList<>();
		boolean isParameter = false;
		for (Integer objectNodeID: objectNodeIDs) {
			ObjectNode objectNode = (ObjectNode)getNodes().get(objectNodeID);
			if (objectNode.getActionNodeID() == -1) {
				isParameter = true;
				break;
			}
		}
		if (isParameter) {
			//if one object node is the parameter of the method
			//consider the artificial LOAD method
			sequence.add(Constants.LOAD_ID);
			for (int i : listOfActionNodes(objectNodeIDs)) {
				if (getNodes().get(i) instanceof MethodNode) {
					MethodNode methodNode = (MethodNode)getNodes().get(i);
					if (PackageUtil.isSystemObject(methodNode.getReference().getDefiningClass())) {
						sequence.add(methodNode.getOffset());
					}
				} else if (getNodes().get(i) instanceof AccessorNode) {
					AccessorNode setterGetterNode = (AccessorNode)getNodes().get(i);
					if (PackageUtil.isSystemObject(setterGetterNode.getReference().getDefiningClass())) {
						sequence.add(setterGetterNode.getOffset());
					}
				}
			}
		} else {
			List<Integer> actionNodes = listOfActionNodes(objectNodeIDs);
			//if object node is create by a non-system method
			//consider the artificial LOAD method
			if (actionNodes.size() > 0) {
				int firstActionNodeID = actionNodes.get(0);
				if (getNodes().get(firstActionNodeID) instanceof MethodNode) {
					MethodNode methodNode = (MethodNode)getNodes().get(firstActionNodeID);
					if (PackageUtil.isSystemObject(methodNode.getReference().getDefiningClass())) {
						sequence.add(methodNode.getOffset());
					}
				} else if (getNodes().get(firstActionNodeID) instanceof AccessorNode) {
					AccessorNode setterGetterNode = (AccessorNode)getNodes().get(firstActionNodeID);
					if (PackageUtil.isSystemObject(setterGetterNode.getReference().getDefiningClass())) {
						sequence.add(setterGetterNode.getOffset());
					}
				}
				if (sequence.size() == 0) sequence.add(Constants.LOAD_ID);
				//consider the rest
				for (int i = 1; i < actionNodes.size();i++) {
					if (getNodes().get(actionNodes.get(i)) instanceof MethodNode) {
						MethodNode methodNode = (MethodNode)getNodes().get(actionNodes.get(i));
						if (PackageUtil.isSystemObject(methodNode.getReference().getDefiningClass())) {
							sequence.add(methodNode.getOffset());
						}
					} else if (getNodes().get(actionNodes.get(i)) instanceof AccessorNode) {
						AccessorNode setterGetterNode = (AccessorNode)getNodes().get(actionNodes.get(i));
						if (PackageUtil.isSystemObject(setterGetterNode.getReference().getDefiningClass())) {
							sequence.add(setterGetterNode.getOffset());
						}
					}
				}
			}
		}

		return sequence;
	}

	public List<Integer> getMethodSequences(ObjectNode objectNode) {
		List<Integer> sequence = new ArrayList<>();
		if (objectNode.getActionNodeID() == -1) {
			//if the object node is the parameter of the method
			//consider the artificial LOAD method
			sequence.add(Constants.LOAD_ID);
			for (int i : listOfActionNodes(objectNode.getID())) {
				if (getNodes().get(i) instanceof MethodNode) {
					MethodNode methodNode = (MethodNode)getNodes().get(i);
					if (PackageUtil.isSystemObject(methodNode.getReference().getDefiningClass())) {
						sequence.add(methodNode.getOffset());
					}
				} else if (getNodes().get(i) instanceof AccessorNode) {
					AccessorNode setterGetterNode = (AccessorNode)getNodes().get(i);
					if (PackageUtil.isSystemObject(setterGetterNode.getReference().getDefiningClass())) {
						sequence.add(setterGetterNode.getOffset());
					}
				}
			}
		} else {
			List<Integer> actionNodes = listOfActionNodes(objectNode.getID());
			//if object node is create by a non-system method
			//consider the artificial LOAD method
			if (actionNodes.size() > 0) {
				int firstActionNodeID = actionNodes.get(0);
				if (getNodes().get(firstActionNodeID) instanceof MethodNode) {
					MethodNode methodNode = (MethodNode)getNodes().get(firstActionNodeID);
					if (PackageUtil.isSystemObject(methodNode.getReference().getDefiningClass())) {
						sequence.add(methodNode.getOffset());
					}
				} else if (getNodes().get(firstActionNodeID) instanceof AccessorNode) {
					AccessorNode setterGetterNode = (AccessorNode)getNodes().get(firstActionNodeID);
					if (PackageUtil.isSystemObject(setterGetterNode.getReference().getDefiningClass())) {
						sequence.add(setterGetterNode.getOffset());
					}
				}
				if (sequence.size() == 0) sequence.add(Constants.LOAD_ID);
				//consider the rest
				for (int i = 1; i < actionNodes.size();i++) {
					if (getNodes().get(actionNodes.get(i)) instanceof MethodNode) {
						MethodNode methodNode = (MethodNode)getNodes().get(actionNodes.get(i));
						if (PackageUtil.isSystemObject(methodNode.getReference().getDefiningClass())) {
							sequence.add(methodNode.getOffset());
						}
					} else if (getNodes().get(actionNodes.get(i)) instanceof AccessorNode) {
						AccessorNode setterGetterNode = (AccessorNode)getNodes().get(actionNodes.get(i));
						if (PackageUtil.isSystemObject(setterGetterNode.getReference().getDefiningClass())) {
							sequence.add(setterGetterNode.getOffset());
						}
					}
				}
			}

		}
		return sequence;
	}

	public void saveToGraphViz() {
		GraphViz gv = new GraphViz();
		gv.addln(gv.start_graph());
		for (Node node: this.nodes) {
			gv.add(node.toDot());
		}
		gv.addln(gv.end_graph());
		//System.out.println(gv.getDotSource());

		String type = "pdf";
		File outFile = new File("/Users/TamNT/Documents/useal/graphviz/testCFG.pdf");

		gv.writeGraphToFile( gv.getGraph( gv.getDotSource(), type ), outFile );
	}

}
