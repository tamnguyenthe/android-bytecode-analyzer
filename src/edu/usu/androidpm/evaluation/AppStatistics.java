package edu.usu.androidpm.evaluation;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;

import org.jf.dexlib2.DexFileFactory;
import org.jf.dexlib2.dexbacked.DexBackedClassDef;
import org.jf.dexlib2.dexbacked.DexBackedDexFile;
import org.jf.dexlib2.dexbacked.DexBackedMethod;
import org.jf.dexlib2.iface.Method;
import org.jf.dexlib2.iface.instruction.Instruction;

import com.google.common.collect.ImmutableList;

import edu.usu.androidpm.cfg.node.MethodNode;
import edu.usu.androidpm.util.Constants;
import edu.usu.androidpm.util.PackageUtil;

public class AppStatistics {
	
	public static void main(String[] args) throws IOException {
		File appDirectory = new File("/home/useallab1/Documents/androidapps/apps");
		if (!appDirectory.isDirectory()) {
			System.err.println("There is a problem with the app directory");
			return;
		}
		System.out.println("Folder: " + appDirectory.getAbsolutePath());
		String[] appNames = appDirectory.list();
		int i = 1;
		for (String appName: appNames) {
			if (!appName.endsWith(".dex")) continue;
			System.out.println(i++ + " Processing " + appName + " ...");
			processDexFile(DexFileFactory.loadDexFile(appDirectory.getAbsolutePath() + "/" + appName, Constants.API));
			//if (i == 1000) break;
		}
		System.out.println("number of classes: " + numberOfClasses);
		System.out.println("number of methods: " + numberOfMethods);
		System.out.println("number of bytecode: " + numberOfBytecode);
	}
	
	static int numberOfClasses = 0;
	static int numberOfMethods = 0;
	static long numberOfBytecode = 0;
	static HashSet<Long> classesHashCodeSet = new HashSet<>(20000000);
	
	// adapted from String.hashCode()
	public static long hash(String string) {
	  long h = 1125899906842597L; // prime
	  int len = string.length();

	  for (int i = 0; i < len; i++) {
	    h = 31*h + string.charAt(i);
	  }
	  return h;
	}
	
	private static boolean checkMethod(Method method) {
		if (method.getName().length() < Constants.MIN_METHOD_NAME_LENGHT) return false;
		if (method.getImplementation() == null) return false;
		int numberOfIfNode = 0; 
		boolean hasSwitch = false;
		int size = 0;
		for (Instruction instruction: method.getImplementation().getInstructions()) {
			switch (instruction.getOpcode()) {
			case IF_EQ:
			case IF_NE:
			case IF_LT:
			case IF_GE:
			case IF_GT:
			case IF_LE:
			case IF_EQZ:
			case IF_NEZ:
			case IF_LTZ:
			case IF_GEZ:
			case IF_GTZ:
			case IF_LEZ:
				numberOfIfNode++;
				break;
			case PACKED_SWITCH:
			case SPARSE_SWITCH:
				hasSwitch = true;
				break;
			default:
				break;
			}
			size++;
		}
		if (numberOfIfNode > 10) return false;
		if (hasSwitch) return false;
		if (size > 1000) return false;
		if (size < 6) return false;
		return true;
	}
	
	private static void processDexFile(DexBackedDexFile loadDexFile) {
		for (DexBackedClassDef classDef: loadDexFile.getClasses()) {
			//we do not process class belongs to the Android Library
			if (!PackageUtil.isSystemClass(classDef)) {
				long clsHash = hash(classDef.getType());
				if (!classesHashCodeSet.contains(clsHash)) {
					classesHashCodeSet.add(clsHash);
					numberOfClasses++;
					for (DexBackedMethod method: classDef.getMethods()) {
						if (checkMethod(method)) processMethod(classDef, method);
					}
				}
			}
		}
	}

	private static void processMethod(DexBackedClassDef classDef,
			DexBackedMethod method) {
		numberOfMethods++;
		ImmutableList<Instruction> instructionList = ImmutableList.copyOf(method.getImplementation().getInstructions());
		numberOfBytecode += instructionList.size();
	}

}
