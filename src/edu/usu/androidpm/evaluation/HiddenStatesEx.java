package edu.usu.androidpm.evaluation;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import be.ac.ulg.montefiore.run.jahmm.Hmm;
import be.ac.ulg.montefiore.run.jahmm.ObservationInteger;
import be.ac.ulg.montefiore.run.jahmm.Opdf;
import be.ac.ulg.montefiore.run.jahmm.OpdfInteger;
import be.ac.ulg.montefiore.run.jahmm.OpdfIntegerFactory;
import be.ac.ulg.montefiore.run.jahmm.draw.GenericHmmDrawerDot;
import be.ac.ulg.montefiore.run.jahmm.learn.BaumWelchLearner;
import be.ac.ulg.montefiore.run.jahmm.learn.KMeansLearner;
import usu.useal.salad.evaluation.DatabaseLoader;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableList;

public class HiddenStatesEx {	
	private DatabaseLoader databaseLoader;
	private List<MutablePair<ImmutableList<Integer>, Integer>> dataset;
	private List<List<ObservationInteger>> trainingSet;
	private List<List<ObservationInteger>> validationSet;
	private BiMap<Integer, Integer> methodIDs;
	private static final double ALPHA = 0.1;
	private static final double VALIDATION_PORTION = 0.125;
	private int nObservations;
	
	public HiddenStatesEx(int objectID) throws Exception {
		databaseLoader = new DatabaseLoader();
		dataset = databaseLoader.loadDatasetForObject(objectID);
		System.out.println(dataset);
	}
	
	public static void main(String[] args) throws Exception {
		HiddenStatesEx hiddenStatesEx = new HiddenStatesEx(421);
		hiddenStatesEx.splitTrainingAndTestSets();
		hiddenStatesEx.trainHMM();
		hiddenStatesEx.trainNGramModel();
		hiddenStatesEx.testNGramModel();
	}

	public void splitTrainingAndTestSets() {
		//initialize training set and test set
		methodIDs = HashBiMap.create();
		trainingSet = new ArrayList<>();
		validationSet = new ArrayList<>();

		List<ObservationInteger> selectedSequence;
		for (MutablePair<ImmutableList<Integer>, Integer> pair: dataset) {
			System.out.println(pair.getLeft());
			for (Integer methodID: pair.getLeft()) {
				if (!methodIDs.containsKey(methodID)) {
					methodIDs.put(methodID, methodIDs.size());
					selectedSequence = new ArrayList<>();
					for (Integer methodID2: pair.getLeft()) {
						if (!methodIDs.containsKey(methodID2)) methodIDs.put(methodID2, methodIDs.size());
						selectedSequence.add(new ObservationInteger(methodIDs.get(methodID2)));
					}
					trainingSet.add(selectedSequence);
					pair.setRight(pair.getRight()-1);
					break;
				}
			}
		}

//		for (int i = 0; i < dataset.size(); i++) {
//			MutablePair<ImmutableList<Integer>, Integer> pair = dataset.get(i);
//			if (pair.getRight() > 0) {
//				selectedSequence = new ArrayList<>();
//				for (Integer methodID: pair.getLeft()) {
//					selectedSequence.add(new ObservationInteger(methodIDs.get(methodID)));
//				}
//				testSet.add(selectedSequence);
//				pair.setRight(pair.getRight()-1);
//			}
//		}

		List<List<ObservationInteger>> tempTraingSet = new ArrayList<>();

		for (MutablePair<ImmutableList<Integer>, Integer> pair: dataset) {
			if(pair.getRight() > 0) {
				for (int i = 0; i < pair.getRight(); i++) {
					selectedSequence = new ArrayList<>();
					for (Integer methodID: pair.getLeft()) {
						selectedSequence.add(new ObservationInteger(methodIDs.get(methodID)));
					}
					tempTraingSet.add(selectedSequence);
				}
			}
		}

		Collections.shuffle(tempTraingSet);
		int validationSize = (int) Math.round(tempTraingSet.size()*VALIDATION_PORTION);
		validationSet.addAll(tempTraingSet.subList(0, validationSize));
		trainingSet.addAll(tempTraingSet.subList(validationSize, validationSet.size()));
		nObservations = methodIDs.size();
	}
	
	private int maxIndex(List<Double> seqs) {
		double max = Double.NEGATIVE_INFINITY;
		int maxIndex = -1;
		for (int i = 0; i < seqs.size(); i++) {
			if (max < seqs.get(i)) {
				max = seqs.get(i);
				maxIndex = i;
			}
		}
		return maxIndex;
	}

	void trainHMM() throws IOException {
		List<Double> lnLikelihoods = new ArrayList<>();
		int minNbStates = 	1;
		int maxNbStates = 20;
		double lnLikelihood;
		for (int i = minNbStates; i <= maxNbStates; i++) {
			lnLikelihood = 0;
			KMeansLearner<ObservationInteger> kml = new KMeansLearner<ObservationInteger>(i, new OpdfIntegerFactory(methodIDs.size()), trainingSet);
			Hmm<ObservationInteger> hmm = kml.iterate();
			BaumWelchLearner bwl = new BaumWelchLearner();
			hmm = bwl.learn(hmm, trainingSet);		
			smoothHmm(hmm);
			for (List<ObservationInteger> seq: validationSet) {
				lnLikelihood += hmm.lnProbability(seq);
				if (hmm.lnProbability(seq) == Double.NEGATIVE_INFINITY) {
					(new GenericHmmDrawerDot()).write(hmm, "error_hmm.dot");
					System.out.println(seq);
				}
			}

			lnLikelihoods.add(lnLikelihood);
		}
		int bestNbStates = minNbStates + maxIndex(lnLikelihoods);
		System.out.println(lnLikelihoods);
		//trainingSet.addAll(validationSet);
		KMeansLearner<ObservationInteger> kml = new KMeansLearner<ObservationInteger>(bestNbStates, new OpdfIntegerFactory(methodIDs.size()), trainingSet);
		Hmm<ObservationInteger> hmm = kml.iterate();
		BaumWelchLearner bwl = new BaumWelchLearner();
		hmm = bwl.learn(hmm, trainingSet);
		(new GenericHmmDrawerDot()).write(hmm, "hmm.dot");
		testHMM(hmm, validationSet);
	}
	
	public void smoothHmm(Hmm<ObservationInteger> hmm) {
		double prob;
		int nbStates = hmm.nbStates();
		int nbObservations = methodIDs.size();
		for (int i = 0; i < nbStates; i++) {
			prob = hmm.getPi(i);
			hmm.setPi(i, (prob + ALPHA)/(1. + nbStates*ALPHA));
		}

		for (int i = 0; i < nbStates; i++) {
			for (int j = 0; j < nbStates; j++) {
				prob = hmm.getAij(i, j);
				hmm.setAij(i, j, (prob + ALPHA)/(1. + nbStates*ALPHA));
			}
		}

		Opdf<ObservationInteger> opdf;
		double[] probs;
		for (int i = 0; i < nbStates; i++) {
			opdf = hmm.getOpdf(i);
			probs = new double[nbObservations];
			for (int j = 0; j < nbObservations; j++) {
				prob = opdf.probability(new ObservationInteger(j));
				probs[j] = (prob + ALPHA)/(1. + nbObservations*ALPHA);
			}
			hmm.setOpdf(i, new OpdfInteger(probs));
		}
	}
	
	public int [] topHitTable = new int[10];
	public double[] finalTopHitTable =  new double[10];

	void testHMM(Hmm<ObservationInteger> hmm, List<List<ObservationInteger>> tSet) throws FileNotFoundException {
		List<Pair<Integer, Double>> rank;
		System.out.println("testHMM");
		int numPredict = 0;
		List<ObservationInteger> predictSeq;
		for (List<ObservationInteger> seq: tSet) {
			for (int i = 0; i < seq.size(); i++) {
				rank = new ArrayList<>();
				for (int elem = 0; elem < methodIDs.size(); elem++) {
					predictSeq = new ArrayList<>(seq.subList(0, i));
					predictSeq.add(new ObservationInteger(elem));
					rank.add(Pair.of(elem, hmm.lnProbability(predictSeq)));
				}
				Collections.sort(rank, new Comparator<Pair<Integer, Double>>() {
					@Override
					public int compare(Pair<Integer, Double> o1,
							Pair<Integer, Double> o2) {
						return -Double.compare(o1.getRight(), o2.getRight());
					}	
				});

				for (int j = 0; j < rank.size(); j++) {
					if (seq.get(i).value == rank.get(j).getLeft().intValue()) {
						if (j < 10) {
							for (int k = j; k < topHitTable.length; k++) {
								topHitTable[k]++;
							}
						}
						break;
					}
				}
				numPredict++;
			}
		}
		System.out.println("Top 10: ");
		for (int i = 0; i < topHitTable.length; i++) {
			finalTopHitTable[i] = (double)topHitTable[i]/numPredict;
			System.out.println(i + " : " + (double)topHitTable[i]/numPredict);
		}
	}
	
	private Map<ImmutableList<Integer>, Map<Integer, Double>> nGramModel;
	private Map<ImmutableList<Integer>, Integer> triGramMap;
	private Map<ImmutableList<Integer>, Integer> duoGramMap;

	public void trainNGramModel() {
		triGramMap = new HashMap<>();
		duoGramMap = new HashMap<>();
		nGramModel = new HashMap<>();
		int twoPrev, onePrev, it;
		for (List<ObservationInteger> sequence: trainingSet) {
			for (int i = 0; i < sequence.size(); i++) {
				if (i == 0) {
					twoPrev = -1;
					onePrev = -1;
				} else if (i == 1) {
					twoPrev = -1;
					onePrev = sequence.get(i-1).value;
				} else {
					twoPrev = sequence.get(i-2).value;
					onePrev= sequence.get(i-1).value;
				}
				it = sequence.get(i).value;
				//collect 3-gram
				ImmutableList<Integer> triGram = ImmutableList.of(onePrev, it);				
				Integer trigramCount = triGramMap.get(triGram);
				if (trigramCount == null) triGramMap.put(triGram, 1);
				else triGramMap.put(triGram, trigramCount+1);

				//collect 2-gram
				ImmutableList<Integer> duoGram = ImmutableList.of(it);
				Integer duoGramCount = duoGramMap.get(duoGram);
				if (duoGramCount == null) duoGramMap.put(duoGram, 1);
				else duoGramMap.put(duoGram, duoGramCount+1);
				if (i == 0) {
					duoGram = ImmutableList.of(onePrev);
					duoGramCount = duoGramMap.get(duoGram);
					if (duoGramCount == null) duoGramMap.put(duoGram, 1);
					else duoGramMap.put(duoGram, duoGramCount+1);
				}
			}
		}

		for (ImmutableList<Integer> triGram: triGramMap.keySet()) {
			ImmutableList<Integer> duoGram = ImmutableList.copyOf(triGram.subList(0, triGram.size()-1));
			if (!nGramModel.containsKey(duoGram)) nGramModel.put(duoGram, new HashMap<Integer, Double>());
			int triGramCount = triGramMap.get(triGram);
			int duoGramCount = duoGramMap.get(duoGram);
			nGramModel.get(duoGram).put(triGram.get(triGram.size()-1),(double)triGramCount/duoGramCount);
		}
	}

	public void testNGramModel() {
		System.out.println("testNGram");
		Arrays.fill(topHitTable, 0);
		Arrays.fill(finalTopHitTable, 0);;
		List<Pair<Integer, Double>> rank;
		int numPredict = 0;
		List<ObservationInteger> predictSeq;
		int twoPrev, onePrev, it;
		for (List<ObservationInteger> seq: validationSet) {
			for (int i = 0; i < seq.size(); i++) {
				if (i == 0) {
					twoPrev = -1;
					onePrev = -1;
				} else if (i == 1) {
					twoPrev = -1;
					onePrev = seq.get(i-1).value;
				} else {
					twoPrev = seq.get(i-2).value;
					onePrev= seq.get(i-1).value;
				}
				ImmutableList<Integer> duoGram = ImmutableList.of(onePrev);
				Map<Integer, Double> pMap = nGramModel.get(duoGram);
				if (pMap == null) {
					numPredict++;
					continue;
				}
				rank = new ArrayList<>();
				for (int elem = 0; elem < methodIDs.size(); elem++) {
					it = elem;
					//if (pMap == null) rank.add(Pair.of(it, -0.1));
					if (pMap.containsKey(it)) rank.add(Pair.of(it, pMap.get(it)));
						//else rank.add(Pair.of(it, -0.1));
				}
				Collections.shuffle(rank);
				Collections.sort(rank, new Comparator<Pair<Integer, Double>>() {
					@Override
					public int compare(Pair<Integer, Double> o1,
							Pair<Integer, Double> o2) {
						return -Double.compare(o1.getRight(), o2.getRight());
					}	
				});

				for (int j = 0; j < rank.size(); j++) {
					if (seq.get(i).value == rank.get(j).getLeft().intValue()) {
						if (j < 10) {
							for (int k = j; k < topHitTable.length; k++) {
								topHitTable[k]++;
							}
						}
						break;
					}
				}
				numPredict++;
			}
		}
		System.out.println("Top 10: ");
		for (int i = 0; i < topHitTable.length; i++) {
			finalTopHitTable[i] = (double)topHitTable[i]/numPredict;
			System.out.println(i + " : " + (double)topHitTable[i]/numPredict);
		}
	}

}
