package edu.usu.androidpm.evaluation;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import be.ac.ulg.montefiore.run.jahmm.Hmm;
import be.ac.ulg.montefiore.run.jahmm.ObservationInteger;
import be.ac.ulg.montefiore.run.jahmm.Opdf;
import be.ac.ulg.montefiore.run.jahmm.OpdfInteger;
import be.ac.ulg.montefiore.run.jahmm.OpdfIntegerFactory;
import be.ac.ulg.montefiore.run.jahmm.draw.GenericHmmDrawerDot;
import be.ac.ulg.montefiore.run.jahmm.learn.BaumWelchLearner;
import be.ac.ulg.montefiore.run.jahmm.learn.KMeansLearner;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableList;

import edu.usu.androidpm.util.GraphViz;
import usu.useal.salad.evaluation.DatabaseLoader;

public class PosterEvaluation2 {
	private DatabaseLoader databaseLoader;
	private List<MutablePair<ImmutableList<Integer>	, Integer>> rawDataset;
	private List<List<ObservationInteger>> dataset;
	private List<List<ObservationInteger>> testSet;
	private BiMap<Integer, Integer> methodIDs;
	private Hmm<ObservationInteger> hmm;
	private int nbStates = 6;
	private static final double ALPHA = 0.001;
	
	public PosterEvaluation2() throws Exception {
		databaseLoader = new DatabaseLoader();
		rawDataset = databaseLoader.loadDatasetForObject(1370);
		methodIDs = HashBiMap.create();
		methodIDs.put(3297, 0);
		methodIDs.put(3358, 1);
		methodIDs.put(1190, 2);
		methodIDs.put(2160, 3);
		methodIDs.put(1189, 4);
		methodIDs.put(2682, 5);
		methodIDs.put(5326, 6);
		methodIDs.put(4024, 7);
		methodIDs.put(7213, 8);
		methodIDs.put(12539, 9);
		filterRawDataset();
	}
	
	private void filterRawDataset() {
		testSet = new ArrayList<>();
		dataset = new ArrayList<>();
		List<ObservationInteger> sequence;
		for (MutablePair<ImmutableList<Integer>	, Integer> pair: rawDataset) {
			sequence = new ArrayList<>();
			for (Integer methodID: pair.getLeft()) if (methodIDs.containsKey(methodID)) sequence.add(new ObservationInteger(methodIDs.get(methodID)));
			if (sequence.size() >= 2) {
				for (int i = 0; i < pair.right; i++) dataset.add(sequence);
				//testSet.add(sequence);
				System.out.println(sequence);
				System.out.println(pair.right);
			}
		}
	}
	Pair<List<List<ObservationInteger>>, List<List<ObservationInteger>>> splitDataset(List<List<ObservationInteger>> dataset, double portion) {
		int trainingSize = (int)Math.round(dataset.size() * portion);
		List<Integer> indexes = new ArrayList<>();
		for (int i = 0; i < dataset.size(); i++) indexes.add(i);
		Collections.shuffle(indexes);
		List<List<ObservationInteger>> trainingSet = new ArrayList<>();
		for (int i = 0; i < trainingSize; i++) trainingSet.add(dataset.get(indexes.get(i)));
		List<List<ObservationInteger>> testSet = new ArrayList<>();
		for (int i = trainingSize; i < dataset.size(); i++) testSet.add(dataset.get(indexes.get(i)));
		return Pair.of(trainingSet, testSet);
	}
	
	Pair<List<List<ObservationInteger>>, List<List<ObservationInteger>>> pairDatasets;
	Pair<List<List<ObservationInteger>>, List<List<ObservationInteger>>> pairDatasets2;
	List<Integer> holds;
	
	public void trainHMM() throws Exception {
		pairDatasets = splitDataset(dataset, 0.875);
		pairDatasets2 = splitDataset(pairDatasets.getLeft(), 0.8);
		
		List<Double> lnLikelihoods = new ArrayList<>();
		int minNbStates = 2;
		int maxNbStates = 17;
		double lnLikelihood;
		for (int i = minNbStates; i <= maxNbStates; i++) {
			lnLikelihood = 0;
			KMeansLearner<ObservationInteger> kml = new KMeansLearner<ObservationInteger>(i, new OpdfIntegerFactory(methodIDs.size()), pairDatasets.getLeft());
			Hmm<ObservationInteger> hmm = kml.iterate();
			BaumWelchLearner bwl = new BaumWelchLearner();
			hmm = bwl.learn(hmm, pairDatasets.getLeft());		
			smoothHmm(hmm);
			for (List<ObservationInteger> seq: pairDatasets.getRight()) {
				lnLikelihood += hmm.lnProbability(seq);
				if (hmm.lnProbability(seq) == Double.NEGATIVE_INFINITY) {
					(new GenericHmmDrawerDot()).write(hmm, "error_hmm.dot");
					System.out.println(seq);
				}
			}

			lnLikelihoods.add(lnLikelihood);
		}
		int bestNbStates = minNbStates + maxIndex(lnLikelihoods);
		System.out.println(bestNbStates);
		
		KMeansLearner<ObservationInteger> kml = new KMeansLearner<ObservationInteger>(nbStates, new OpdfIntegerFactory(methodIDs.size()), dataset);
		hmm = kml.iterate();
		BaumWelchLearner bwl = new BaumWelchLearner();
		hmm = bwl.learn(hmm, dataset);
		smoothHmm(hmm);
	}
	
	private int maxIndex(List<Double> seqs) {
		double max = Double.NEGATIVE_INFINITY;
		int maxIndex = -1;
		for (int i = 0; i < seqs.size(); i++) {
			if (max < seqs.get(i)) {
				max = seqs.get(i);
				maxIndex = i;
			}
		}
		return maxIndex;
	}
	
	public void smoothHmm(Hmm<ObservationInteger> hmm) {
		double prob;
		int nbStates = hmm.nbStates();
		int nbObservations = methodIDs.size();
		for (int i = 0; i < nbStates; i++) {
			prob = hmm.getPi(i);
			hmm.setPi(i, (prob + ALPHA)/(1. + nbStates*ALPHA));
		}

		for (int i = 0; i < nbStates; i++) {
			for (int j = 0; j < nbStates; j++) {
				prob = hmm.getAij(i, j);
				hmm.setAij(i, j, (prob + ALPHA)/(1. + nbStates*ALPHA));
			}
		}

		Opdf<ObservationInteger> opdf;
		double[] probs;
		for (int i = 0; i < nbStates; i++) {
			opdf = hmm.getOpdf(i);
			probs = new double[nbObservations];
			for (int j = 0; j < nbObservations; j++) {
				prob = opdf.probability(new ObservationInteger(j));
				probs[j] = (prob + ALPHA)/(1. + nbObservations*ALPHA);
			}
			hmm.setOpdf(i, new OpdfInteger(probs));
		}
	}
	
	public void writeHMM() {
		Map<Integer, String> labelMap = new HashMap<>();
//		labelMap.put(0, "Error");
//		labelMap.put(1, "Initial");
//		labelMap.put(2, "Initial");
//		labelMap.put(3, "Initialized");
//		labelMap.put(4, "DataSourceConfigured");
//		labelMap.put(5, "DataSourceConfigured");
//		labelMap.put(6, "DataSourceConfigured");
//		labelMap.put(7, "DataSourceConfigured");
//		labelMap.put(8, "DataSourceConfigured");
//		labelMap.put(9, "DataSourceConfigured");
//		labelMap.put(10, "Prepared");
//		labelMap.put(11, "Recording");
//		labelMap.put(12, "Recording");
		GraphViz gv = new GraphViz();
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		gv.addln(gv.start_graph());
		gv.addln("graph [autosize=false, size=\"50,50!\", resolution=100];\n");
		gv.addln("node [height=0.02, width=0.01];\n");
		gv.addln("graph [bb=\"0,0,100,50\"];\n");
		for (int i = 0; i < nbStates; i++) {
			int[] nextStates = findTheLargestAndSecondLargestIndex(hmm, i);
			int[] methodIndexs = findTheLargestAndSecondLargestIndex(hmm.getOpdf(i));
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(i + " [shape=");
			if (hmm.getPi(i) > 0.0001) {
				stringBuilder.append("doublecircle, label=\"");
				stringBuilder.append("Pi = " + df.format(hmm.getPi(i)) + "\n");
			} else {
				stringBuilder.append("circle, label=\"");
			}
			for (int methodIndex: methodIndexs) {
				System.out.println(databaseLoader.getMethodNamesBiMap().get(methodIDs.inverse().get(methodIndex)));
				stringBuilder.append(databaseLoader.getMethodNamesBiMap().get(methodIDs.inverse().get(methodIndex)) + " - " + df.format(hmm.getOpdf(i).probability(new ObservationInteger(methodIndex))) + "\n");
			}
			stringBuilder.append("\"];\n");
			for (int nextState: nextStates) {
				stringBuilder.append(i + " -> " + nextState);
				stringBuilder.append("[label=" + df.format(hmm.getAij(i, nextState)) + "];\n");
			}
			gv.add(stringBuilder.toString());
		}
		gv.addln(gv.end_graph());
		String type = "png";
		File outFile = new File("/Users/TamNT/Documents/Poster/testCFG.png");
		gv.writeGraphToFile( gv.getGraph( gv.getDotSource(), type ), outFile );
	}
	
	private static String getName(String str) {
		String[] parts = str.split("[>(]");
		return parts[parts.length-2] + "()";
	}
	
	private int[] findTheLargestAndSecondLargestIndex(Hmm<ObservationInteger> hmm, int stateNb) {
		double[] rank = new double[nbStates];
		for (int i = 0; i < hmm.nbStates();i++) {
			double probability = hmm.getAij(stateNb, i);
			rank[i] = probability;
		}
		return maxKIndex(rank, 3);
	}
	
	private int[] findTheLargestAndSecondLargestIndex(Opdf<ObservationInteger> pdf) {
		double[] rank = new double[methodIDs.size()];
		for (int i = 0; i < methodIDs.size();i++) {
			double probability = pdf.probability(new ObservationInteger(i));
			rank[i] = probability;
		}
		return maxKIndex(rank, 3);
	}
	
	/**
	  * Return the indexes correspond to the top-k largest in an array.
	  */
	public static int[] maxKIndex(double[] array, int top_k) {
	    double[] max = new double[top_k];
	    int[] maxIndex = new int[top_k];
	    Arrays.fill(max, Double.NEGATIVE_INFINITY);
	    Arrays.fill(maxIndex, -1);

	    top: for(int i = 0; i < array.length; i++) {
	        for(int j = 0; j < top_k; j++) {
	            if(array[i] > max[j]) {
	                for(int x = top_k - 1; x > j; x--) {
	                    maxIndex[x] = maxIndex[x-1]; max[x] = max[x-1];
	                }
	                maxIndex[j] = i; max[j] = array[i];
	                continue top;
	            }
	        }
	    }
	    return maxIndex;
	}
	
	
	public static void main(String[] args) throws Exception {
		PosterEvaluation2 posterEvaluation = new PosterEvaluation2();
		posterEvaluation.trainHMM();
		//posterEvaluation.trainNGramModel();
		//posterEvaluation.testNGramModel();
		posterEvaluation.writeHMM();
	}
}
