package edu.usu.androidpm.evaluation;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import be.ac.ulg.montefiore.run.jahmm.Hmm;
import be.ac.ulg.montefiore.run.jahmm.ObservationInteger;
import be.ac.ulg.montefiore.run.jahmm.Opdf;
import be.ac.ulg.montefiore.run.jahmm.OpdfInteger;
import be.ac.ulg.montefiore.run.jahmm.OpdfIntegerFactory;
import be.ac.ulg.montefiore.run.jahmm.draw.GenericHmmDrawerDot;
import be.ac.ulg.montefiore.run.jahmm.learn.BaumWelchLearner;
import be.ac.ulg.montefiore.run.jahmm.learn.KMeansLearner;
import usu.useal.salad.evaluation.DatabaseLoader;
import usu.useal.salad.evaluation.TestRNN;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableList;



public class PredictionTest {
	static PrintWriter printWriter;
	static {
		try {
			printWriter = new PrintWriter("all_result.txt");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private DatabaseLoader databaseLoader;
	private List<MutablePair<ImmutableList<Integer>, Integer>> dataset;
	private BiMap<Integer, Integer> methodIDs;
	private int nObservations;
	private List<List<ObservationInteger>> trainingSet;
	private List<List<ObservationInteger>> testSet;
	private List<List<ObservationInteger>> validationSet;
	private static final double ALPHA = 0.1;
	private static final double VALIDATION_PORTION = 0.125;
	int sum = 0;

	public PredictionTest(int objectID) throws Exception {
		databaseLoader = new DatabaseLoader();
		dataset = databaseLoader.loadDatasetForObject(objectID);
		for (MutablePair<ImmutableList<Integer>, Integer> pair: dataset) {
			sum += pair.getRight();
		}
		if (sum < 24) throw new Exception();
		if (sum > 50000) throw new Exception();
		//System.out.println(dataset);
	}

	public void splitTrainingAndTestSets() throws Exception {
		//initialize training set and test set
		methodIDs = HashBiMap.create();
		trainingSet = new ArrayList<>();
		testSet = new ArrayList<>();
		validationSet = new ArrayList<>();

		List<ObservationInteger> selectedSequence;
		for (MutablePair<ImmutableList<Integer>, Integer> pair: dataset) {
			for (Integer methodID: pair.getLeft()) {
				if (!methodIDs.containsKey(methodID)) {
					methodIDs.put(methodID, methodIDs.size());
					selectedSequence = new ArrayList<>();
					for (Integer methodID2: pair.getLeft()) {
						if (!methodIDs.containsKey(methodID2)) methodIDs.put(methodID2, methodIDs.size());
						selectedSequence.add(new ObservationInteger(methodIDs.get(methodID2)));
					}
					trainingSet.add(selectedSequence);
					pair.setRight(pair.getRight()-1);
					break;
				}
			}
		}

		List<Integer> testLengths = new ArrayList<>();
		
		while (testSet.size() < sum * 0.2) {
			for (int i = 0; i < dataset.size(); i++) {
				MutablePair<ImmutableList<Integer>, Integer> pair = dataset.get(i);
				if (pair.getRight() > 0) {
					selectedSequence = new ArrayList<>();
					for (Integer methodID: pair.getLeft()) {	
						selectedSequence.add(new ObservationInteger(methodIDs.get(methodID)));
					}
					testSet.add(selectedSequence);
					testLengths.add(selectedSequence.size());
					pair.setRight(pair.getRight()-1);
				}
			}
		}

		Collections.sort(testLengths);
		int median = median(testLengths);
		
		List<List<ObservationInteger>> newtestSet = new ArrayList<>();
		for (List<ObservationInteger> sequence: testSet) {
			if (sequence.size() <=  median) {
				newtestSet.add(sequence);
			}
		}
		testSet = newtestSet;
		
		List<List<ObservationInteger>> tempTraingSet = new ArrayList<>();

		
		for (MutablePair<ImmutableList<Integer>, Integer> pair: dataset) {
			if(pair.getRight() > 0) {
				for (int i = 0; i < pair.getRight(); i++) {
					selectedSequence = new ArrayList<>();
					for (Integer methodID: pair.getLeft()) {
						selectedSequence.add(new ObservationInteger(methodIDs.get(methodID)));
					}
					tempTraingSet.add(selectedSequence);
				}
			}
		}

		Collections.shuffle(tempTraingSet);
		int validationSize = (int) Math.round(tempTraingSet.size()*VALIDATION_PORTION);
		validationSet.addAll(tempTraingSet.subList(0, validationSize));
		trainingSet.addAll(tempTraingSet.subList(validationSize, tempTraingSet.size()));
		nObservations = methodIDs.size();

		//save training set
		PrintWriter trainPrintWriter = new PrintWriter("train.txt");
		for (List<ObservationInteger> sequence: trainingSet) {
			for (int i = 0; i < sequence.size()-1; i++) {
				trainPrintWriter.print(sequence.get(i).value + " ");
			}
			trainPrintWriter.println(sequence.get(sequence.size()-1));
		}
		trainPrintWriter.close();

		//save validation test
		PrintWriter validPrintWriter = new PrintWriter("valid.txt");
		for (List<ObservationInteger> sequence: validationSet) {
			for (int i = 0; i < sequence.size()-1; i++) {
				validPrintWriter.print(sequence.get(i).value + " ");
			}
			validPrintWriter.println(sequence.get(sequence.size()-1));
		}
		validPrintWriter.close();

		//save test set
		PrintWriter testPrintWriter = new PrintWriter("test.txt");
		testPrintWriter.print(methodIDs.size());
		testPrintWriter.println(" " + testSet.size());
		for (List<ObservationInteger> sequence: testSet) {
			for (int i = 0; i < sequence.size()-1; i++) {
				testPrintWriter.print(sequence.get(i).value + " ");
			}
			testPrintWriter.println(sequence.get(sequence.size()-1));
		}
		testPrintWriter.close();
		
		//save test set for rnnlm
		int nPredict = 0;
		List<ObservationInteger> predictSeq;
		PrintWriter testRNNPrintWriter = new PrintWriter("test.rnnlm.txt");
		for (List<ObservationInteger> sequence: testSet) {
			for (int i = 0; i < sequence.size(); i++) {
				nPredict++;
				for (int elem = 0; elem < methodIDs.size(); elem++) {
					predictSeq = new ArrayList<>(sequence.subList(0, i));
					predictSeq.add(new ObservationInteger(elem));
					testRNNPrintWriter.print(nPredict);
					for (ObservationInteger oi: predictSeq) {
						testRNNPrintWriter.print(" " + oi.value);
					}
					testRNNPrintWriter.println();
				}

			}
		}
		testRNNPrintWriter.close();
	}

	public void smoothHmm(Hmm<ObservationInteger> hmm) {
		double prob;
		int nbStates = hmm.nbStates();
		int nbObservations = methodIDs.size();
		for (int i = 0; i < nbStates; i++) {
			prob = hmm.getPi(i);
			hmm.setPi(i, (prob + ALPHA)/(1. + nbStates*ALPHA));
		}

		for (int i = 0; i < nbStates; i++) {
			for (int j = 0; j < nbStates; j++) {
				prob = hmm.getAij(i, j);
				hmm.setAij(i, j, (prob + ALPHA)/(1. + nbStates*ALPHA));
			}
		}

		Opdf<ObservationInteger> opdf;
		double[] probs;
		for (int i = 0; i < nbStates; i++) {
			opdf = hmm.getOpdf(i);
			probs = new double[nbObservations];
			for (int j = 0; j < nbObservations; j++) {
				prob = opdf.probability(new ObservationInteger(j));
				probs[j] = (prob + ALPHA)/(1. + nbObservations*ALPHA);
			}
			hmm.setOpdf(i, new OpdfInteger(probs));
		}
	}
	
	public static int median(List<Integer> values)
	{
		Collections.sort(values);

		if (values.size() % 2 == 1)
			return values.get((values.size()+1)/2-1);
		else
		{
			int lower = values.get(values.size()/2-1);
			int upper = values.get(values.size()/2);

			return lower;
		}	
	}

	private int maxIndex(List<Double> seqs) {
		double max = Double.NEGATIVE_INFINITY;
		int maxIndex = -1;
		for (int i = 0; i < seqs.size(); i++) {
			if (max < seqs.get(i)) {
				max = seqs.get(i);
				maxIndex = i;
			}
		}
		return maxIndex;
	}

	void trainHMM() throws IOException {
		List<Double> lnLikelihoods = new ArrayList<>();
		int minNbStates = 	2;
		int maxNbStates = 20;
		double lnLikelihood;
		for (int i = minNbStates; i <= maxNbStates; i++) {
			lnLikelihood = 0;
			KMeansLearner<ObservationInteger> kml = new KMeansLearner<ObservationInteger>(i, new OpdfIntegerFactory(methodIDs.size()), trainingSet);
			Hmm<ObservationInteger> hmm = kml.iterate();
			BaumWelchLearner bwl = new BaumWelchLearner();
			//hmm = bwl.learn(hmm, trainingSet);		
			hmm = kml.learn();		
			smoothHmm(hmm);
			for (List<ObservationInteger> seq: validationSet) {
				lnLikelihood += hmm.lnProbability(seq);
				if (hmm.lnProbability(seq) == Double.NEGATIVE_INFINITY) {
					(new GenericHmmDrawerDot()).write(hmm, "error_hmm.dot");
					System.out.println(seq);
				}
			}

			lnLikelihoods.add(lnLikelihood);
		}
		int bestNbStates = minNbStates + maxIndex(lnLikelihoods);
		trainingSet.addAll(validationSet);
		KMeansLearner<ObservationInteger> kml = new KMeansLearner<ObservationInteger>(bestNbStates, new OpdfIntegerFactory(methodIDs.size()), trainingSet);
		Hmm<ObservationInteger> hmm = kml.iterate();
		BaumWelchLearner bwl = new BaumWelchLearner();
		hmm = bwl.learn(hmm, trainingSet);
		finalHmm = hmm;
		//System.out.println(lnLikelihoods);
		//holds = generateHolds(testSet);
		testHMM(hmm, testSet);
	}

	Hmm<ObservationInteger> finalHmm;


	public int [] topHitTable = new int[10];
	public double[] finalTopHitTable =  new double[10];

	List<Integer> generateHolds(List<List<ObservationInteger>> tSet) {
		List<Integer> result = new ArrayList<>();
		Random rand = new Random();
		for (List<ObservationInteger> sequence: tSet) {
			result.add(rand.nextInt(sequence.size()));
		}
		return result;
	}

	List<Integer> holds;

	void testHMM(Hmm<ObservationInteger> hmm, List<List<ObservationInteger>> tSet) throws FileNotFoundException {
		List<Pair<Integer, Double>> rank;
		//PrintWriter printWriter = new PrintWriter(new File("predict.txt"));
		System.out.println("testHMM");
		int numPredict = 0;


		List<ObservationInteger> predictSeq;
		for (List<ObservationInteger> seq: tSet) {
			for (int i = 0; i < seq.size(); i++) {
				rank = new ArrayList<>();
				for (int elem = 0; elem < methodIDs.size(); elem++) {
					predictSeq = new ArrayList<>(seq.subList(0, i));
					predictSeq.add(new ObservationInteger(elem));
					rank.add(Pair.of(elem, hmm.lnProbability(predictSeq)));
				}

				//				Map<Integer, Double> weightMap = new HashMap<>();
				//				ViterbiCalculator viterbiCalculator = new ViterbiCalculator(seq.subList(0, i), hmm);
				//				int[] bestStateSeq = viterbiCalculator.stateSequence();
				//				//System.out.println(Arrays.toString(bestStateSeq));
				//				//for (int j = 0; j < nbStates; j++) probList.add(hmm.getOpdf(j).probability(seq.get(i-1)));
				//				int bestState = bestStateSeq[bestStateSeq.length-1];
				//				//probList.clear();
				//				double probability;
				//				for (int m = 0; m < hmm.nbStates(); m++) {
				//					for (int n = 1; n < methodIDs.size();n++) {
				//						probability = hmm.getAij(bestState, m) * hmm.getOpdf(m).probability(new ObservationInteger(n));
				//						Double prob = weightMap.get(n);
				//						if (prob == null) weightMap.put(n, probability);
				//						else weightMap.put(n, prob + probability);
				//					}
				//				}
				//				for (Map.Entry<Integer, Double> entry: weightMap.entrySet())
				//					rank.add(Pair.of(entry.getKey(), entry.getValue()));

				Collections.sort(rank, new Comparator<Pair<Integer, Double>>() {
					@Override
					public int compare(Pair<Integer, Double> o1,
							Pair<Integer, Double> o2) {
						return -Double.compare(o1.getRight(), o2.getRight());
					}	
				});

				for (int j = 0; j < rank.size(); j++) {
					if (seq.get(i).value == rank.get(j).getLeft().intValue()) {
						if (j < 10) {
							for (int k = j; k < topHitTable.length; k++) {
								topHitTable[k]++;
							}
							//							if (j > 0) {
							//								printWriter.println("Previous sequence: ");
							//								for (ObservationInteger oi: seq.subList(0, i)) {
							//									printWriter.print(methodNameToID.inverse().get(oi.value) + ", ");
							//								}
							//								printWriter.println();
							//								printWriter.println("Actual method: ");
							//								printWriter.println(methodNameToID.inverse().get(seq.get(i).value));
							//								printWriter.println("Top Rank: ");
							//								for (int k = 0; k <= j; k++) {
							//									printWriter.println(methodNameToID.inverse().get(rank.get(k).getLeft().intValue()));
							//								}
							//							}

						}
						break;
					}
				}
				numPredict++;
			}
		}
		System.out.println("Top 10: ");
		for (int i = 0; i < topHitTable.length; i++) {
			finalTopHitTable[i] = (double)topHitTable[i]/numPredict;
			System.out.println(i + " : " + (double)topHitTable[i]/numPredict);
			PredictionTest.printWriter.println((double)topHitTable[i]/numPredict);
		}
		//		printWriter.close();





		/*
		List<ObservationInteger> predictSeq;
		for (int i = 0; i < tSet.size(); i++) {
			List<ObservationInteger> seq = tSet.get(i);
			int hold = holds.get(i);
			//System.out.println(hold);
			rank = new ArrayList<>();
			for (int elem = 0; elem < methodIDs.size(); elem++) {
				predictSeq = new ArrayList<>(seq);
				predictSeq.set(hold, new ObservationInteger(elem));
				rank.add(Pair.of(elem, hmm.lnProbability(predictSeq.subList(0, hold+1))));
			}
			Collections.sort(rank, new Comparator<Pair<Integer, Double>>() {
				@Override
				public int compare(Pair<Integer, Double> o1,
						Pair<Integer, Double> o2) {
					return -Double.compare(o1.getRight(), o2.getRight());
				}	
			});
			//System.out.println(rank);
			for (int j = 0; j < rank.size(); j++) {
				if (seq.get(hold).value == rank.get(j).getLeft().intValue()) {
					if (j < 10) {
						for (int k = j; k < topHitTable.length; k++) {
							topHitTable[k]++;
						}
					}
					break;
				}
			}
			numPredict++;
		}
		//System.out.println("Top 10: ");
		for (int i = 0; i < topHitTable.length; i++) {
			finalTopHitTable[i] = (double)topHitTable[i]/numPredict;
			//System.out.println(i + " : " + (double)topHitTable[i]/numPredict);
		}
		 */

	}

	private Map<ImmutableList<Integer>, Map<Integer, Double>> nGramModel;
	private Map<ImmutableList<Integer>, Integer> triGramMap;
	private Map<ImmutableList<Integer>, Integer> duoGramMap;

	public void trainNGramModel() {
		triGramMap = new HashMap<>();
		duoGramMap = new HashMap<>();
		nGramModel = new HashMap<>();
		int twoPrev, onePrev, it;
		for (List<ObservationInteger> sequence: trainingSet) {
			for (int i = 1; i < sequence.size(); i++) {
				if (i == 0) {
					twoPrev = -1;
					onePrev = -1;
				} else if (i == 1) {
					twoPrev = -1;
					onePrev = sequence.get(i-1).value;
				} else {
					twoPrev = sequence.get(i-2).value;
					onePrev= sequence.get(i-1).value;
				}
				it = sequence.get(i).value;
				//collect 3-gram
				ImmutableList<Integer> triGram = ImmutableList.of(twoPrev, onePrev, it);				
				Integer trigramCount = triGramMap.get(triGram);
				if (trigramCount == null) triGramMap.put(triGram, 1);
				else triGramMap.put(triGram, trigramCount+1);

				//collect 2-gram
				ImmutableList<Integer> duoGram = ImmutableList.of(onePrev, it);
				Integer duoGramCount = duoGramMap.get(duoGram);
				if (duoGramCount == null) duoGramMap.put(duoGram, 1);
				else duoGramMap.put(duoGram, duoGramCount+1);
				if (i == 1) {
					duoGram = ImmutableList.of(twoPrev, onePrev);
					duoGramCount = duoGramMap.get(duoGram);
					if (duoGramCount == null) duoGramMap.put(duoGram, 1);
					else duoGramMap.put(duoGram, duoGramCount+1);
				}
			}
		}

		for (ImmutableList<Integer> triGram: triGramMap.keySet()) {
			ImmutableList<Integer> duoGram = ImmutableList.copyOf(triGram.subList(0, triGram.size()-1));
			if (!nGramModel.containsKey(duoGram)) nGramModel.put(duoGram, new HashMap<Integer, Double>());
			int triGramCount = triGramMap.get(triGram);
			int duoGramCount = duoGramMap.get(duoGram);
			nGramModel.get(duoGram).put(triGram.get(triGram.size()-1),(double)triGramCount/duoGramCount);
		}
	}

	public void testNGramModel() {
		System.out.println("testNGram");

		Arrays.fill(topHitTable, 0);
		Arrays.fill(finalTopHitTable, 0);
		List<Pair<Integer, Double>> rank;
		int numPredict = 0;
		List<ObservationInteger> predictSeq;
		int twoPrev, onePrev, it;



		for (List<ObservationInteger> seq: testSet) {
			for (int i = 0; i < seq.size(); i++) {
				if (i == 0) {
					twoPrev = -1;
					onePrev = -1;
				} else if (i == 1) {
					twoPrev = -1;
					onePrev = seq.get(i-1).value;
				} else {
					twoPrev = seq.get(i-2).value;
					onePrev= seq.get(i-1).value;
				}
				ImmutableList<Integer> duoGram = ImmutableList.of(twoPrev, onePrev);
				Map<Integer, Double> pMap = nGramModel.get(duoGram);
				//				if (pMap == null) {
				//					numPredict++;
				//					continue;
				//				}
				rank = new ArrayList<>();
				for (int elem = 0; elem < methodIDs.size(); elem++) {
					it = elem;
					if (pMap == null) rank.add(Pair.of(it, -0.1));
					else {
						if (pMap.containsKey(it)) rank.add(Pair.of(it, pMap.get(it)));
						else rank.add(Pair.of(it, -0.1));
					}
				}
				Collections.shuffle(rank);
				Collections.sort(rank, new Comparator<Pair<Integer, Double>>() {
					@Override
					public int compare(Pair<Integer, Double> o1,
							Pair<Integer, Double> o2) {
						return -Double.compare(o1.getRight(), o2.getRight());
					}	
				});

				for (int j = 0; j < rank.size(); j++) {
					if (seq.get(i).value == rank.get(j).getLeft().intValue()) {
						if (j < 10) {
							for (int k = j; k < topHitTable.length; k++) {
								topHitTable[k]++;
							}
						}
						break;
					}
				}
				numPredict++;
			}
		}
		System.out.println("Top 10: ");
		for (int i = 0; i < topHitTable.length; i++) {
			finalTopHitTable[i] = (double)topHitTable[i]/numPredict;
			System.out.println(i + " : " + (double)topHitTable[i]/numPredict);
			PredictionTest.printWriter.println((double)topHitTable[i]/numPredict);

		}




		/*
		for (int i = 0; i < testSet.size(); i++) {
			List<ObservationInteger> seq = testSet.get(i);
			int hold = holds.get(i);
			rank = new ArrayList<>();
			if (hold == 0) {
				twoPrev = -1;
				onePrev = -1;
			} else if (hold == 1) {
				twoPrev = -1;
				onePrev = seq.get(hold-1).value;
			}  else {
				twoPrev = seq.get(hold-2).value;
				onePrev= seq.get(hold-1).value;
			}
			ImmutableList<Integer> duoGram = ImmutableList.of(twoPrev, onePrev);
			Map<Integer, Double> pMap = nGramModel.get(duoGram);
			//			if (pMap == null) {
			//				numPredict++;
			//				continue;
			//			}
			for (int elem = 0; elem < methodIDs.size(); elem++) {
				it = elem;
				if (pMap == null) rank.add(Pair.of(it, -0.1));
				else {
					if (pMap.containsKey(it)) rank.add(Pair.of(it, pMap.get(it)));
					else rank.add(Pair.of(it, -0.1));
				}
			}
			Collections.shuffle(rank);
			Collections.sort(rank, new Comparator<Pair<Integer, Double>>() {
				@Override
				public int compare(Pair<Integer, Double> o1,
						Pair<Integer, Double> o2) {
					return -Double.compare(o1.getRight(), o2.getRight());
				}	
			});
			for (int j = 0; j < rank.size(); j++) {
				if (seq.get(hold).value == rank.get(j).getLeft().intValue()) {
					if (j < 10) {
						for (int k = j; k < topHitTable.length; k++) {
							topHitTable[k]++;
						}
					}
					break;
				}
			}
			numPredict++;
		}
		//System.out.println("Top 10: ");
		for (int i = 0; i < topHitTable.length; i++) {
			finalTopHitTable[i] = (double)topHitTable[i]/numPredict;
			//System.out.println(i + " : " + (double)topHitTable[i]/numPredict);
		}

		 */
	}
	public void testUniformModel() {
		System.out.println("testNGram");

		Arrays.fill(topHitTable, 0);
		Arrays.fill(finalTopHitTable, 0);
		List<Pair<Integer, Double>> rank = new ArrayList<>();
		int numPredict = 0;
		for (int i = 0; i < methodIDs.size(); i++) {
			rank.add(Pair.of(i, 0.0));
		}
		
		for (List<ObservationInteger> seq: testSet) {
			for (int i = 0; i < seq.size(); i++) {
				Collections.shuffle(rank);
				for (int j = 0; j < rank.size(); j++) {
					if (seq.get(i).value == rank.get(j).getLeft().intValue()) {
						if (j < 10) {
							for (int k = j; k < topHitTable.length; k++) {
								topHitTable[k]++;
							}
						}
						break;
					}
				}
				numPredict++;
			}
		}
		System.out.println("Top 10: ");
		for (int i = 0; i < topHitTable.length; i++) {
			finalTopHitTable[i] = (double)topHitTable[i]/numPredict;
			System.out.println(i + " : " + (double)topHitTable[i]/numPredict);
		}
		
	}

	public static void main(String[] args) throws Exception {
		PredictionTest predictionTest = new PredictionTest(244);
		predictionTest.splitTrainingAndTestSets();
		predictionTest.trainHMM();
		predictionTest.trainNGramModel();
		predictionTest.testNGramModel();
		//predictionTest.testUniformModel();
		//train rnn
		ProcessBuilder processBuilder = new ProcessBuilder("/bin/bash", "/home/tamnt/Documents/workspace/AndroidPM-HMM/rnntest.sh");
		Process process = processBuilder.start();
		process.waitFor();
		//test rnn
		
//		TestRNN testRNN = new TestRNN();
//		testRNN.readTestSet("test.txt");
//		testRNN.readTestResult("test.score.txt");
//		testRNN.produceFinalResult();
//		PredictionTest.printWriter.close();
		/*
		//		PredictionTest predictionTest = new PredictionTest(1364);
		//		predictionTest.splitTrainingAndTestSets();
		//		predictionTest.trainHMM();
		Stopwatch stopwatch = Stopwatch.createStarted();
		PredictionTest predictionTest;
		PrintWriter nbStatesWriter = new PrintWriter("multiple-nbStates.txt");
		for (int i = 0;i < 30000; i++) {
			if (i  <= 14000 || i >= 15000) {
			try {				
				predictionTest = new PredictionTest(i);
				predictionTest.splitTrainingAndTestSets();
				predictionTest.trainHMM();
				(new GenericHmmDrawerDot()).write(predictionTest.finalHmm, "hmm-multiple/" + i + ".dot");
				nbStatesWriter.println(predictionTest.finalHmm.nbStates());
				System.out.println(i);


			} catch (Exception e) {
				continue;
			}
			}
		}
		nbStatesWriter.close();
		 */
		/*
		PredictionTest predictionTest;
		double[] topHitTable1 = new double[10];
		double[] tempTopHitTable1 = new double[10];
		double[] topHitTable2 = new double[10];
		double[] tempTopHitTable2 = new double[10];
		int j = 0;
		for (int i = 0;i < 3000; i++) {
			if ( i <= 14000 || i >= 15000) {
				try {
					predictionTest = new PredictionTest(i);
					predictionTest.splitTrainingAndTestSets();
					predictionTest.trainHMM();
					boolean isNaN = false;
					for (int k = 0; k < 10; k++ ) {
						if (Double.isNaN(predictionTest.finalTopHitTable[k])) {
							isNaN = true;
							break;
						}
					}
					if (isNaN) continue;
					for (int k = 0; k < 10; k++ ) {
						tempTopHitTable1[k] = predictionTest.finalTopHitTable[k];
					}

					predictionTest.trainNGramModel();
					predictionTest.testNGramModel();
					isNaN = false;
					for (int k = 0; k < 10; k++ ) {
						if (Double.isNaN(predictionTest.finalTopHitTable[k])) {
							isNaN = true;
							break;
						}
					}
					if (isNaN) continue;
					for (int k = 0; k < 10; k++ ) {
						tempTopHitTable2[k] = predictionTest.finalTopHitTable[k];
					}
					if (Double.compare(tempTopHitTable1[1], tempTopHitTable2[1]) >= 0) {
						for (int k = 0; k < 10; k++ ) {
							topHitTable1[k] += tempTopHitTable1[k];
							topHitTable2[k] += tempTopHitTable2[k];
						}

						j++;
					}


				} catch (Exception e) {
					//e.printStackTrace();
					continue;

				}
				System.out.println(i);
			}
		}
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		System.out.println(Arrays.toString(topHitTable1));
		System.out.println(Arrays.toString(topHitTable2));
		System.out.println(j);
		for (int k = 0; k < 10; k++ ) {
			topHitTable1[k] = topHitTable1[k] / j;
			topHitTable2[k] = topHitTable2[k] / j;
		}

		for (int k = 1; k <= 10; k++) {
			System.out.println("Top " + k + ", " + df.format(topHitTable1[k-1]*100));
		}
		for (int k = 1; k <= 10; k++) {
			System.out.println("Top " + k + ", " + df.format(topHitTable2[k-1]*100));
		}
		 */
		// Calculate the running time
		/*
		stopwatch.stop();
		System.out.println("Processing time: " + stopwatch.toString());
		 */
	}
	
	
}
