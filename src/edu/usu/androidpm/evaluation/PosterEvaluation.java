package edu.usu.androidpm.evaluation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import be.ac.ulg.montefiore.run.jahmm.Hmm;
import be.ac.ulg.montefiore.run.jahmm.Observation;
import be.ac.ulg.montefiore.run.jahmm.ObservationInteger;
import be.ac.ulg.montefiore.run.jahmm.Opdf;
import be.ac.ulg.montefiore.run.jahmm.OpdfInteger;
import be.ac.ulg.montefiore.run.jahmm.OpdfIntegerFactory;
import be.ac.ulg.montefiore.run.jahmm.ViterbiCalculator;
import be.ac.ulg.montefiore.run.jahmm.draw.GenericHmmDrawerDot;
import be.ac.ulg.montefiore.run.jahmm.learn.BaumWelchLearner;
import be.ac.ulg.montefiore.run.jahmm.learn.KMeansLearner;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableList;

import edu.usu.androidpm.util.GraphViz;
import usu.useal.salad.evaluation.DatabaseLoader;

public class PosterEvaluation {
	private DatabaseLoader databaseLoader;
	private List<MutablePair<ImmutableList<Integer>	, Integer>> rawDataset;
	private List<List<ObservationInteger>> dataset;
	private List<List<ObservationInteger>> testSet;
	private BiMap<Integer, Integer> methodIDs;
	private Hmm<ObservationInteger> hmm;
	private int nbStates = 8;
	private static final double ALPHA = 0.001;

	
	public PosterEvaluation() throws Exception {
		databaseLoader = new DatabaseLoader();
		rawDataset = databaseLoader.loadDatasetForObject(494);
		methodIDs = HashBiMap.create();
		methodIDs.put(2740, 0);
		methodIDs.put(2736, 1);
		methodIDs.put(16282, 2);
		methodIDs.put(2737, 3);
		methodIDs.put(2738, 4);
		methodIDs.put(19026, 5);
		methodIDs.put(2735, 6);
		methodIDs.put(19025, 7);
		methodIDs.put(19024, 8);
		methodIDs.put(16283, 9);
		methodIDs.put(2739, 10);
		methodIDs.put(2742, 11);
		methodIDs.put(2743, 12);
		methodIDs.put(2741, 13);
		filterRawDataset();
	}
	
	private void filterRawDataset() {
		testSet = new ArrayList<>();
		dataset = new ArrayList<>();
		List<ObservationInteger> sequence;
		for (MutablePair<ImmutableList<Integer>	, Integer> pair: rawDataset) {
			sequence = new ArrayList<>();
			for (Integer methodID: pair.getLeft()) if (methodIDs.containsKey(methodID)) sequence.add(new ObservationInteger(methodIDs.get(methodID)));
			if (sequence.size() >= 2) {
				for (int i = 0; i < pair.right; i++) dataset.add(sequence);
				//testSet.add(sequence);
				System.out.println(sequence);
				System.out.println(pair.right);
			}
		}
	}
	
	Pair<List<List<ObservationInteger>>, List<List<ObservationInteger>>> splitDataset(List<List<ObservationInteger>> dataset, double portion) {
		int trainingSize = (int)Math.round(dataset.size() * portion);
		List<Integer> indexes = new ArrayList<>();
		for (int i = 0; i < dataset.size(); i++) indexes.add(i);
		Collections.shuffle(indexes);
		List<List<ObservationInteger>> trainingSet = new ArrayList<>();
		for (int i = 0; i < trainingSize; i++) trainingSet.add(dataset.get(indexes.get(i)));
		List<List<ObservationInteger>> testSet = new ArrayList<>();
		for (int i = trainingSize; i < dataset.size(); i++) testSet.add(dataset.get(indexes.get(i)));
		return Pair.of(trainingSet, testSet);
	}
	
	Pair<List<List<ObservationInteger>>, List<List<ObservationInteger>>> pairDatasets;
	Pair<List<List<ObservationInteger>>, List<List<ObservationInteger>>> pairDatasets2;
	List<Integer> holds;
	public void trainHMM() throws Exception {
		pairDatasets = splitDataset(dataset, 0.8);
		pairDatasets2 = splitDataset(pairDatasets.getLeft(), 0.8);

		List<Double> lnLikelihoods = new ArrayList<>();
		int minNbStates = 2;
		int maxNbStates = 30;
		double lnLikelihood;
		for (int i = minNbStates; i <= maxNbStates; i++) {
			lnLikelihood = 0;
			KMeansLearner<ObservationInteger> kml = new KMeansLearner<ObservationInteger>(i, new OpdfIntegerFactory(methodIDs.size()), pairDatasets2.getLeft());
			Hmm<ObservationInteger> hmm = kml.iterate();
			BaumWelchLearner bwl = new BaumWelchLearner();
			hmm = bwl.learn(hmm, pairDatasets2.getLeft());		
			smoothHmm(hmm);
			for (List<ObservationInteger> seq: pairDatasets2.getRight()) {
				lnLikelihood += hmm.lnProbability(seq);
				if (hmm.lnProbability(seq) == Double.NEGATIVE_INFINITY) {
					(new GenericHmmDrawerDot()).write(hmm, "error_hmm.dot");
					System.out.println(seq);
				}
			}

			lnLikelihoods.add(lnLikelihood);
		}
		int bestNbStates = minNbStates + maxIndex(lnLikelihoods);
		System.out.println(bestNbStates);
		KMeansLearner<ObservationInteger> kml = new KMeansLearner<ObservationInteger>(11, new OpdfIntegerFactory(methodIDs.size()), pairDatasets.getLeft());
		Hmm<ObservationInteger> hmm = kml.iterate();
		BaumWelchLearner bwl = new BaumWelchLearner();
		hmm = bwl.learn(hmm, pairDatasets.getLeft());
		smoothHmm(hmm);
		holds = generateHolds(pairDatasets.getRight());
		testHMM(hmm, pairDatasets.getRight(), holds);
//		Pair<List<List<ObservationInteger>>, List<List<ObservationInteger>>> pairDatasets = splitDataset(dataset, 0.8);
//		KMeansLearner<ObservationInteger> kml = new KMeansLearner<>(nbStates, new OpdfIntegerFactory(methodIDs.size()), pairDatasets.getLeft());
//		hmm = kml.iterate();
//		BaumWelchLearner bwl = new BaumWelchLearner();
//		hmm = bwl.learn(hmm, pairDatasets.getLeft());
//		(new GenericHmmDrawerDot()).write(hmm, "final_hmm.dot");
	}
	
	List<Integer> generateHolds(List<List<ObservationInteger>> tSet) {
		List<Integer> result = new ArrayList<>();
	    Random rand = new Random();
		for (List<ObservationInteger> sequence: tSet) {
			result.add(rand.nextInt(sequence.size()));
		}
		return result;
	}
	
	
	public void smoothHmm(Hmm<ObservationInteger> hmm) {
		double prob;
		int nbStates = hmm.nbStates();
		int nbObservations = methodIDs.size();
		for (int i = 0; i < nbStates; i++) {
			prob = hmm.getPi(i);
			hmm.setPi(i, (prob + ALPHA)/(1. + nbStates*ALPHA));
		}

		for (int i = 0; i < nbStates; i++) {
			for (int j = 0; j < nbStates; j++) {
				prob = hmm.getAij(i, j);
				hmm.setAij(i, j, (prob + ALPHA)/(1. + nbStates*ALPHA));
			}
		}

		Opdf<ObservationInteger> opdf;
		double[] probs;
		for (int i = 0; i < nbStates; i++) {
			opdf = hmm.getOpdf(i);
			probs = new double[nbObservations];
			for (int j = 0; j < nbObservations; j++) {
				prob = opdf.probability(new ObservationInteger(j));
				probs[j] = (prob + ALPHA)/(1. + nbObservations*ALPHA);
			}
			hmm.setOpdf(i, new OpdfInteger(probs));
		}
	}
	
	public int [] topHitTable = new int[10];
	public double[] finalTopHitTable =  new double[10];
	
	void testHMM(Hmm<ObservationInteger> hmm, List<List<ObservationInteger>> tSet, List<Integer> holds) throws FileNotFoundException {
		List<Pair<Integer, Double>> rank;
		//PrintWriter printWriter = new PrintWriter(new File("predict.txt"));
		System.out.println("testHMM");
		int numPredict = 0;
		
		
		for (List<ObservationInteger> seq: tSet) {
			for (int i = 1; i < seq.size(); i++) {
				rank = new ArrayList<>();
				Map<Integer, Double> weightMap = new HashMap<>();
				List<Double> probList = new ArrayList<>();
				ViterbiCalculator viterbiCalculator = new ViterbiCalculator(seq.subList(0, i), hmm);
				int[] bestStateSeq = viterbiCalculator.stateSequence();
				System.out.println(Arrays.toString(bestStateSeq));
				//for (int j = 0; j < nbStates; j++) probList.add(hmm.getOpdf(j).probability(seq.get(i-1)));
				int bestState = bestStateSeq[bestStateSeq.length-1];
				//probList.clear();
				double probability;
				for (int m = 0; m < nbStates; m++) {
					for (int n = 0; n < methodIDs.size();n++) {
						probability = hmm.getAij(bestState, m) * hmm.getOpdf(m).probability(new ObservationInteger(n));
						Double prob = weightMap.get(n);
						if (prob == null) weightMap.put(n, probability);
						else weightMap.put(n, prob + probability);
					}
				}
				for (Map.Entry<Integer, Double> entry: weightMap.entrySet())
					rank.add(Pair.of(entry.getKey(), entry.getValue()));
				Collections.sort(rank, new Comparator<Pair<Integer, Double>>() {
					@Override
					public int compare(Pair<Integer, Double> o1,
							Pair<Integer, Double> o2) {
						return -Double.compare(o1.getRight(), o2.getRight());
					}	
				});
				for (int j = 0; j < rank.size(); j++) {
					if (seq.get(i).value == rank.get(j).getLeft().intValue()) {
						if (j < 10) {
							for (int k = j; k < topHitTable.length; k++) {
								topHitTable[k]++;
							}
						}
						break;
					}
				}
				numPredict++;
			}
		}
		System.out.println("Top 10: ");
		for (int i = 0; i < topHitTable.length; i++) {
			finalTopHitTable[i] = (double)topHitTable[i]/numPredict;
			System.out.println(i + " : " + (double)topHitTable[i]/numPredict);
		}
		
		
		/*
		List<ObservationInteger> predictSeq;
		for (int i = 0; i < tSet.size(); i++) {
			List<ObservationInteger> seq = tSet.get(i);
			int hold = holds.get(i);
			System.out.println(hold);
			rank = new ArrayList<>();
			for (int elem = 0; elem < methodIDs.size(); elem++) {
				predictSeq = new ArrayList<>(seq);
				predictSeq.set(hold, new ObservationInteger(elem));
				rank.add(Pair.of(elem, hmm.lnProbability(predictSeq)));
			}
			Collections.sort(rank, new Comparator<Pair<Integer, Double>>() {
				@Override
				public int compare(Pair<Integer, Double> o1,
						Pair<Integer, Double> o2) {
					return -Double.compare(o1.getRight(), o2.getRight());
				}	
			});
			System.out.println(rank);
			for (int j = 0; j < rank.size(); j++) {
				if (seq.get(hold).value == rank.get(j).getLeft().intValue()) {
					if (j < 10) {
						for (int k = j; k < topHitTable.length; k++) {
							topHitTable[k]++;
						}
					}
					break;
				}
			}
			numPredict++;
		}
		System.out.println("Top 10: ");
		for (int i = 0; i < topHitTable.length; i++) {
			finalTopHitTable[i] = (double)topHitTable[i]/numPredict;
			System.out.println(i + " : " + (double)topHitTable[i]/numPredict);
		}
		*/
		
		//printWriter.close();
		/*
		List<ObservationInteger> predictSeq;

		for (List<ObservationInteger> seq: tSet) {
			for (int i = 0; i < seq.size(); i++) {
				rank = new ArrayList<>();
				for (int elem = 0; elem < methodIDs.size(); elem++) {
					predictSeq = new ArrayList<>(seq.subList(0, i));
					predictSeq.add(new ObservationInteger(elem));
					rank.add(Pair.of(elem, hmm.lnProbability(predictSeq)));
				}
				Collections.sort(rank, new Comparator<Pair<Integer, Double>>() {
					@Override
					public int compare(Pair<Integer, Double> o1,
							Pair<Integer, Double> o2) {
						return -Double.compare(o1.getRight(), o2.getRight());
					}	
				});

				for (int j = 0; j < rank.size(); j++) {
					if (seq.get(i).value == rank.get(j).getLeft().intValue()) {
						if (j < 10) {
							for (int k = j; k < topHitTable.length; k++) {
								topHitTable[k]++;
							}
							//							if (j > 0) {
							//								printWriter.println("Previous sequence: ");
							//								for (ObservationInteger oi: seq.subList(0, i)) {
							//									printWriter.print(methodNameToID.inverse().get(oi.value) + ", ");
							//								}
							//								printWriter.println();
							//								printWriter.println("Actual method: ");
							//								printWriter.println(methodNameToID.inverse().get(seq.get(i).value));
							//								printWriter.println("Top Rank: ");
							//								for (int k = 0; k <= j; k++) {
							//									printWriter.println(methodNameToID.inverse().get(rank.get(k).getLeft().intValue()));
							//								}
							//							}

						}
						break;
					}
				}
				numPredict++;
			}
		}
		System.out.println("Top 10: ");
		for (int i = 0; i < topHitTable.length; i++) {
			finalTopHitTable[i] = (double)topHitTable[i]/numPredict;
			System.out.println(i + " : " + (double)topHitTable[i]/numPredict);
		}
		//printWriter.close();
		
		*/
	}
	
	private Map<ImmutableList<Integer>, Map<Integer, Double>> nGramModel;
	private Map<ImmutableList<Integer>, Integer> triGramMap;
	private Map<ImmutableList<Integer>, Integer> duoGramMap;

	public void trainNGramModel() {
		triGramMap = new HashMap<>();
		duoGramMap = new HashMap<>();
		nGramModel = new HashMap<>();
		int twoPrev, onePrev, it;
		for (List<ObservationInteger> sequence: pairDatasets.getLeft()) {
			for (int i = 0; i < sequence.size(); i++) {
				if (i == 0) {
					twoPrev = -1;
					onePrev = -1;
				} else if (i == 1) {
					twoPrev = -1;
					onePrev = sequence.get(i-1).value;
				} else {
					twoPrev = sequence.get(i-2).value;
					onePrev= sequence.get(i-1).value;
				}
				it = sequence.get(i).value;
				//collect 3-gram
				ImmutableList<Integer> triGram = ImmutableList.of(onePrev, it);				
				Integer trigramCount = triGramMap.get(triGram);
				if (trigramCount == null) triGramMap.put(triGram, 1);
				else triGramMap.put(triGram, trigramCount+1);

				//collect 2-gram
				ImmutableList<Integer> duoGram = ImmutableList.of(it);
				Integer duoGramCount = duoGramMap.get(duoGram);
				if (duoGramCount == null) duoGramMap.put(duoGram, 1);
				else duoGramMap.put(duoGram, duoGramCount+1);
				if (i == 0) {
					duoGram = ImmutableList.of(onePrev);
					duoGramCount = duoGramMap.get(duoGram);
					if (duoGramCount == null) duoGramMap.put(duoGram, 1);
					else duoGramMap.put(duoGram, duoGramCount+1);
				}
			}
		}

		for (ImmutableList<Integer> triGram: triGramMap.keySet()) {
			ImmutableList<Integer> duoGram = ImmutableList.copyOf(triGram.subList(0, triGram.size()-1));
			if (!nGramModel.containsKey(duoGram)) nGramModel.put(duoGram, new HashMap<Integer, Double>());
			int triGramCount = triGramMap.get(triGram);
			int duoGramCount = duoGramMap.get(duoGram);
			nGramModel.get(duoGram).put(triGram.get(triGram.size()-1),(double)triGramCount/duoGramCount);
		}
	}

	public void testNGramModel() {
		System.out.println("testNGram");

		Arrays.fill(topHitTable, 0);
		Arrays.fill(finalTopHitTable, 0);;
		List<Pair<Integer, Double>> rank;
		int numPredict = 0;
		List<ObservationInteger> predictSeq;
		int twoPrev, onePrev, it;
		
		/*
		for (int i = 0; i < pairDatasets.getRight().size(); i++) {
			List<ObservationInteger> seq = pairDatasets.getRight().get(i);
			int hold = holds.get(i);
			rank = new ArrayList<>();
			if (hold == 0) {
				twoPrev = -1;
				onePrev = -1;
			} else if (hold == 1) {
				twoPrev = -1;
				onePrev = seq.get(hold-1).value;
			}  else {
				twoPrev = seq.get(hold-2).value;
				onePrev= seq.get(hold-1).value;
			}
			ImmutableList<Integer> duoGram = ImmutableList.of(twoPrev, onePrev);
			Map<Integer, Double> pMap = nGramModel.get(duoGram);
			if (pMap == null) {
				numPredict++;
				continue;
			}
			for (int elem = 0; elem < methodIDs.size(); elem++) {
				it = elem;
				//if (pMap == null) rank.add(Pair.of(it, -0.1));
				if (pMap.containsKey(it)) rank.add(Pair.of(it, pMap.get(it)));
					//else rank.add(Pair.of(it, -0.1));
			}
			Collections.shuffle(rank);
			Collections.sort(rank, new Comparator<Pair<Integer, Double>>() {
				@Override
				public int compare(Pair<Integer, Double> o1,
						Pair<Integer, Double> o2) {
					return -Double.compare(o1.getRight(), o2.getRight());
				}	
			});
			for (int j = 0; j < rank.size(); j++) {
				if (seq.get(hold).value == rank.get(j).getLeft().intValue()) {
					if (j < 10) {
						for (int k = j; k < topHitTable.length; k++) {
							topHitTable[k]++;
						}
					}
					break;
				}
			}
			numPredict++;
		}
		System.out.println("Top 10: ");
		for (int i = 0; i < topHitTable.length; i++) {
			finalTopHitTable[i] = (double)topHitTable[i]/numPredict;
			System.out.println(i + " : " + (double)topHitTable[i]/numPredict);
		}
		*/
		
		
		for (List<ObservationInteger> seq: pairDatasets.getRight()) {
			for (int i = 0; i < seq.size(); i++) {
				if (i == 0) {
					twoPrev = -1;
					onePrev = -1;
				} else if (i == 1) {
					twoPrev = -1;
					onePrev = seq.get(i-1).value;
				} else {
					twoPrev = seq.get(i-2).value;
					onePrev= seq.get(i-1).value;
				}
				ImmutableList<Integer> duoGram = ImmutableList.of( onePrev);
				Map<Integer, Double> pMap = nGramModel.get(duoGram);
				if (pMap == null) {
					numPredict++;
					continue;
				}
				rank = new ArrayList<>();
				for (int elem = 0; elem < methodIDs.size(); elem++) {
					it = elem;
					//if (pMap == null) rank.add(Pair.of(it, -0.1));
					if (pMap.containsKey(it)) rank.add(Pair.of(it, pMap.get(it)));
						//else rank.add(Pair.of(it, -0.1));
				}
				Collections.shuffle(rank);
				Collections.sort(rank, new Comparator<Pair<Integer, Double>>() {
					@Override
					public int compare(Pair<Integer, Double> o1,
							Pair<Integer, Double> o2) {
						return -Double.compare(o1.getRight(), o2.getRight());
					}	
				});

				for (int j = 0; j < rank.size(); j++) {
					if (seq.get(i).value == rank.get(j).getLeft().intValue()) {
						if (j < 10) {
							for (int k = j; k < topHitTable.length; k++) {
								topHitTable[k]++;
							}
						}
						break;
					}
				}
				numPredict++;
			}
		}
		System.out.println("Top 10: ");
		for (int i = 0; i < topHitTable.length; i++) {
			finalTopHitTable[i] = (double)topHitTable[i]/numPredict;
			System.out.println(i + " : " + (double)topHitTable[i]/numPredict);
		}
		
	}
	
	private int maxIndex(List<Double> seqs) {
		double max = Double.NEGATIVE_INFINITY;
		int maxIndex = -1;
		for (int i = 0; i < seqs.size(); i++) {
			if (max < seqs.get(i)) {
				max = seqs.get(i);
				maxIndex = i;
			}
		}
		return maxIndex;
	}
	
	public void writeHMM() {
		Map<Integer, String> labelMap = new HashMap<>();
//		labelMap.put(0, "Error");
//		labelMap.put(1, "Initial");
//		labelMap.put(2, "Initial");
//		labelMap.put(3, "Initialized");
//		labelMap.put(4, "DataSourceConfigured");
//		labelMap.put(5, "DataSourceConfigured");
//		labelMap.put(6, "DataSourceConfigured");
//		labelMap.put(7, "DataSourceConfigured");
//		labelMap.put(8, "DataSourceConfigured");
//		labelMap.put(9, "DataSourceConfigured");
//		labelMap.put(10, "Prepared");
//		labelMap.put(11, "Recording");
//		labelMap.put(12, "Recording");
		GraphViz gv = new GraphViz();
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		gv.addln(gv.start_graph());
		gv.addln("graph [autosize=false, size=\"50,50!\", resolution=100];\n");
		gv.addln("node [height=0.02, width=0.01];\n");
		gv.addln("graph [bb=\"0,0,100,50\"];\n");
		for (int i = 0; i < nbStates; i++) {
			List<Integer> nextStates = findTheLargestAndSecondLargestIndex(hmm, i);
			List<Integer> methodIndexs = findTheLargestAndSecondLargestIndex(hmm.getOpdf(i));
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(i + " [shape=");
			if (hmm.getPi(i) > 0.0001) {
				stringBuilder.append("doublecircle, label=\"");
				stringBuilder.append("Pi = " + df.format(hmm.getPi(i)) + "\n");
			} else {
				stringBuilder.append("circle, label=\"");
			}
			for (int methodIndex: methodIndexs) {
				stringBuilder.append(getName(databaseLoader.getMethodNamesBiMap().get(methodIDs.inverse().get(methodIndex))) + " - " + df.format(hmm.getOpdf(i).probability(new ObservationInteger(methodIndex))) + "\n");
			}
			stringBuilder.append("\"];\n");
			for (int nextState: nextStates) {
				stringBuilder.append(i + " -> " + nextState);
				stringBuilder.append("[label=" + df.format(hmm.getAij(i, nextState)) + "];\n");
			}
			gv.add(stringBuilder.toString());
		}
		gv.addln(gv.end_graph());
		String type = "png";
		File outFile = new File("/home/tamnt/Documents/Poster/testCFG.png");
		gv.writeGraphToFile( gv.getGraph( gv.getDotSource(), type ), outFile );
	}
	
	private static String getName(String str) {
		String[] parts = str.split("[>(]");
		return parts[parts.length-2] + "()";
	}
	
	private List<Integer> findTheLargestAndSecondLargestIndex(Hmm<ObservationInteger> hmm, int stateNb) {
		List<Integer> result = new ArrayList<>(2);
		result.add(0);
		result.add(0);
		double max1 = -1;
		double max2 = -1;
		for (int i = 0; i < hmm.nbStates();i++) {
			double probability = hmm.getAij(stateNb, i);
			if (max1 < probability) {
				max2 = max1;
				max1 = probability;
				result.set(1, result.get(0));
				result.set(0, i);
			} else if (max2 < probability) {
				max2 = probability;
				result.set(1, i);
			}
		}
		return result;
	}
	
	private List<Integer> findTheLargestAndSecondLargestIndex(Opdf<ObservationInteger> pdf) {
		List<Integer> result = new ArrayList<>(2);
		result.add(0);
		result.add(0);
		double max1 = -1;
		double max2 = -1;
		for (int i = 0; i < methodIDs.size();i++) {
			double probability = pdf.probability(new ObservationInteger(i));
			if (max1 < probability) {
				max2 = max1;
				max1 = probability;
				result.set(1, result.get(0));
				result.set(0, i);
			} else if (max2 < probability) {
				max2 = probability;
				result.set(1, i);
			}
		}
		return result;
	}
	
	public static void main(String[] args) throws Exception {
		PosterEvaluation posterEvaluation = new PosterEvaluation();
		posterEvaluation.trainHMM();
		posterEvaluation.trainNGramModel();
		posterEvaluation.testNGramModel();
		//posterEvaluation.writeHMM();
	}
	
}
