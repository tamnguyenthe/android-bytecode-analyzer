package edu.usu.androidpm.cfg.node;

import edu.usu.androidpm.util.SetUtil;
import gnu.trove.set.hash.TIntHashSet;

public class PrimitiveNode extends DataNode {
	
	
	public PrimitiveNode(int id, int offset) {
		super(id, offset);
	}
	
	public PrimitiveNode(int id, int offset, String type, int register) {
		super(id, offset, type, register);
	}

	public PrimitiveNode(int id, int offset, String type, int register,
			int actionNodeID) {
		super(id, offset, type, register, actionNodeID);
	}

	public PrimitiveNode(int id, int offset, String type, int register,
			int actionNodeID, int hostObjectID) {
		super(id, offset, type, register, actionNodeID, hostObjectID);
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof PrimitiveNode)) return false;
		PrimitiveNode primitiveNode = (PrimitiveNode)o;
		return getType().equals(primitiveNode.getType())
				&& SetUtil.hasIntersection(getRegisters(), primitiveNode.getRegisters())
				&& getHostObjectID() == primitiveNode.getHostObjectID();
				//&& getActionNodeID() == primitiveNode.getActionNodeID();
	}

	@Override
	public Node getCopy() {
		PrimitiveNode primitiveNode = new PrimitiveNode(getID(), getOffset());
		primitiveNode.setType(getType());
		primitiveNode.setRegisters(new TIntHashSet());
		primitiveNode.getRegisters().addAll(getRegisters());
		primitiveNode.setActionNodeID(getActionNodeID());
		primitiveNode.setHostObjectID(getHostObjectID());
		primitiveNode.getControlEdges().addAllOf(getControlEdges());
		primitiveNode.getDataEdges().addAllOf(getDataEdges());
		return primitiveNode;
	}
}
