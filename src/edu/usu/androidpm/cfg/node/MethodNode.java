package edu.usu.androidpm.cfg.node;

import org.jf.dexlib2.iface.reference.MethodReference;


public class MethodNode extends ActionNode {

	private MethodReference reference;

	public MethodNode(int id,  int offset, short opcode, MethodReference methodReference) {
		super(id, offset, opcode);
		setReference(methodReference);
	}

	public MethodReference getReference() {
		return reference;
	}


	public void setReference(MethodReference reference) {
		this.reference = reference;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof MethodNode)) return false;
		MethodNode methodNode = (MethodNode)o;
		return getOpcode() == methodNode.getOpcode()
				&& getHostObjectID() == methodNode.getHostObjectID()
				&& getReference().equals(methodNode.getReference());
	}

	@Override
	public Node getCopy() {
		MethodNode methodNode = new MethodNode(getID(), getOffset(), getOpcode(), getReference());
		methodNode.getInputRegisters().addAllOf(getInputRegisters());
		methodNode.setOutputRegister(getOutputRegister());
		methodNode.setOutputType(getOutputType());
		methodNode.setHostObjectID(getHostObjectID());
		methodNode.getControlEdges().addAllOf(getControlEdges());
		methodNode.getDataEdges().addAllOf(getDataEdges());
		return methodNode;
	}


	
}
