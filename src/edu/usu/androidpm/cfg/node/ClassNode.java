package edu.usu.androidpm.cfg.node;

import edu.usu.androidpm.util.SetUtil;
import gnu.trove.set.hash.TIntHashSet;

public class ClassNode extends DataNode {
	
	public ClassNode(int id, int offset) {
		super(id, offset);
	}
	
	public ClassNode(int id, int offset, String type, int register) {
		super(id, offset, type, register);
	}

	public ClassNode(int id, int offset, String type, int register,
			int actionNodeID) {
		super(id, offset, type, register, actionNodeID);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof ClassNode)) return false;
		ClassNode classNode = (ClassNode)o;
		return getType().equals(classNode.getType())
				&& SetUtil.hasIntersection(getRegisters(), classNode.getRegisters())
				&& getHostObjectID() == classNode.getHostObjectID();
				//&& getActionNodeID() == classNode.getActionNodeID();
	}

	@Override
	public Node getCopy() {
		ClassNode classNode = new ClassNode(getID(), getOffset());
		classNode.setType(getType());
		classNode.setRegisters(new TIntHashSet());
		classNode.getRegisters().addAll(getRegisters());
		classNode.setActionNodeID(getActionNodeID());
		classNode.setHostObjectID(getHostObjectID());
		classNode.getControlEdges().addAllOf(getControlEdges());
		classNode.getDataEdges().addAllOf(getDataEdges());
		return classNode;
	}
}
