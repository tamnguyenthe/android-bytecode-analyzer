package edu.usu.androidpm.cfg.node;

import org.jf.dexlib2.iface.reference.FieldReference;


public class AccessorNode extends ActionNode {
	
	private FieldReference reference;


	public AccessorNode(int id, int offset, short opcode, FieldReference fieldReference) {
		super(id, offset, opcode);
		setReference(fieldReference);
		
	}
	
	public FieldReference getReference() {
		return reference;
	}


	public void setReference(FieldReference reference) {
		this.reference = reference;
	}
	
	
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof AccessorNode)) return false;
		AccessorNode accessorNode = (AccessorNode) o;
		return getOpcode() == accessorNode.getOpcode()
			&& getHostObjectID() == accessorNode.getHostObjectID()
			&& getReference().equals(accessorNode.getReference());
	}

	@Override
	public Node getCopy() {
		AccessorNode accessorNode = new AccessorNode(getID(), getOffset(), getOpcode(), getReference());
		accessorNode.getInputRegisters().addAllOf(getInputRegisters());
		accessorNode.setOutputRegister(getOutputRegister());
		accessorNode.setOutputType(getOutputType());
		accessorNode.setHostObjectID(getHostObjectID());
		accessorNode.getControlEdges().addAllOf(getControlEdges());
		accessorNode.getDataEdges().addAllOf(getDataEdges());
		return accessorNode;
	}
}
