package edu.usu.androidpm.cfg.node;

public class OperationNode extends ActionNode {

	public OperationNode(int id, int offset, short opcode) {
		super(id, offset, opcode);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof OperationNode)) return false;
		OperationNode operationNode = (OperationNode)o;
		return getOpcode() == operationNode.getOpcode()
			&& getHostObjectID() == operationNode.getHostObjectID();
	}

	@Override
	public Node getCopy() {
		OperationNode operationNode = new OperationNode(getID(), getOffset(), getOpcode());
		operationNode.getInputRegisters().addAllOf(getInputRegisters());
		operationNode.setOutputRegister(getOutputRegister());
		operationNode.setOutputType(getOutputType());
		operationNode.setHostObjectID(getHostObjectID());
		operationNode.getControlEdges().addAllOf(getControlEdges());
		operationNode.getDataEdges().addAllOf(getDataEdges());
		return operationNode;
	}

}
