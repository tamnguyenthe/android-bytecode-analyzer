package edu.usu.androidpm.cfg.node;

import edu.usu.androidpm.util.SetUtil;
import gnu.trove.set.hash.TIntHashSet;

public class ObjectNode extends DataNode {
	
	public ObjectNode(int id, int offset) {
		super(id, offset);
	}
	
	public ObjectNode(int id, int offset, String type, int register) {
		super(id, offset, type, register);
	}

	public ObjectNode(int id, int offset, String type, int register,
			int actionNodeID) {
		super(id, offset, type, register, actionNodeID);
	}

	public ObjectNode(int id, int offset, String type, int register,
			int actionNodeID, int hostObjectID) {
		super(id, offset, type, register, actionNodeID, hostObjectID);
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof ObjectNode)) return false;
		ObjectNode objectNode = (ObjectNode)o;
		return getType().equals(objectNode.getType())
			&& SetUtil.hasIntersection(getRegisters(), objectNode.getRegisters())
			&& getHostObjectID() == objectNode.getHostObjectID();
			//&& getActionNodeID() == objectNode.getActionNodeID();
	}

	@Override
	public Node getCopy() {
		ObjectNode objectNode = new ObjectNode(getID(), getOffset());
		objectNode.setType(getType());
		objectNode.setRegisters(new TIntHashSet());
		objectNode.getRegisters().addAll(getRegisters());
		objectNode.setActionNodeID(getActionNodeID());
		objectNode.setHostObjectID(getHostObjectID());
		objectNode.getControlEdges().addAllOf(getControlEdges());
		objectNode.getDataEdges().addAllOf(getDataEdges());
		return objectNode;
	}

}
