package edu.usu.androidpm.cfg.node;


import cern.colt.list.IntArrayList;
public abstract class ActionNode extends Node {

	private final short opcode;
	private IntArrayList inputRegisters;
	private int outputRegister;
	private String outputType;
	private int hostObjectID;
	

	protected ActionNode(int id, int offset, short opcode) {
		super(id, offset);
		this.opcode = opcode;
		this.inputRegisters = new IntArrayList();
		this.outputRegister = -1;
		this.outputType = null;
		this.hostObjectID = -1;
	}

	public short getOpcode() {
		return opcode;
	}
	
	public IntArrayList getInputRegisters() {
		return inputRegisters;
	}
	
	public void setInputRegisters(IntArrayList inputRegisters) {
		this.inputRegisters = inputRegisters;
	}

	public int getOutputRegister() {
		return outputRegister;
	}

	public void setOutputRegister(int outputRegister) {
		this.outputRegister = outputRegister;
	}

	public String getOutputType() {
		return outputType;
	}

	public void setOutputType(String outputType) {
		this.outputType = outputType;
	}
	
	public int getHostObjectID() {
		return hostObjectID;
	}

	public void setHostObjectID(int hostObjectID) {
		this.hostObjectID = hostObjectID;
	}
	
	@Override
	public String toDot() {
		StringBuilder buf = new StringBuilder();
		buf.append(this.getID() + " ");
		buf.append("[shape=box,label= \"" + this.getClass().getSimpleName() + " " + this.getID() + " (" + Integer.toHexString(getOffset()) + ") " + "\"];\n");
		for (int i = 0; i < this.getControlEdges().size();i++) {
			buf.append(this.getID() + " -> " + this.getControlEdges().get(i) + " [color=black];\n");
		}
		for (int i = 0; i < this.getDataEdges().size();i++) {
			buf.append(this.getID() + " -> " + this.getDataEdges().get(i) + "[color=red];\n");
		}
		return buf.toString();
	}	
}
