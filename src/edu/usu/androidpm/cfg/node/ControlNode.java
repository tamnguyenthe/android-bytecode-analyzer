package edu.usu.androidpm.cfg.node;

import cern.colt.list.IntArrayList;


public class ControlNode extends Node {
	private final short opcode;
	private IntArrayList inputRegisters;
	
	public ControlNode(int id, int offset, short opcode) {
		super(id, offset);
		this.opcode = opcode;
		this.inputRegisters = new IntArrayList();
	}
	
	public short getOpcode() {
		return opcode;
	}
	
	public void setInputRegisters(IntArrayList inputRegisters) {
		this.inputRegisters = inputRegisters;
	}

	public IntArrayList getInputRegisters() {
		return inputRegisters;
	}

	
	@Override
	public String toDot() {
		StringBuilder buf = new StringBuilder();
		buf.append(this.getID() + " ");
		buf.append("[shape=diamond,label= \"" + this.getClass().getSimpleName() + " " + this.getID() + "\"];\n");
		for (int i = 0; i < this.getControlEdges().size();i++) {
			buf.append(this.getID() + " -> " + this.getControlEdges().get(i) + " [color=black];\n");
		}
		for (int i = 0; i < this.getDataEdges().size();i++) {
			buf.append(this.getID() + " -> " + this.getDataEdges().get(i) + " [color=red];\n");
		}
		return buf.toString();
	}


	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof ControlNode)) return false;
		ControlNode controlNode = (ControlNode)o;
		return this.opcode == controlNode.opcode;
	}

	@Override
	public Node getCopy() {
		ControlNode controlNode = new ControlNode(getID(), getOffset(), getOpcode());
		controlNode.getInputRegisters().addAllOf(getInputRegisters());
		controlNode.getControlEdges().addAllOf(getControlEdges());
		controlNode.getDataEdges().addAllOf(getDataEdges());
		return controlNode;
	}
}
