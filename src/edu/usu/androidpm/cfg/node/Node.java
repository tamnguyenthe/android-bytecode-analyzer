package edu.usu.androidpm.cfg.node;

import cern.colt.list.IntArrayList;

public abstract class Node {
	private final int id;
	private final int offset;
	private IntArrayList dataEdges;
	private IntArrayList controlEdges;
	private IntArrayList dataBackEdges;
	
	protected Node(int id, int offset) {
		this.id = id;
		this.offset = offset;
		this.dataEdges = new IntArrayList();
		this.controlEdges = new IntArrayList();
		this.dataBackEdges = new IntArrayList();
	}
	
	public int getID() {
		return id;
	}
	
	public int getOffset() {
		return offset;
	}
	
	public IntArrayList getDataEdges() {
		return dataEdges;
	}

	public IntArrayList getControlEdges() {
		return controlEdges;
	}
	
	public IntArrayList getDataBackEdges() {
		return dataBackEdges;
	}
	
	public abstract String toDot();
	
	public abstract Node getCopy();
	
	@Override
	public int hashCode() {
		return this.id;
	}
}
