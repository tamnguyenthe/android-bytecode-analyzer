package edu.usu.androidpm.cfg.node;

import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;


public abstract class DataNode extends Node {
	private String type;
	private TIntSet registers;
	private int actionNodeID;
	private int hostObjectID;
	
	public DataNode(int id, int offset) {
		super(id, offset);
	}
	
	public DataNode(int id, int offset, String type, int register) {
		super(id, offset);
		setType(type);
		setRegisters(new TIntHashSet());
		getRegisters().add(register);
		setActionNodeID(-1);
		setHostObjectID(-1);
	}
	
	public DataNode(int id, int offset, String type, int register, int actionNodeID) {
		super(id, offset);
		setType(type);
		setRegisters(new TIntHashSet());
		getRegisters().add(register);
		setActionNodeID(actionNodeID);
		setHostObjectID(-1);
	}
	
	public DataNode(int id, int offset, String type, int register, int actionNodeID, int hostObjectID) {
		super(id, offset);
		setType(type);
		setRegisters(new TIntHashSet());
		getRegisters().add(register);
		setActionNodeID(actionNodeID);
		setHostObjectID(hostObjectID);
	}
	
	

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public TIntSet getRegisters() {
		return registers;
	}

	protected void setRegisters(TIntSet registers) {
		this.registers = registers;
	}
	
	public int getActionNodeID() {
		return actionNodeID;
	}

	public void setActionNodeID(int actionNodeID) {
		this.actionNodeID = actionNodeID;
	}
	
	public int getHostObjectID() {
		return hostObjectID;
	}

	public void setHostObjectID(int hostObjectID) {
		this.hostObjectID = hostObjectID;
	}
	
	@Override
	public String toDot() {
		StringBuilder buf = new StringBuilder();
		buf.append(getID() + " ");
		buf.append("[shape=ellipse,label= \"" + this.getClass().getSimpleName() + " " + getID() + ":\\n" + type + " | reg: " + getRegisters() + "\"];\n");
		for (int i = 0; i < this.getDataEdges().size();i++) {
			buf.append(this.getID() + " -> " + this.getDataEdges().get(i) + " [color=red];\n");
		}
		return buf.toString();
	}
}
