package edu.usu.androidpm.cfg;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.jf.dexlib2.iface.instruction.Instruction;
import org.jf.dexlib2.iface.instruction.OffsetInstruction;
import org.jf.dexlib2.iface.instruction.SwitchElement;
import org.jf.dexlib2.iface.instruction.formats.PackedSwitchPayload;
import org.jf.dexlib2.iface.instruction.formats.SparseSwitchPayload;

/**
 * This class represents a node in a instruction graph
 * 
 * @author TamNT
 * @version 1.0
 */

public class InstructionNode {
	private final int offset;
	private final Instruction instruction;
	private final InstructionGraph instructionGraph;
	private boolean isControlNode = true;
	private List<Integer> nextNodes;
	
	public InstructionNode(int offset, Instruction instruction, InstructionGraph instructionGraph) {
		this.offset = offset;
		this.instruction = instruction;
		this.instructionGraph = instructionGraph;
	}
	
	public List<Integer> getNextNodes() {
		return this.nextNodes;
	}
	
	public int getOffset() {
		return offset;
	}

	public Instruction getInstruction() {
		return instruction;
	}
	
	public void setNextNodes() {
		int baseOffset;
		Instruction instruction2;
		this.nextNodes = new ArrayList<>();
		switch (instruction.getOpcode()) {
			case RETURN_VOID:
			case RETURN:
			case RETURN_WIDE:
			case RETURN_OBJECT:
			case THROW:
			case PACKED_SWITCH_PAYLOAD:
			case SPARSE_SWITCH_PAYLOAD:
			case ARRAY_PAYLOAD:
				break;
			case GOTO:
			case GOTO_16:
			case GOTO_32:
				this.nextNodes.add(offset + ((OffsetInstruction)instruction).getCodeOffset());
				break;
			case PACKED_SWITCH:
				this.nextNodes.add(offset + instruction.getCodeUnits());
				baseOffset = offset + ((OffsetInstruction)instruction).getCodeOffset();
				instruction2 = instructionGraph.getInstructionNodeAtCodeOffset(baseOffset).instruction;
				for (SwitchElement switchElement: ((PackedSwitchPayload)instruction2).getSwitchElements()) {
					this.nextNodes.add(offset + switchElement.getOffset());
				}
				Collections.reverse(this.nextNodes);
				break;
			case SPARSE_SWITCH:
				this.nextNodes.add(offset + instruction.getCodeUnits());
				baseOffset = offset + ((OffsetInstruction)instruction).getCodeOffset();
				instruction2 = instructionGraph.getInstructionNodeAtCodeOffset(baseOffset).instruction;
				for (SwitchElement switchElement: ((SparseSwitchPayload)instruction2).getSwitchElements()) {
					this.nextNodes.add(offset + switchElement.getOffset());
				}
				Collections.reverse(this.nextNodes);
				break;
			case IF_EQ:
			case IF_NE:
			case IF_LT:
			case IF_GE:
			case IF_GT:
			case IF_LE:
			case IF_EQZ:
			case IF_NEZ:
			case IF_LTZ:
			case IF_GEZ:
			case IF_GTZ:
			case IF_LEZ:
				this.nextNodes.add(offset + ((OffsetInstruction)instruction).getCodeOffset());
				this.nextNodes.add(offset + instruction.getCodeUnits());
				break;
			default:
				isControlNode = false;
				this.nextNodes.add(offset + instruction.getCodeUnits());
				break;
		}
	}
	
	public boolean isControlNode() {
		return isControlNode;
	}
}
