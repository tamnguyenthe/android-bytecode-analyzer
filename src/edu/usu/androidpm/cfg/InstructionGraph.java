package edu.usu.androidpm.cfg;

import java.util.ArrayList;
import java.util.List;

import org.jf.dexlib2.iface.instruction.Instruction;
import org.jf.dexlib2.util.InstructionOffsetMap;

/**
 * This class represents a instruction graph that is built from 
 * the Dalvik bytecode of a method
 * @author TamNT
 * @version 1.0
 */

public class InstructionGraph {
	private final List<InstructionNode> nodes;
	private final InstructionOffsetMap instructionOffsetMap;
	
	public InstructionGraph(List<? extends Instruction> instructions) {
		this.nodes = new ArrayList<>(instructions.size());
		this.instructionOffsetMap = new InstructionOffsetMap(instructions);
		int offset;
		Instruction instruction;
		for (int i = 0; i < instructions.size(); i++) {
			offset = instructionOffsetMap.getInstructionCodeOffset(i);
			instruction = instructions.get(i);
			this.nodes.add(new InstructionNode(offset, instruction, this));
		}
		
		for (InstructionNode instructionNode: this.nodes) {
			instructionNode.setNextNodes();
		}
	}
	
	public List<InstructionNode> getNodes() {
		return nodes;
	}
	
	public InstructionNode getInstructionNodeAtCodeOffset(int offset) {
		return nodes.get(instructionOffsetMap.getInstructionIndexAtCodeOffset(offset));
	}	
}
