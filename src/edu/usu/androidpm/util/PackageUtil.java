package edu.usu.androidpm.util;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.jf.dexlib2.dexbacked.DexBackedClassDef;

import com.opencsv.CSVReader;

public class PackageUtil {
	public static Set<String> blockedClasses;
	private static Set<String> androidPackages;

	static {
		blockedClasses = new HashSet<String>();	
		androidPackages = new HashSet<String>();
		try {
			CSVReader blockedClassesReader = new CSVReader(new FileReader("data/blocked_classes.csv"));
			String nextLine[];
			while ((nextLine = blockedClassesReader.readNext()) != null) {
				blockedClasses.add(nextLine[0]);
			}
			blockedClassesReader.close();
			
			CSVReader packageReader = new CSVReader(new FileReader("data/android_packages.csv"));
			String packageNameInDexFormat;
			while ((nextLine = packageReader.readNext()) != null) {
				packageNameInDexFormat = "L" + nextLine[0].replace('.', '/');
				androidPackages.add(packageNameInDexFormat);
			}
			packageReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static boolean isSystemClass(DexBackedClassDef classDef) {
		for (String packageName: androidPackages) {
			if (classDef.getType().startsWith(packageName)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isSystemObject(String objectName) {
		for (String packageName: androidPackages) {
			if (objectName.startsWith(packageName)) {
				if (blockedClasses.contains(objectName))return false;
				else return true;
			}
		}
		return false;
	}
	
	public static void main(String[] args) {
		System.out.println(androidPackages);
	}
}
