package edu.usu.androidpm.util;

import java.util.EnumMap;

import org.jf.dexlib2.Opcode;


public class PrimitiveType {
	public static final String VOID 	= "V";
	public static final String BOOLEAN	= "Z";
	public static final String BYTE 	= "B";
	public static final String SHORT 	= "S";
	public static final String CHAR 	= "C";
	public static final String INT 		= "I";
	public static final String LONG 	= "J";
	public static final String FLOAT	= "F";
	public static final String DOUBLE	= "D";
	public static final String STRING	= "T";

	private static EnumMap<Opcode, String> opcodeToTypeMap = new EnumMap<>(Opcode.class);
	static {

		//const
		opcodeToTypeMap.put(Opcode.CONST_4,INT);
		opcodeToTypeMap.put(Opcode.CONST_16,INT);
		opcodeToTypeMap.put(Opcode.CONST,INT);
		opcodeToTypeMap.put(Opcode.CONST_HIGH16,INT);
		opcodeToTypeMap.put(Opcode.CONST_WIDE_16,LONG);
		opcodeToTypeMap.put(Opcode.CONST_WIDE_32,LONG);
		opcodeToTypeMap.put(Opcode.CONST_WIDE,LONG);
		opcodeToTypeMap.put(Opcode.CONST_WIDE_HIGH16,LONG);

		opcodeToTypeMap.put(Opcode.CONST_STRING, STRING);
		opcodeToTypeMap.put(Opcode.CONST_STRING_JUMBO, STRING);

		//unop
		opcodeToTypeMap.put(Opcode.NEG_INT, INT);
		opcodeToTypeMap.put(Opcode.NOT_INT, INT);
		opcodeToTypeMap.put(Opcode.NEG_LONG, LONG);
		opcodeToTypeMap.put(Opcode.NOT_LONG, LONG);
		opcodeToTypeMap.put(Opcode.NEG_FLOAT, FLOAT);
		opcodeToTypeMap.put(Opcode.NEG_DOUBLE, DOUBLE);
		opcodeToTypeMap.put(Opcode.INT_TO_LONG, LONG);
		opcodeToTypeMap.put(Opcode.INT_TO_FLOAT, FLOAT);
		opcodeToTypeMap.put(Opcode.INT_TO_DOUBLE, DOUBLE);
		opcodeToTypeMap.put(Opcode.LONG_TO_INT, INT);
		opcodeToTypeMap.put(Opcode.LONG_TO_FLOAT, FLOAT);
		opcodeToTypeMap.put(Opcode.LONG_TO_DOUBLE, DOUBLE);
		opcodeToTypeMap.put(Opcode.FLOAT_TO_INT, INT);
		opcodeToTypeMap.put(Opcode.FLOAT_TO_LONG, LONG);
		opcodeToTypeMap.put(Opcode.FLOAT_TO_DOUBLE, DOUBLE);
		opcodeToTypeMap.put(Opcode.DOUBLE_TO_INT, INT);
		opcodeToTypeMap.put(Opcode.DOUBLE_TO_LONG, LONG);
		opcodeToTypeMap.put(Opcode.DOUBLE_TO_FLOAT, FLOAT);
		opcodeToTypeMap.put(Opcode.INT_TO_BYTE, BYTE);
		opcodeToTypeMap.put(Opcode.INT_TO_CHAR, CHAR);
		opcodeToTypeMap.put(Opcode.INT_TO_SHORT, SHORT);

		//binop
		opcodeToTypeMap.put(Opcode.ADD_INT, INT);
		opcodeToTypeMap.put(Opcode.SUB_INT, INT);
		opcodeToTypeMap.put(Opcode.MUL_INT, INT);
		opcodeToTypeMap.put(Opcode.DIV_INT, INT);
		opcodeToTypeMap.put(Opcode.REM_INT, INT);
		opcodeToTypeMap.put(Opcode.AND_INT, INT);
		opcodeToTypeMap.put(Opcode.OR_INT, INT);
		opcodeToTypeMap.put(Opcode.XOR_INT, INT);
		opcodeToTypeMap.put(Opcode.SHL_INT, INT);
		opcodeToTypeMap.put(Opcode.SHR_INT, INT);
		opcodeToTypeMap.put(Opcode.USHR_INT, INT);
		opcodeToTypeMap.put(Opcode.ADD_LONG, LONG);
		opcodeToTypeMap.put(Opcode.SUB_LONG, LONG);
		opcodeToTypeMap.put(Opcode.MUL_LONG, LONG);
		opcodeToTypeMap.put(Opcode.DIV_LONG, LONG);
		opcodeToTypeMap.put(Opcode.REM_LONG, LONG);
		opcodeToTypeMap.put(Opcode.AND_LONG, LONG);
		opcodeToTypeMap.put(Opcode.OR_LONG, LONG);
		opcodeToTypeMap.put(Opcode.XOR_LONG, LONG);
		opcodeToTypeMap.put(Opcode.SHL_LONG, LONG);
		opcodeToTypeMap.put(Opcode.SHR_LONG, LONG);
		opcodeToTypeMap.put(Opcode.USHR_LONG, LONG);
		opcodeToTypeMap.put(Opcode.ADD_FLOAT, FLOAT);
		opcodeToTypeMap.put(Opcode.SUB_FLOAT, FLOAT);
		opcodeToTypeMap.put(Opcode.MUL_FLOAT, FLOAT);
		opcodeToTypeMap.put(Opcode.DIV_FLOAT, FLOAT);
		opcodeToTypeMap.put(Opcode.REM_FLOAT, FLOAT);
		opcodeToTypeMap.put(Opcode.ADD_DOUBLE, DOUBLE);
		opcodeToTypeMap.put(Opcode.SUB_DOUBLE, DOUBLE);
		opcodeToTypeMap.put(Opcode.MUL_DOUBLE, DOUBLE);
		opcodeToTypeMap.put(Opcode.DIV_DOUBLE, DOUBLE);
		opcodeToTypeMap.put(Opcode.REM_DOUBLE, DOUBLE);

		//binop-2addr
		opcodeToTypeMap.put(Opcode.ADD_INT_2ADDR, INT);
		opcodeToTypeMap.put(Opcode.SUB_INT_2ADDR, INT);
		opcodeToTypeMap.put(Opcode.MUL_INT_2ADDR, INT);
		opcodeToTypeMap.put(Opcode.DIV_INT_2ADDR, INT);
		opcodeToTypeMap.put(Opcode.REM_INT_2ADDR, INT);
		opcodeToTypeMap.put(Opcode.AND_INT_2ADDR, INT);
		opcodeToTypeMap.put(Opcode.OR_INT_2ADDR, INT);
		opcodeToTypeMap.put(Opcode.XOR_INT_2ADDR, INT);
		opcodeToTypeMap.put(Opcode.SHL_INT_2ADDR, INT);
		opcodeToTypeMap.put(Opcode.SHR_INT_2ADDR, INT);
		opcodeToTypeMap.put(Opcode.USHR_INT_2ADDR, INT);
		opcodeToTypeMap.put(Opcode.ADD_LONG_2ADDR, LONG);
		opcodeToTypeMap.put(Opcode.SUB_LONG_2ADDR, LONG);
		opcodeToTypeMap.put(Opcode.MUL_LONG_2ADDR, LONG);
		opcodeToTypeMap.put(Opcode.DIV_LONG_2ADDR, LONG);
		opcodeToTypeMap.put(Opcode.REM_LONG_2ADDR, LONG);
		opcodeToTypeMap.put(Opcode.AND_LONG_2ADDR, LONG);
		opcodeToTypeMap.put(Opcode.OR_LONG_2ADDR, LONG);
		opcodeToTypeMap.put(Opcode.XOR_LONG_2ADDR, LONG);
		opcodeToTypeMap.put(Opcode.SHL_LONG_2ADDR, LONG);
		opcodeToTypeMap.put(Opcode.SHR_LONG_2ADDR, LONG);
		opcodeToTypeMap.put(Opcode.USHR_LONG_2ADDR, LONG);
		opcodeToTypeMap.put(Opcode.ADD_FLOAT_2ADDR, FLOAT);
		opcodeToTypeMap.put(Opcode.SUB_FLOAT_2ADDR, FLOAT);
		opcodeToTypeMap.put(Opcode.MUL_FLOAT_2ADDR, FLOAT);
		opcodeToTypeMap.put(Opcode.DIV_FLOAT_2ADDR, FLOAT);
		opcodeToTypeMap.put(Opcode.REM_FLOAT_2ADDR, FLOAT);
		opcodeToTypeMap.put(Opcode.ADD_DOUBLE_2ADDR, DOUBLE);
		opcodeToTypeMap.put(Opcode.SUB_DOUBLE_2ADDR, DOUBLE);
		opcodeToTypeMap.put(Opcode.MUL_DOUBLE_2ADDR, DOUBLE);
		opcodeToTypeMap.put(Opcode.DIV_DOUBLE_2ADDR, DOUBLE);
		opcodeToTypeMap.put(Opcode.REM_DOUBLE_2ADDR, DOUBLE);

		//binop-lit
		opcodeToTypeMap.put(Opcode.ADD_INT_LIT16, INT);
		opcodeToTypeMap.put(Opcode.RSUB_INT, INT);
		opcodeToTypeMap.put(Opcode.MUL_INT_LIT16, INT);
		opcodeToTypeMap.put(Opcode.DIV_INT_LIT16, INT);
		opcodeToTypeMap.put(Opcode.REM_INT_LIT16, INT);
		opcodeToTypeMap.put(Opcode.AND_INT_LIT16, INT);
		opcodeToTypeMap.put(Opcode.OR_INT_LIT16, INT);
		opcodeToTypeMap.put(Opcode.XOR_INT_LIT16, INT);

		opcodeToTypeMap.put(Opcode.ADD_INT_LIT8, INT);
		opcodeToTypeMap.put(Opcode.RSUB_INT_LIT8, INT);
		opcodeToTypeMap.put(Opcode.MUL_INT_LIT8, INT);
		opcodeToTypeMap.put(Opcode.DIV_INT_LIT8, INT);
		opcodeToTypeMap.put(Opcode.REM_INT_LIT8, INT);
		opcodeToTypeMap.put(Opcode.AND_INT_LIT8, INT);
		opcodeToTypeMap.put(Opcode.OR_INT_LIT8, INT);
		opcodeToTypeMap.put(Opcode.XOR_INT_LIT8, INT);
		opcodeToTypeMap.put(Opcode.SHL_INT_LIT8, INT);
		opcodeToTypeMap.put(Opcode.SHR_INT_LIT8, INT);
		opcodeToTypeMap.put(Opcode.USHR_INT_LIT8, INT);
	}

	public static String getOutputTypeForOpcode(Opcode opcode) {
		return opcodeToTypeMap.get(opcode);
	}

	public static void main(String[] args) {
		System.out.println(opcodeToTypeMap.get(Opcode.REM_DOUBLE));
		System.out.println(opcodeToTypeMap.get(Opcode.DOUBLE_TO_LONG));
	}
}
