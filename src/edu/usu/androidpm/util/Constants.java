package edu.usu.androidpm.util;

public class Constants {
	public static final int API = 20;
	public static final int MIN_METHOD_NAME_LENGHT = 3;
	public static final String INIT = "<init>";
	public static final String LOAD = "<LOAD>";
	public static final int LOAD_ID = -1;
	public static final char COMMA_CHAR = ',';
	public static final char QUOTE_CHAR = '\"';
	
	public static final String OBJECTS_FILE = "C:\\Users\\Tam Nguyen\\Documents\\FINALDB\\Objects.csv";
	public static final String METHOD_NAMES_FILE = "C:\\Users\\Tam Nguyen\\Documents\\FINALDB\\MethodNames.csv";
	public static final String MULTIPLE_OBJECTS_FILE = "C:\\Users\\Tam Nguyen\\Documents\\FINALDB\\MultipleObjects.csv";
	public static final String SINGLE_OBJECT_USAGE_DIR = "C:\\Users\\Tam Nguyen\\Documents\\FINALDB\\SingleObjectUsages";
	public static final String MULTIPLE_OBJECTS_USAGE_DIR = "C:\\Users\\Tam Nguyen\\Documents\\FINALDB\\MultipleObjectsUsages";
	
	public static final double EPSILON = 0.00000000001;

}
