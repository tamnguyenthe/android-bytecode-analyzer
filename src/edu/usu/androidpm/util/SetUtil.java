package edu.usu.androidpm.util;

import java.util.Set;

import gnu.trove.set.TIntSet;

public class SetUtil {
	public static boolean hasIntersection(TIntSet s1, TIntSet s2) {
		for (int i: s1.toArray()) if (s2.contains(i)) return true;
		return false;
	}
	
	public static boolean hasIntersection(Set<Integer> s1, Set<Integer> s2) {
		for (Integer i: s1) if (s2.contains(i)) return true;
		return false;
	}
}
