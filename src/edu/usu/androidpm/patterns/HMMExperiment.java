package edu.usu.androidpm.patterns;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import be.ac.ulg.montefiore.run.jahmm.Hmm;
import be.ac.ulg.montefiore.run.jahmm.ObservationInteger;
import be.ac.ulg.montefiore.run.jahmm.Opdf;
import be.ac.ulg.montefiore.run.jahmm.OpdfIntegerFactory;
import be.ac.ulg.montefiore.run.jahmm.draw.GenericHmmDrawerDot;
import be.ac.ulg.montefiore.run.jahmm.io.ObservationIntegerReader;
import be.ac.ulg.montefiore.run.jahmm.io.ObservationSequencesReader;
import be.ac.ulg.montefiore.run.jahmm.learn.KMeansLearner;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

public class HMMExperiment {
	private final String className;
	private final BiMap<String, Integer> methodNameToID;
	private List<List<Integer>> sequences;
	private Hmm<ObservationInteger> fittedHmm;
	
	public HMMExperiment(String className) {
		this.className = className;
		methodNameToID = HashBiMap.create();
		sequences = new ArrayList<>();
	}

	public String getClassName() {
		return className;
	}

	public BiMap<String, Integer> getMethodNameToID() {
		return methodNameToID;
	}

	public List<List<Integer>> getSequences() {
		return sequences;
	}
	
	public void addSequence(List<String> sequence) {
		List<Integer> idSequence = new ArrayList<>(sequence.size());
		for (String str: sequence) {
			if (!methodNameToID.containsKey(str)) methodNameToID.put(str, methodNameToID.size());
			idSequence.add(methodNameToID.get(str));
		}
		sequences.add(idSequence);
	}
	
	public void writePaths() throws FileNotFoundException {
		PrintWriter printWriter = new PrintWriter(new File("BufferedReader.seq"));
		for (List<Integer> sequence: sequences) {
			if (sequence.size() > 1) {
				for (Integer id: sequence) {
					printWriter.print(id + " ; ");
				}
				printWriter.println();
			}
		}
		printWriter.close();
	}
	
	public static void main(String[] args) throws Exception {
		Reader reader = new FileReader("BufferedReader.seq");
		List<List<ObservationInteger>> seqs = ObservationSequencesReader.readSequences(new ObservationIntegerReader(), reader);
		KMeansLearner<ObservationInteger> kml = new KMeansLearner<ObservationInteger>(5, new OpdfIntegerFactory(12), seqs);
		Hmm<ObservationInteger> fittedHmm = kml.learn();
		(new GenericHmmDrawerDot()).write(fittedHmm, "hmm.dot");
		
	}

	public Hmm<ObservationInteger> getFittedHmm() {
		return fittedHmm;
	}

	public void setFittedHmm(Hmm<ObservationInteger> fittedHmm) {
		this.fittedHmm = fittedHmm;
	}
	
	public void writeResult() throws Exception {
		PrintWriter printWriter = new PrintWriter(new File("hmm.txt"));
		for (int i = 0; i < fittedHmm.nbStates(); i++) {
			printWriter.println("state " + i + ": Pi = " + fittedHmm.getPi(i));
			List<Integer> top2 = findTheLargestAndSecondLargestIndex(fittedHmm.getOpdf(i));
			for (int j: top2) {
				if (fittedHmm.getOpdf(i).probability(new ObservationInteger(j)) > 0) {
					printWriter.println(methodNameToID.inverse().get(j) + " - " + fittedHmm.getOpdf(i).probability(new ObservationInteger(j)));
				}
			}
			top2 = findTheLargestAndSecondLargestIndex(fittedHmm, i);
			printWriter.print("next states: ");
			for (int j: top2) {
				printWriter.print(" { state " + j + " : " + fittedHmm.getAij(i, j) +" } ");
			}
			printWriter.println();
			printWriter.println();
		}
		printWriter.close();
	}
	
	private List<Integer> findTheLargestAndSecondLargestIndex(Hmm<ObservationInteger> hmm, int stateNb) {
		List<Integer> result = new ArrayList<>(2);
		result.add(0);
		result.add(0);
		double max1 = -1;
		double max2 = -1;
		for (int i = 0; i < hmm.nbStates();i++) {
			double probability = hmm.getAij(stateNb, i);
			if (max1 < probability) {
				max2 = max1;
				max1 = probability;
				result.set(1, result.get(0));
				result.set(0, i);
			} else if (max2 < probability) {
				max2 = probability;
				result.set(1, i);
			}
		}
		return result;
	}
	
	private List<Integer> findTheLargestAndSecondLargestIndex(Opdf<ObservationInteger> pdf) {
		List<Integer> result = new ArrayList<>(2);
		result.add(0);
		result.add(0);
		double max1 = -1;
		double max2 = -1;
		for (int i = 0; i < methodNameToID.size();i++) {
			double probability = pdf.probability(new ObservationInteger(i));
			if (max1 < probability) {
				max2 = max1;
				max1 = probability;
				result.set(1, result.get(0));
				result.set(0, i);
			} else if (max2 < probability) {
				max2 = probability;
				result.set(1, i);
			}
		}
		return result;
	}

}
