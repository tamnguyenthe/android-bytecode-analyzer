package edu.usu.androidpm.patterns;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import be.ac.ulg.montefiore.run.jahmm.Hmm;
import be.ac.ulg.montefiore.run.jahmm.ObservationInteger;
import be.ac.ulg.montefiore.run.jahmm.Opdf;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

public class HMMPairExperiment {
	private final String className1;
	private final String className2;
	private final BiMap<String, Integer> methodNameToID;
	private List<List<Integer>> sequences;
	private Hmm<ObservationInteger> fittedHmm;
	
	public HMMPairExperiment(String className1, String className2) {
		this.className1 = className1;
		this.className2 = className2;
		methodNameToID = HashBiMap.create();
		sequences = new ArrayList<>();
	}

	public Hmm<ObservationInteger> getFittedHmm() {
		return fittedHmm;
	}

	public void setFittedHmm(Hmm<ObservationInteger> fittedHmm) {
		this.fittedHmm = fittedHmm;
	}

	public List<List<Integer>> getSequences() {
		return sequences;
	}

	public void setSequences(List<List<Integer>> sequences) {
		this.sequences = sequences;
	}

	public BiMap<String, Integer> getMethodNameToID() {
		return methodNameToID;
	}

	public String getClassName2() {
		return className2;
	}

	public String getClassName1() {
		return className1;
	}
	
	public void addSequence(List<String> sequence) {
		List<Integer> idSequence = new ArrayList<>(sequence.size());
		for (String str: sequence) {
			if (!methodNameToID.containsKey(str)) methodNameToID.put(str, methodNameToID.size());
			idSequence.add(methodNameToID.get(str));
		}
		sequences.add(idSequence);
	}
	
	public void writePaths() throws FileNotFoundException {
		PrintWriter printWriter = new PrintWriter(new File("BufferedReader.seq"));
		for (List<Integer> sequence: sequences) {
			if (sequence.size() > 1) {
				for (Integer id: sequence) {
					printWriter.print(id + " ; ");
				}
				printWriter.println();
			}
		}
		printWriter.close();
	}
	
	public void writeResult() throws Exception {
		PrintWriter printWriter = new PrintWriter(new File("hmm.txt"));
		for (int i = 0; i < fittedHmm.nbStates(); i++) {
			printWriter.println("state " + i + ": Pi = " + fittedHmm.getPi(i));
			List<Integer> top2 = findTheLargestAndSecondLargestIndex(fittedHmm.getOpdf(i));
			for (int j: top2) {
				printWriter.println(methodNameToID.inverse().get(j) + " - " + fittedHmm.getOpdf(i).probability(new ObservationInteger(j)));
			}
			top2 = findTheLargestAndSecondLargestIndex(fittedHmm, i);
			printWriter.print("next states: ");
			for (int j: top2) {
				printWriter.print(" { state " + j + " : " + fittedHmm.getAij(i, j) +" } ");
			}
			printWriter.println();
			printWriter.println();
		}
		printWriter.close();
	}
	
	private List<Integer> findTheLargestAndSecondLargestIndex(Hmm<ObservationInteger> hmm, int stateNb) {
		List<Integer> result = new ArrayList<>(2);
		result.add(0);
		result.add(0);
		double max1 = -1;
		double max2 = -1;
		for (int i = 0; i < hmm.nbStates();i++) {
			double probability = hmm.getAij(stateNb, i);
			if (max1 < probability) {
				max2 = max1;
				max1 = probability;
				result.set(1, result.get(0));
				result.set(0, i);
			} else if (max2 < probability) {
				max2 = probability;
				result.set(1, i);
			}
		}
		return result;
	}
	
	private List<Integer> findTheLargestAndSecondLargestIndex(Opdf<ObservationInteger> pdf) {
		List<Integer> result = new ArrayList<>(2);
		result.add(0);
		result.add(0);
		double max1 = -1;
		double max2 = -1;
		for (int i = 0; i < methodNameToID.size();i++) {
			double probability = pdf.probability(new ObservationInteger(i));
			if (max1 < probability) {
				max2 = max1;
				max1 = probability;
				result.set(1, result.get(0));
				result.set(0, i);
			} else if (max2 < probability) {
				max2 = probability;
				result.set(1, i);
			}
		}
		return result;
	}


}
