package edu.usu.androidpm.patterns;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;


import edu.usu.androidpm.util.MapUtil;

public class PairMiner {
	private Map<String, Integer> singleObjectCount;
	private Map<Pair<String, String>, Integer> pairObjectCount;

	private Map<String, List<List<String>>> singleUsages;
	private Map<Pair<String, String>, List<List<String>>> pairUsages;
	private Set<String> objectsSet;

	public Set<String> getObjectsSet() {
		return objectsSet;
	}


	private static final int DELTA = 10;

	public PairMiner() {
		singleObjectCount = new HashMap<>();
		pairObjectCount = new HashMap<>();
		singleUsages = new HashMap<>();
		pairUsages = new HashMap<>();
		objectsSet =  new HashSet<>();
	}

	public Map<String, Integer> getSingleObjectCount() {
		return singleObjectCount;
	}

	public Map<Pair<String, String>, Integer> getPairObjectCount() {
		return pairObjectCount;
	}

	public Map<String, List<List<String>>> getSingleUsages() {
		return singleUsages;
	}

	public Map<Pair<String, String>, List<List<String>>> getPairUsages() {
		return pairUsages;
	}

	public void outputResult() throws IOException {
		double score;
		CSVWriter csvWriter = new CSVWriter(new FileWriter("SingleObjects.csv"));
		String[] nextLine = new String[1];
		int count = 0;
		for (Entry<String, Integer> entry: MapUtil.entriesSortedByValues(singleObjectCount)) {
			nextLine[0] = entry.getKey();
			csvWriter.writeNext(nextLine);
			if (++count == 300) break;
		}
		csvWriter.close();

		Map<Pair<String, String>, Double> pairMap = new HashMap<>();
		for (Entry<Pair<String, String>, Integer> entry: pairObjectCount.entrySet()) {
			score = (double)(entry.getValue() - DELTA) / (singleObjectCount.get(entry.getKey().getLeft()) * singleObjectCount.get(entry.getKey().getRight()));
			pairMap.put(entry.getKey(), score);
		}
		
		CSVWriter csvPairWriter = new CSVWriter(new FileWriter("PairObjects.csv"));
		nextLine = new String[2];
		count = 0;
		for (Entry<Pair<String, String>, Double> entry: MapUtil.entriesSortedByValues(pairMap)) {
			nextLine[0] = entry.getKey().getLeft();
			nextLine[1] = entry.getKey().getRight();
			csvPairWriter.writeNext(nextLine);
			if (++count == 300) break;
		}
		csvPairWriter.close();
	}
	
	
	public void read() throws IOException {
		CSVReader objectCsvReader = new CSVReader(new FileReader("SingleObjects.csv"));
		String[] nextLine;
		while((nextLine = objectCsvReader.readNext()) != null) {
			singleUsages.put(nextLine[0], new ArrayList<List<String>>());
			objectsSet.add(nextLine[0]);
		}
		objectCsvReader.close();
		
		CSVReader pairCsvReader = new CSVReader(new FileReader("PairObjects.csv"));
		while ((nextLine = pairCsvReader.readNext()) != null) {
			Pair<String, String> pair = Pair.of(nextLine[0], nextLine[1]);
			pairUsages.put(pair, new ArrayList<List<String>>());
			objectsSet.add(nextLine[0]);
			objectsSet.add(nextLine[1]);
		}
		pairCsvReader.close();
	}
}
