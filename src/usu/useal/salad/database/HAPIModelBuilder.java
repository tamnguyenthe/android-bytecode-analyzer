package usu.useal.salad.database;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import be.ac.ulg.montefiore.run.jahmm.Hmm;
import be.ac.ulg.montefiore.run.jahmm.ObservationInteger;
import be.ac.ulg.montefiore.run.jahmm.Opdf;
import be.ac.ulg.montefiore.run.jahmm.OpdfInteger;
import be.ac.ulg.montefiore.run.jahmm.OpdfIntegerFactory;
import be.ac.ulg.montefiore.run.jahmm.draw.GenericHmmDrawerDot;
import be.ac.ulg.montefiore.run.jahmm.learn.BaumWelchLearner;
import be.ac.ulg.montefiore.run.jahmm.learn.KMeansLearner;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableList;

import edu.usu.androidpm.symbolic.instruction.ReturnInstruction;

public class HAPIModelBuilder {
	private static final double ALPHA = 0.01;
	private static final double VALIDATION_PORTION = 0.125;

	//id of object or object set
	private int objectID;
	//sequence datasets
	private List<MutablePair<ImmutableList<Integer>, Integer>> dataset;
	private List<List<ObservationInteger>> trainingSet;
	private List<List<ObservationInteger>> validationSet;
	//method id mapping
	private BiMap<Integer, Integer> methodIDBiMap;
	private Hmm<ObservationInteger> hapi;
	private Map<Integer, Double> popularThredholdsByLength;
	private DatabaseBuilder databaseBuilder;
	public HAPIModelBuilder(int objectID, List<MutablePair<ImmutableList<Integer>, Integer>> dataset, DatabaseBuilder dbBuilder) throws Exception {
		int sum = 0;
		for (MutablePair<ImmutableList<Integer>, Integer> pair: dataset) {
			sum += pair.getRight();
		}
		if (sum < 24) throw new Exception();
		if (sum > 50000) throw new Exception();
		
		this.objectID = objectID;
		this.dataset = dataset;
		this.databaseBuilder =  dbBuilder;
		popularThredholdsByLength = new HashMap<>();
		methodIDBiMap = HashBiMap.create();
		splitTrainingAndValidationSet();
	}

	private void splitTrainingAndValidationSet() {
		trainingSet = new ArrayList<>();
		validationSet = new ArrayList<>();
		boolean needToPreAddedToTrainingSet;
		List<ObservationInteger> selectedSequence;
		for (MutablePair<ImmutableList<Integer>, Integer> pair: dataset) {
			//update the id bimap
			needToPreAddedToTrainingSet = false;
			for (Integer methodID: pair.getLeft()) {
				if (!methodIDBiMap.containsKey(methodID)) {
					methodIDBiMap.put(methodID, methodIDBiMap.size());
					needToPreAddedToTrainingSet = true;
				}
			}
			//this guarantees that training set always contains all methods
			if (needToPreAddedToTrainingSet) {
				selectedSequence = new ArrayList<>();
				for (Integer methodID: pair.getLeft()) selectedSequence.add(new ObservationInteger(methodIDBiMap.get(methodID)));
				trainingSet.add(selectedSequence);
				pair.setRight(pair.getRight()-1);
			}
		}

		//create a temp dataset to split traing/test set
		List<List<ObservationInteger>> tempSet = new ArrayList<>();
		for (MutablePair<ImmutableList<Integer>, Integer> pair: dataset) {
			if(pair.getRight() > 0) {
				for (int i = 0; i < pair.getRight(); i++) {
					selectedSequence = new ArrayList<>();
					for (Integer methodID: pair.getLeft()) selectedSequence.add(new ObservationInteger(methodIDBiMap.get(methodID)));
					tempSet.add(selectedSequence);
				}
			}
		}
		Collections.shuffle(tempSet);

		//split training/validation set
		int validationSize = (int) Math.round(tempSet.size()*VALIDATION_PORTION);
		validationSet.addAll(tempSet.subList(0, validationSize));
		trainingSet.addAll(tempSet.subList(validationSize, tempSet.size()));	
	}

	void trainHMM() throws Exception {
		List<Double> lnLikelihoods = new ArrayList<>();
		int minNbStates = 2;
		int maxNbStates = 20;
		double lnLikelihood;
		for (int i = minNbStates; i <= maxNbStates; i++) {
			lnLikelihood = 0;
			KMeansLearner<ObservationInteger> kml = new KMeansLearner<ObservationInteger>(i, new OpdfIntegerFactory(methodIDBiMap.size()), trainingSet);
			Hmm<ObservationInteger> hmm = kml.iterate();
			BaumWelchLearner bwl = new BaumWelchLearner();
			hmm = bwl.learn(hmm, trainingSet);		
			smoothHmm(hmm);
			for (List<ObservationInteger> seq: validationSet) {
				lnLikelihood += hmm.lnProbability(seq);
				if (hmm.lnProbability(seq) == Double.NEGATIVE_INFINITY) {
					(new GenericHmmDrawerDot()).write(hmm, "error_hmm.dot");
					System.out.println(seq);
				}
			}
			lnLikelihoods.add(lnLikelihood);
		}
		int bestNbStates = minNbStates + maxIndex(lnLikelihoods);
		trainingSet.addAll(validationSet);
		KMeansLearner<ObservationInteger> kml = new KMeansLearner<ObservationInteger>(bestNbStates, new OpdfIntegerFactory(methodIDBiMap.size()), trainingSet);
		Hmm<ObservationInteger> hmm = kml.iterate();
		BaumWelchLearner bwl = new BaumWelchLearner();
		hmm = bwl.learn(hmm, trainingSet);
		//smoothHmm(hmm);
		hapi = hmm;
		PrintWriter printWriter = new PrintWriter("sequence_prob.txt");
		List<ObservationInteger> sequence;
		List<Pair<ImmutableList<Integer>, Double>> probSequences = new ArrayList<>();
		
		Map<Integer, List<Double>> probByLength = new HashMap<>();
		for (MutablePair<ImmutableList<Integer>, Integer> pair: dataset) {
			sequence = new ArrayList<>();
			for (Integer methodID: pair.getLeft())
				sequence.add(new ObservationInteger(methodIDBiMap.get(methodID)));
			printWriter.println(sequence.size() + ", " + hmm.probability(sequence));
			//probSequences.add(Pair.of(pair.getLeft(), hmm.probability(sequence)));
			List<Double> probOfLength = probByLength.get(pair.getLeft().size());
			if (probOfLength == null) {
				probOfLength = new ArrayList<>();
				probByLength.put(pair.getLeft().size(), probOfLength);
			}
			probOfLength.add(hmm.probability(sequence));
		}
		for (Map.Entry<Integer, List<Double>> entry: probByLength.entrySet()) {
			Collections.sort(entry.getValue());
			int splitIndex = entry.getValue().size() * 75 / 100;
			popularThredholdsByLength.put(entry.getKey(), entry.getValue().get(splitIndex));
		}
//		Collections.sort(probSequences, new Comparator<Pair<ImmutableList<Integer>, Double>>() {
//			@Override
//			public int compare(Pair<ImmutableList<Integer>, Double> o1,
//					Pair<ImmutableList<Integer>, Double> o2) {
//				return Double.compare(o1.getRight(), o2.getRight());
//			}
//		});
//		int splitIndex = probSequences.size()* 75 / 100 ;
//		popularThreshold  = probSequences.get(splitIndex).getRight();
//		int j = probSequences.size()-1;
//		while (j > probSequences.size() * 0.75) {
//			printWriter.print("best sequences");
//			printWriter.print("[");
//			for (Integer methodID: probSequences.get(j).getLeft()) {
//				printWriter.print(databaseBuilder.getMethodNamesBiMap().get(methodID) + " ");
//			}
//			printWriter.print("]\n");
//			j--;
//		}
		//		printWriter.print("worst sequences");
		//		for (int i = 0; i < 10; i++) {
		//			printWriter.print("[");
		//			for (Integer methodID: probSequences.get(i).getLeft()) {
		//				printWriter.print(databaseBuilder.getMethodNamesBiMap().get(methodID) + " ");
		//			}
		//			printWriter.print("]\n");
		//		}
		//		printWriter.print("best sequences");
		//		for (int j = 10; j > 0; j--) {
		//			printWriter.print("[");
		//			for (Integer methodID: probSequences.get(probSequences.size()-j).getLeft()) {
		//				printWriter.print(databaseBuilder.getMethodNamesBiMap().get(methodID) + " ");
		//			}
		//			printWriter.print("]\n");
		//		}
//		printWriter.close();
//		hapi = hmm;
	}

	//smooth a hmm
	public void smoothHmm(Hmm<ObservationInteger> hmm) {
		double prob;
		int nbStates = hmm.nbStates();
		int nbObservations = methodIDBiMap.size();
		for (int i = 0; i < nbStates; i++) {
			prob = hmm.getPi(i);
			hmm.setPi(i, (prob + ALPHA)/(1. + nbStates*ALPHA));
		}

		for (int i = 0; i < nbStates; i++) {
			for (int j = 0; j < nbStates; j++) {
				prob = hmm.getAij(i, j);
				hmm.setAij(i, j, (prob + ALPHA)/(1. + nbStates*ALPHA));
			}
		}

		Opdf<ObservationInteger> opdf;
		double[] probs;
		for (int i = 0; i < nbStates; i++) {
			opdf = hmm.getOpdf(i);
			probs = new double[nbObservations];
			for (int j = 0; j < nbObservations; j++) {
				prob = opdf.probability(new ObservationInteger(j));
				probs[j] = (prob + ALPHA)/(1. + nbObservations*ALPHA);
			}
			hmm.setOpdf(i, new OpdfInteger(probs));
		}
	}

	//find max index of a sequence
	private int maxIndex(List<Double> seqs) {
		double max = Double.NEGATIVE_INFINITY;
		int maxIndex = -1;
		for (int i = 0; i < seqs.size(); i++) {
			if (max < seqs.get(i)) {
				max = seqs.get(i);
				maxIndex = i;
			}
		}
		return maxIndex;
	}

	public void writeHAPIModel() throws Exception {
		PrintWriter printWriter = new PrintWriter("HAPI/Single/" + objectID + ".hapi");
		int nMethods = methodIDBiMap.size();
		int nStates = hapi.nbStates();
		//write number of observations and number of hidden states
		printWriter.println(nMethods + " " + nStates);
		//write method id map
		for (Map.Entry<Integer, Integer> entry: methodIDBiMap.entrySet()) printWriter.println(entry.getKey() + " : " + entry.getValue());
		//write 
		printWriter.println(popularThredholdsByLength.size());
		//write 
		for (Map.Entry<Integer, Double> entry: popularThredholdsByLength.entrySet()) {
			printWriter.println(entry.getKey() + " : " + entry.getValue());
		}
		//write pi
		printWriter.print(hapi.getPi(0));
		for (int i = 1; i < nStates; i++) printWriter.print(" " + hapi.getPi(i));
		printWriter.println();
		//write B
		for (int i = 0; i < nStates; i++) {
			Opdf<ObservationInteger> callingDistribution =  hapi.getOpdf(i);
			printWriter.print(callingDistribution.probability(new ObservationInteger(0)));
			for (int j = 1; j < nMethods; j++) printWriter.print(" " + callingDistribution.probability(new ObservationInteger(j)));
			printWriter.println();
		}
		//write A
		for (int i = 0; i < nStates; i++) {
			printWriter.print(hapi.getAij(i, 0));
			for (int j = 1; j < nStates; j++) printWriter.print(" " + hapi.getAij(i, j));
			printWriter.println();
		}
		printWriter.close();
	}
}
