package usu.useal.salad.database;

import java.util.Map;

public class DatabaseMain {
	//the main function to build HAPI database
	public static void main(String[] args) {
		DatabaseBuilder databaseBuilder = new DatabaseBuilder();
		for (Map.Entry<Integer, String> entry: databaseBuilder.getObjectsBiMap().entrySet()) {
			try {
				databaseBuilder.buildModelForAnObject(entry.getKey());
			} catch (Exception e){
				System.err.println("problem with " + entry.getValue());
				continue;
			}
			System.out.println(entry.getKey());
		}
	}
}
