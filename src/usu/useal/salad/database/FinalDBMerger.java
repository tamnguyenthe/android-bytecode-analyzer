package usu.useal.salad.database;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableList;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import edu.usu.androidpm.util.Constants;

public class FinalDBMerger {
	private BiMap<Integer, String> finalObjectsBiMap; 							// object ID map
	private BiMap<Integer, String> finalMethodNamesBiMap; 						// method ID map
	private BiMap<Integer, ImmutableList<Integer>> finalMultipleObjectsBiMap;	// object set ID map
	private File directory;														// database directory
	private File destDirectory;													// final database directory
	private List<File> dbFolders;

	// OK
	public FinalDBMerger() {
		// initialize original folder
		directory = new File("/home/useallab1/Documents/NEWDB");
		if (!directory.exists()) System.out.println("cannot locate the DBs folder");
		dbFolders = new ArrayList<>();
		for (File projectFolder: directory.listFiles()) {
			if (projectFolder.isDirectory())
				dbFolders.add(projectFolder);
		}
		System.out.println("DBs Folder: " + directory);
		System.out.println("DB Sub-Folders: " + dbFolders);

		// initialize destination folder
		destDirectory = new File("/home/useallab1/Documents/FINALDB");
		if (!destDirectory.exists()) destDirectory.mkdirs();
		try {
			FileUtils.cleanDirectory(destDirectory);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Dest Folder: " + destDirectory);

		// initialize maps
		finalObjectsBiMap = HashBiMap.create();
		finalMethodNamesBiMap = HashBiMap.create();
		finalMultipleObjectsBiMap = HashBiMap.create();	
	}

	public void joinMaps() throws Exception {
		// merge object id, method id, object set id maps
		String[] entries = new String[2];
		List<Triple<BiMap<Integer, String>, BiMap<Integer, String>, BiMap<Integer, ImmutableList<Integer>>>> dbParts = new ArrayList<>();
		for (File projectFolder: dbFolders) {
			Triple<BiMap<Integer, String>, BiMap<Integer, String>, BiMap<Integer, ImmutableList<Integer>>> triple = loadMaps(projectFolder);
			BiMap<Integer, String> objectsBiMap = triple.getLeft();
			BiMap<Integer, String> methodNamesBiMap = triple.getMiddle();
			BiMap<Integer, ImmutableList<Integer>> multipleObjectsBiMap = triple.getRight();
			dbParts.add(triple);

			for (Map.Entry<Integer, String> entry: objectsBiMap.entrySet()) {
				if (!finalObjectsBiMap.inverse().containsKey(entry.getValue())) 
					finalObjectsBiMap.put(finalObjectsBiMap.size()+1, entry.getValue());
			}
			for (Map.Entry<Integer, String> entry: methodNamesBiMap.entrySet()) {
				if (!finalMethodNamesBiMap.inverse().containsKey(entry.getValue()))
					finalMethodNamesBiMap.put(finalMethodNamesBiMap.size()+1, entry.getValue());
			}
			for (Map.Entry<Integer, ImmutableList<Integer>> entry: multipleObjectsBiMap.entrySet()) {
				List<Integer> newIDList = new ArrayList<>();
				for (Integer oldObjectID: entry.getValue()) 
					newIDList.add(finalObjectsBiMap.inverse().get(objectsBiMap.get(oldObjectID)));
				Collections.sort(newIDList);
				ImmutableList<Integer> newIDImmutableList = ImmutableList.copyOf(newIDList);
				if (!finalMultipleObjectsBiMap.inverse().containsKey(newIDImmutableList))
					finalMultipleObjectsBiMap.put(finalMultipleObjectsBiMap.size()+1, newIDImmutableList);
			}
//			System.out.println(objectsBiMap.size());
//			System.out.println(methodNamesBiMap.size());
//			System.out.println(multipleObjectsBiMap.size());
		}

		// save object names map
		CSVWriter objectsCsvWriter = new CSVWriter(new FileWriter(new File(destDirectory, "Objects.csv")), ',', '"');
		for (Map.Entry<Integer, String> entry: finalObjectsBiMap.entrySet()) {
			entries[0] = entry.getValue();
			entries[1] = Integer.toString(entry.getKey());
			objectsCsvWriter.writeNext(entries);
		}
		objectsCsvWriter.close();

		// save method names map
		CSVWriter methodsCsvWriter = new CSVWriter(new FileWriter(new File(destDirectory, "MethodNames.csv")), ',', '"');
		for (Map.Entry<Integer, String> entry: finalMethodNamesBiMap.entrySet()) {
			entries[0] = entry.getValue();
			entries[1] = Integer.toString(entry.getKey()); 
			methodsCsvWriter.writeNext(entries);
		}
		methodsCsvWriter.close();

		// save multiple objects id map
		CSVWriter multipleObjectsCsvWriter = new CSVWriter(new FileWriter(new File(destDirectory, "MultipleObjects.csv")), ',', '"');
		for (Map.Entry<Integer, ImmutableList<Integer>> entry: finalMultipleObjectsBiMap.entrySet()) {
			entries[0] = entry.getValue().toString();
			entries[1] = Integer.toString(entry.getKey());
			multipleObjectsCsvWriter.writeNext(entries);
		}
		multipleObjectsCsvWriter.close();
		
//		System.out.println(finalObjectsBiMap.size());
//		System.out.println(finalMethodNamesBiMap.size());
//		System.out.println(finalMultipleObjectsBiMap.size());

		
		// final single object usages
		File finalObjectUsagesFolder = new File(destDirectory, "SingleObjectUsages");
		if (!finalObjectUsagesFolder.exists()) finalObjectUsagesFolder.mkdirs();
		FileUtils.cleanDirectory(finalObjectUsagesFolder);

		List<Pair<ImmutableList<Integer>, Integer>> sequences;
		Map<ImmutableList<Integer>, Integer> finalSequences;
		for (Map.Entry<Integer, String> entry: finalObjectsBiMap.entrySet()) {
			finalSequences = new HashMap<>();
			for (int i = 0; i < dbParts.size(); i++) {
				BiMap<Integer, String> objectsBiMap = dbParts.get(i).getLeft();
				BiMap<Integer, String> methodNamesBiMap = dbParts.get(i).getMiddle();
				Integer objectID = objectsBiMap.inverse().get(entry.getValue());
				if (objectID != null) {
					sequences = loadDatasetForObject(objectID, dbFolders.get(i).getAbsolutePath() + File.separator + "SingleObjectUsages");
					if (sequences != null) {
						for (Pair<ImmutableList<Integer>, Integer> pair: sequences) {
							List<Integer> newSequence = new ArrayList<>();
							for (Integer methodID: pair.getLeft()) newSequence.add(finalMethodNamesBiMap.inverse().get(methodNamesBiMap.get(methodID)));
							ImmutableList<Integer> newSequenceIList = ImmutableList.copyOf(newSequence);
							Integer currentCount = finalSequences.get(newSequenceIList);
							if (currentCount == null) finalSequences.put(newSequenceIList, pair.getRight());
							else finalSequences.put(newSequenceIList, pair.getRight() + currentCount);
						}
					}
				}
			}
			CSVWriter singleObjectUsageCsvWriter = new CSVWriter(new FileWriter(finalObjectUsagesFolder.getAbsolutePath() + File.separator + entry.getKey() + ".csv"), ',', '"');
			for (Map.Entry<ImmutableList<Integer>, Integer> entry2: finalSequences.entrySet()) {
				entries[0] = entry2.getKey().toString();
				entries[1] = entry2.getValue().toString();
				singleObjectUsageCsvWriter.writeNext(entries);
			}
			singleObjectUsageCsvWriter.close();
		}

		// final object set usages
		File finalObjectSetFolder = new File(destDirectory, "MultipleObjectsUsages");
		if (!finalObjectSetFolder.exists()) finalObjectSetFolder.mkdirs();
		FileUtils.cleanDirectory(finalObjectSetFolder);

		for (Map.Entry<Integer, ImmutableList<Integer>> entry: finalMultipleObjectsBiMap.entrySet()) {
			finalSequences = new HashMap<>();
			for (int i = 0; i < dbParts.size(); i++) {
				BiMap<Integer, String> objectsBiMap = dbParts.get(i).getLeft();
				BiMap<Integer, String> methodNamesBiMap = dbParts.get(i).getMiddle();
				BiMap<Integer, ImmutableList<Integer>> multipleObjectsBiMap = dbParts.get(i).getRight();
				boolean contains = true;
				List<Integer> partIDList = new ArrayList<>();
				for (Integer objectID: entry.getValue()) {
					Integer oldID = objectsBiMap.inverse().get(finalObjectsBiMap.get(objectID));
					if (oldID == null) {
						contains = false;
						break;
					} else partIDList.add(oldID);
				}
				if (!contains) continue;
				Collections.sort(partIDList);
				ImmutableList<Integer> partIDIList = ImmutableList.copyOf(partIDList);
				Integer objectSetID = multipleObjectsBiMap.inverse().get(partIDIList);
				if (objectSetID != null) {
					sequences = loadDatasetForObject(objectSetID, dbFolders.get(i).getAbsolutePath() + File.separator + "MultipleObjectsUsages");
					if (sequences != null) {
						for (Pair<ImmutableList<Integer>, Integer> pair: sequences) {
							List<Integer> newSequence = new ArrayList<>();
							for (Integer methodID: pair.getLeft()) newSequence.add(finalMethodNamesBiMap.inverse().get(methodNamesBiMap.get(methodID)));
							ImmutableList<Integer> newSequenceIList = ImmutableList.copyOf(newSequence);
							Integer currentCount = finalSequences.get(newSequenceIList);
							if (currentCount == null) finalSequences.put(newSequenceIList, pair.getRight());
							else finalSequences.put(newSequenceIList, pair.getRight() + currentCount);
						}
					}
				}
			}
			CSVWriter multipleObjectsUsageCsvWriter = new CSVWriter(new FileWriter(finalObjectSetFolder.getAbsolutePath() + File.separator + entry.getKey() + ".csv"), ',', '"');
			for (Map.Entry<ImmutableList<Integer>, Integer> entry2: finalSequences.entrySet()) {
				entries[0] = entry2.getKey().toString();
				entries[1] = entry2.getValue().toString();
				multipleObjectsUsageCsvWriter.writeNext(entries);
			}
			multipleObjectsUsageCsvWriter.close();
		}
	}

	// OK
	List<Pair<ImmutableList<Integer>, Integer>> loadDatasetForObject(int objectID, String path) throws Exception {
		String filePath = path + File.separator + objectID + ".csv";
		File file  = new File(filePath);
		if (!file.exists()) return null;
		List<Pair<ImmutableList<Integer>, Integer>> dataset = new ArrayList<>();
		CSVReader singleObjectUsageCsvReader = new CSVReader(new FileReader(file), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		String[] entries;
		ImmutableList<Integer> sequence;
		Integer count;

		while ((entries = singleObjectUsageCsvReader.readNext()) != null) {
			sequence = getListFromString(entries[0]);
			count = Integer.valueOf(entries[1]);
			dataset.add(Pair.of(sequence, count));
		}
		singleObjectUsageCsvReader.close();
		return dataset;
	}

	// OK
	private Triple<BiMap<Integer, String>, BiMap<Integer, String>, BiMap<Integer, ImmutableList<Integer>>> loadMaps(File projectFolder) throws Exception {
		String[] entries;
		// load the object id map
		BiMap<Integer, String> objectsBiMap = HashBiMap.create();
		CSVReader objectsCsvReader = new CSVReader(new FileReader(new File(projectFolder, "Objects.csv")), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		while ((entries = objectsCsvReader.readNext()) != null) {
			objectsBiMap.put(Integer.valueOf(entries[1]), entries[0]);
		}
		objectsCsvReader.close();

		// load the method name id map
		BiMap<Integer, String> methodNamesBiMap = HashBiMap.create();
		CSVReader methodNamesCsvReader = new CSVReader(new FileReader(new File(projectFolder, "MethodNames.csv")), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		while ((entries = methodNamesCsvReader.readNext()) != null) {
			methodNamesBiMap.put(Integer.valueOf(entries[1]), entries[0]);
		}
		methodNamesCsvReader.close();

		// load the multiple object id map
		ImmutableList<Integer> multipleObjectsIDs;
		BiMap<Integer, ImmutableList<Integer>> multipleObjectsBiMap = HashBiMap.create();
		CSVReader multipleObjectsCsvReader = new CSVReader(new FileReader(new File(projectFolder, "MultipleObjects.csv")), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		while ((entries = multipleObjectsCsvReader.readNext()) != null) {
			multipleObjectsIDs = getListFromString(entries[0]);
			multipleObjectsBiMap.put(Integer.valueOf(entries[1]), multipleObjectsIDs);
		}
		multipleObjectsCsvReader.close();
		return Triple.of(objectsBiMap, methodNamesBiMap, multipleObjectsBiMap);
	}

	private ImmutableList<Integer> getListFromString(String strArr) {
		String[] parts = strArr.substring(1, strArr.length()-1).split(", ");
		List<Integer> result = new ArrayList<>(parts.length);
		for (int i = 0; i < parts.length; i++) result.add(Integer.valueOf(parts[i]));
		return ImmutableList.copyOf(result);
	}

	public static void main(String[] args) throws Exception {
		FinalDBMerger finalDbMerger = new FinalDBMerger();
		finalDbMerger.joinMaps();
	}


}
