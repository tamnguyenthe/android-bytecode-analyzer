package usu.useal.salad.database;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.Pair;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableList;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import edu.usu.androidpm.util.Constants;

public class SourceDBMerger {

	private File bytecodeDirectory;
	private BiMap<Integer, String> finalObjectsBiMap; 							// object ID map
	private BiMap<Integer, String> finalMethodNamesBiMap; 						// method ID map
	private BiMap<Integer, ImmutableList<Integer>> finalMultipleObjectsBiMap;	// object set ID map


	private File sourceDirectory;
	private BiMap<Integer, String> sourceObjectBimap;
	private BiMap<Integer, String> sourceMethodNamesBimap;
	private BiMap<Integer, ImmutableList<Integer>> sourceObjectSetBimap;

	private File finalSourceDirectory;

	public BiMap<Integer, String> getFinalObjectsBiMap() {
		return finalObjectsBiMap;
	}

	public BiMap<Integer, String> getFinalMethodNamesBiMap() {
		return finalMethodNamesBiMap;
	}

	public BiMap<Integer, ImmutableList<Integer>> getFinalMultipleObjectsBiMap() {
		return finalMultipleObjectsBiMap;
	}

	public BiMap<Integer, String> getSourceObjectBimap() {
		return sourceObjectBimap;
	}

	public BiMap<Integer, ImmutableList<Integer>> getSourceObjectSetBimap() {
		return sourceObjectSetBimap;
	}

	public BiMap<Integer, String> getSourceMethodNamesBimap() {
		return sourceMethodNamesBimap;
	}


	public SourceDBMerger() {
		bytecodeDirectory = new File("/home/useallab1/Documents/FINALDB");
		sourceDirectory = new File("/home/useallab1/Documents/SOURCEDB");

		finalObjectsBiMap = HashBiMap.create();
		finalMethodNamesBiMap = HashBiMap.create();
		finalMultipleObjectsBiMap = HashBiMap.create();	
		sourceObjectBimap = HashBiMap.create();
		sourceMethodNamesBimap = HashBiMap.create();
		sourceObjectSetBimap = HashBiMap.create();
	}

	private void loadFinalMaps() throws Exception {
		String[] entries;
		// load the object id map
		CSVReader objectsCsvReader = new CSVReader(new FileReader(new File(bytecodeDirectory, "Objects.csv")), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		while ((entries = objectsCsvReader.readNext()) != null) {
			finalObjectsBiMap.put(Integer.valueOf(entries[1]), entries[0]);
		}
		objectsCsvReader.close();

		// load the method name id map
		CSVReader methodNamesCsvReader = new CSVReader(new FileReader(new File(bytecodeDirectory, "MethodNames.csv")), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		while ((entries = methodNamesCsvReader.readNext()) != null) {
			finalMethodNamesBiMap.put(Integer.valueOf(entries[1]), entries[0]);
		}
		methodNamesCsvReader.close();

		// load the multiple object id map
		ImmutableList<Integer> multipleObjectsIDs;
		CSVReader multipleObjectsCsvReader = new CSVReader(new FileReader(new File(bytecodeDirectory, "MultipleObjects.csv")), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		while ((entries = multipleObjectsCsvReader.readNext()) != null) {
			multipleObjectsIDs = getListFromString(entries[0]);
			finalMultipleObjectsBiMap.put(Integer.valueOf(entries[1]), multipleObjectsIDs);
		}
		multipleObjectsCsvReader.close();
	}

	private void loadSourceMaps() throws Exception {
		String[] entries;
		// load the object id map
		CSVReader objectsCsvReader = new CSVReader(new FileReader(new File(sourceDirectory, "Objects.csv")), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		while ((entries = objectsCsvReader.readNext()) != null) {
			sourceObjectBimap.put(Integer.valueOf(entries[1]), entries[0]);
		}
		objectsCsvReader.close();

		// load the method name id map
		CSVReader methodNamesCsvReader = new CSVReader(new FileReader(new File(sourceDirectory, "MethodNames.csv")), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		while ((entries = methodNamesCsvReader.readNext()) != null) {
			sourceMethodNamesBimap.put(Integer.valueOf(entries[1]), entries[0]);
		}
		methodNamesCsvReader.close();

		// load the multiple object id map
		ImmutableList<Integer> multipleObjectsIDs;
		CSVReader multipleObjectsCsvReader = new CSVReader(new FileReader(new File(sourceDirectory, "MultipleObjects.csv")), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		while ((entries = multipleObjectsCsvReader.readNext()) != null) {
			multipleObjectsIDs = getListFromString(entries[0]);
			sourceObjectSetBimap.put(Integer.valueOf(entries[1]), multipleObjectsIDs);
		}
		multipleObjectsCsvReader.close();
	}


	BiMap<Integer, String> mObjectBiMap;
	BiMap<Integer, ImmutableList<Integer>> mObjectSetBiMap;

	private void saveNewDB() throws Exception {
		// create final source code database folder
		finalSourceDirectory = new File("/home/useallab1/Documents/FINALSOURCEDB");
		if (!finalSourceDirectory.exists()) finalSourceDirectory.mkdirs();
		FileUtils.cleanDirectory(finalSourceDirectory);
		// create object folder
		File objectFolder = new File(finalSourceDirectory, "SingleObjectUsages");
		if (!objectFolder.exists()) objectFolder.mkdirs();
		FileUtils.cleanDirectory(objectFolder);
		// create object set folder
		File objectSetFolder = new File(finalSourceDirectory, "MultipleObjectsUsages");
		if (!objectSetFolder.exists()) objectSetFolder.mkdirs();
		FileUtils.cleanDirectory(objectSetFolder);

		String[] entries = new String[2];
		// merge object map
		List<Pair<ImmutableList<Integer>, Integer>> sequences;
		Map<ImmutableList<Integer>, Integer> finalSequences;
		List<Integer> newSequence;
		mObjectBiMap = HashBiMap.create();
		for (Map.Entry<Integer, String> entry: sourceObjectBimap.entrySet()) {
			if (finalObjectsBiMap.inverse().containsKey(entry.getValue())) {
				mObjectBiMap.put(finalObjectsBiMap.inverse().get(entry.getValue()), entry.getValue());
				// merge sequences
				finalSequences = new HashMap<>();
				sequences = loadDatasetForObject(entry.getKey(), sourceDirectory.getAbsolutePath() + File.separator + "SingleObjectUsages");
				if (sequences != null) {
					for (Pair<ImmutableList<Integer>, Integer> pair: sequences) {
						boolean valid = true;
						newSequence = new ArrayList<>();
						for (Integer methodID: pair.getLeft()) {
							if (!finalMethodNamesBiMap.inverse().containsKey(sourceMethodNamesBimap.get(methodID))) {
								valid = false;
								break;
							}
							newSequence.add(finalMethodNamesBiMap.inverse().get(sourceMethodNamesBimap.get(methodID)));
						}
						if (!valid) continue;
						if (newSequence.size() >= 2) {
							ImmutableList<Integer> newSequenceIList = ImmutableList.copyOf(newSequence);
							Integer currentCount = finalSequences.get(newSequenceIList);
							if (currentCount == null) finalSequences.put(newSequenceIList, pair.getRight());
							else finalSequences.put(newSequenceIList, pair.getRight() + currentCount);
						}
					}
					CSVWriter singleObjectUsageCsvWriter = new CSVWriter(new FileWriter(objectFolder.getAbsolutePath() + File.separator + finalObjectsBiMap.inverse().get(entry.getValue()) + ".csv"), ',', '"');
					for (Map.Entry<ImmutableList<Integer>, Integer> entry2: finalSequences.entrySet()) {
						entries[0] = entry2.getKey().toString();
						entries[1] = entry2.getValue().toString();
						singleObjectUsageCsvWriter.writeNext(entries);
					}
					singleObjectUsageCsvWriter.close();
				}

			}
		}

		// merge object set map
		mObjectSetBiMap = HashBiMap.create();
		List<Integer> newObjectList;
		ImmutableList<Integer> newObjectIList;
		boolean valid;
		for (Map.Entry<Integer, ImmutableList<Integer>> entry: sourceObjectSetBimap.entrySet()) {
			newObjectList = new ArrayList<>();
			valid = true;
			for (Integer objectID: entry.getValue()) {
				if (mObjectBiMap.inverse().containsKey(sourceObjectBimap.get(objectID))) {
					newObjectList.add(mObjectBiMap.inverse().get(sourceObjectBimap.get(objectID)));
				} else {
					valid = false;
					break;
				}
			}
			if (!valid) continue;
			Collections.sort(newObjectList);
			newObjectIList = ImmutableList.copyOf(newObjectList);
			if (finalMultipleObjectsBiMap.inverse().containsKey(newObjectIList)) {
				mObjectSetBiMap.put(finalMultipleObjectsBiMap.inverse().get(newObjectIList), newObjectIList);
				// merge sequences
				finalSequences = new HashMap<>();
				sequences = loadDatasetForObject(entry.getKey(), sourceDirectory.getAbsolutePath() + File.separator + "MultipleObjectsUsages");
				if (sequences != null) {
					for (Pair<ImmutableList<Integer>, Integer> pair: sequences) {
						valid = true;
						newSequence = new ArrayList<>();
						for (Integer methodID: pair.getLeft()) {
							if (!finalMethodNamesBiMap.inverse().containsKey(sourceMethodNamesBimap.get(methodID))) {
								valid = false;
								break;
							}
							newSequence.add(finalMethodNamesBiMap.inverse().get(sourceMethodNamesBimap.get(methodID)));
						}
						if (!valid) continue;
						if (newSequence.size() >= 2) {
							ImmutableList<Integer> newSequenceIList = ImmutableList.copyOf(newSequence);
							Integer currentCount = finalSequences.get(newSequenceIList);
							if (currentCount == null) finalSequences.put(newSequenceIList, pair.getRight());
							else finalSequences.put(newSequenceIList, pair.getRight() + currentCount);
						}
					}
					CSVWriter multipleObjectsUsageCsvWriter = new CSVWriter(new FileWriter(objectSetFolder.getAbsolutePath() + File.separator + finalMultipleObjectsBiMap.inverse().get(newObjectIList) + ".csv"), ',', '"');
					for (Map.Entry<ImmutableList<Integer>, Integer> entry2: finalSequences.entrySet()) {
						entries[0] = entry2.getKey().toString();
						entries[1] = entry2.getValue().toString();
						multipleObjectsUsageCsvWriter.writeNext(entries);
					}
					multipleObjectsUsageCsvWriter.close();
				}
			}
		}

		// save object names map
		CSVWriter objectsCsvWriter = new CSVWriter(new FileWriter(new File(finalSourceDirectory, "Objects.csv")), ',', '"');
		for (Map.Entry<Integer, String> entry: mObjectBiMap.entrySet()) {
			entries[0] = entry.getValue();
			entries[1] = Integer.toString(entry.getKey());
			objectsCsvWriter.writeNext(entries);
		}
		objectsCsvWriter.close();

		// save multiple objects id map
		CSVWriter multipleObjectsCsvWriter = new CSVWriter(new FileWriter(new File(finalSourceDirectory, "MultipleObjects.csv")), ',', '"');
		for (Map.Entry<Integer, ImmutableList<Integer>> entry: mObjectSetBiMap.entrySet()) {
			entries[0] = entry.getValue().toString();
			entries[1] = Integer.toString(entry.getKey());
			multipleObjectsCsvWriter.writeNext(entries);
		}
		multipleObjectsCsvWriter.close();

	}
	private ImmutableList<Integer> getListFromString(String strArr) {
		String[] parts = strArr.substring(1, strArr.length()-1).split(", ");
		List<Integer> result = new ArrayList<>(parts.length);
		for (int i = 0; i < parts.length; i++) result.add(Integer.valueOf(parts[i]));
		return ImmutableList.copyOf(result);
	}


	// OK
	List<Pair<ImmutableList<Integer>, Integer>> loadDatasetForObject(int objectID, String path) throws Exception {
		String filePath = path + File.separator + objectID + ".csv";
		File file  = new File(filePath);
		if (!file.exists()) return null;
		List<Pair<ImmutableList<Integer>, Integer>> dataset = new ArrayList<>();
		CSVReader singleObjectUsageCsvReader = new CSVReader(new FileReader(file), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		String[] entries;
		ImmutableList<Integer> sequence;
		Integer count;

		while ((entries = singleObjectUsageCsvReader.readNext()) != null) {
			sequence = getListFromString(entries[0]);
			count = Integer.valueOf(entries[1]);
			dataset.add(Pair.of(sequence, count));
		}
		singleObjectUsageCsvReader.close();
		return dataset;
	}


	public static void main(String[] args) throws Exception {
		SourceDBMerger sourceDBMerger = new SourceDBMerger();
		sourceDBMerger.loadFinalMaps();
		sourceDBMerger.loadSourceMaps();
		sourceDBMerger.saveNewDB();
	}
}
