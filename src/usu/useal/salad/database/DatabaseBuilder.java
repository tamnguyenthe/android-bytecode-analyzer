package usu.useal.salad.database;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.MutablePair;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableList;
import com.opencsv.CSVReader;

import edu.usu.androidpm.util.Constants;

public class DatabaseBuilder {

	private BiMap<Integer, String> objectsBiMap;
	private BiMap<Integer, String> methodNamesBiMap;
	private BiMap<Integer, ImmutableList<Integer>> multipleObjectsBiMap;

	public DatabaseBuilder() {
		try {
			loadNameMaps();
		} catch (Exception e) {
			System.err.println("Problem when reading name maps");
			e.printStackTrace();
		}
	}

	private void loadNameMaps() throws Exception {
		String[] entries;

		//load the object id map
		objectsBiMap = HashBiMap.create();
		CSVReader objectsCsvReader = new CSVReader(new FileReader(Constants.OBJECTS_FILE), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		while ((entries = objectsCsvReader.readNext()) != null) {
			objectsBiMap.put(Integer.valueOf(entries[1]), entries[0]);
		}
		objectsCsvReader.close();

		//load the method name id map
		methodNamesBiMap = HashBiMap.create();
		CSVReader methodNamesCsvReader = new CSVReader(new FileReader(Constants.METHOD_NAMES_FILE), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		while ((entries = methodNamesCsvReader.readNext()) != null) {
			methodNamesBiMap.put(Integer.valueOf(entries[1]), entries[0]);
		}
		methodNamesCsvReader.close();

		//load the multiple objecs id map
		ImmutableList<Integer> multipleObjectsIDs;
		multipleObjectsBiMap = HashBiMap.create();
		CSVReader multipleObjectsCsvReader = new CSVReader(new FileReader(Constants.MULTIPLE_OBJECTS_FILE), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		while ((entries = multipleObjectsCsvReader.readNext()) != null) {
			multipleObjectsIDs = getListFromString(entries[0]);
			multipleObjectsBiMap.put(Integer.valueOf(entries[1]), multipleObjectsIDs);
		}
		multipleObjectsCsvReader.close();
	}

	private ImmutableList<Integer> getListFromString(String strArr) {
		String[] parts = strArr.substring(1, strArr.length()-1).split(", ");
		List<Integer> result = new ArrayList<>(parts.length);
		for (int i = 0; i < parts.length; i++) result.add(Integer.valueOf(parts[i]));
		return ImmutableList.copyOf(result);
	}

	public void buildModelForAnObject(int objectID) throws Exception {
		HAPIModelBuilder hapiModelBuilder;
		hapiModelBuilder = new HAPIModelBuilder(objectID, loadDatasetForObject(objectID), this);
		hapiModelBuilder.trainHMM();
		hapiModelBuilder.writeHAPIModel();
	}

	public void buildModelForObjectSet(int objectSetID) throws Exception {
		HAPIModelBuilder hapiModelBuilder;
		hapiModelBuilder = new HAPIModelBuilder(objectSetID, loadDatasetForObjectSet(objectSetID), this);
		hapiModelBuilder.trainHMM();
		hapiModelBuilder.writeHAPIModel();
	}

	public List<MutablePair<ImmutableList<Integer>, Integer>> loadDatasetForObject(int objectID) throws Exception {
		int loadID = methodNamesBiMap.inverse().get(Constants.LOAD);
		List<MutablePair<ImmutableList<Integer>, Integer>> dataset = new ArrayList<>();
		String fileName = Constants.SINGLE_OBJECT_USAGE_DIR + "/" + objectID + ".csv";
		CSVReader singleObjectUsageCsvReader = new CSVReader(new FileReader(fileName), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		String[] entries;
		ImmutableList<Integer> sequence;
		Integer count;
		while ((entries = singleObjectUsageCsvReader.readNext()) != null) {
			sequence = getListFromString(entries[0]);
			//filter loadID
			if (sequence.get(0) == loadID) sequence = sequence.subList(1, sequence.size());
			//only consider sequences have lenght at least 2
			if (sequence.size() >= 2) {
				//? only consider sequences appear at least 2
				count = Integer.valueOf(entries[1]);
				// (count >= 10 && count <= 200) {
				dataset.add(MutablePair.of(sequence, count));
				//}
			}
		}
		singleObjectUsageCsvReader.close();
		return dataset;
	}

	public List<MutablePair<ImmutableList<Integer>, Integer>> loadDatasetForObjectSet(int multipleObjectsID) throws Exception {
		int loadID = methodNamesBiMap.inverse().get(Constants.LOAD);
		List<MutablePair<ImmutableList<Integer>, Integer>> dataset = new ArrayList<>();
		String fileName = Constants.MULTIPLE_OBJECTS_USAGE_DIR + "/" + multipleObjectsID + ".csv";
		CSVReader multipleObjectsUsageCsvReader = new CSVReader(new FileReader(fileName), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		String[] entries;
		ImmutableList<Integer> sequence;
		Integer count;
		while ((entries = multipleObjectsUsageCsvReader.readNext()) != null) {
			sequence = getListFromString(entries[0]);
			//filter loadID
			if (sequence.get(0) == loadID) sequence = sequence.subList(1, sequence.size());
			//only consider sequences have lenght at least 2
			if (sequence.size() > 2) {				
				//? only consider sequences have length at least 2
				count = Integer.valueOf(entries[1]);
				dataset.add(MutablePair.of(sequence, count));
			}
		}
		multipleObjectsUsageCsvReader.close();
		return dataset;
	}

	public BiMap<Integer, String> getObjectsBiMap() {
		return objectsBiMap;
	}

	public BiMap<Integer, String> getMethodNamesBiMap() {
		return methodNamesBiMap;
	}

	public BiMap<Integer, ImmutableList<Integer>> getMultipleObjectsBiMap() {
		return multipleObjectsBiMap;
	}
}
