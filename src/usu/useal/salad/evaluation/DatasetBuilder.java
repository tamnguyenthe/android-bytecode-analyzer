package usu.useal.salad.evaluation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import be.ac.ulg.montefiore.run.jahmm.ObservationInteger;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableList;

public class DatasetBuilder {
	public static final int NFOLDS = 5; // number of folds
	private List<MutablePair<ImmutableList<Integer>, Integer>> rawDataset; // raw data set
	private BiMap<Integer, Integer> methodIDs; // mapping between method id to method index
	private List<List<ObservationInteger>> initialTrainingSet; // initial training set to insure containing all method indices
	private List<List<ObservationInteger>> dataset; // processed dataset
	private List<Pair<List<Integer>, List<Integer>>> crossValidationIndices; // cross validation indices
	private List<Pair<List<Integer>, List<Integer>>> trainValidIndices; // train and validation indices in each fold
	private List<List<Integer>> holeIndices;
	private boolean isReady;
	private boolean isSingle;
	private int id;
	
	public DatasetBuilder(List<MutablePair<ImmutableList<Integer>, Integer>> rawDataset, boolean isSingle, int id) {
		// initialize instance variables
		this.rawDataset = rawDataset;
		methodIDs = HashBiMap.create();
		initialTrainingSet = new ArrayList<>();
		dataset = new ArrayList<>();
		isReady = true;
		this.isSingle = isSingle;
		this.id = id;
		// process raw data set (update initialTrainingSet and dataset)
		buildDataset();
		// build folds for cross validation
		crossValidationIndices = CrossValidation.kFold(dataset.size(), NFOLDS);
		// split training and validation indices
		trainValidIndices = new ArrayList<>();
		for (int i = 0; i < NFOLDS; i++) 
			trainValidIndices.add(CrossValidation.splitTrainingAndValidSet(crossValidationIndices.get(i).getLeft()));
		holeIndices = new ArrayList<>();
		for (int i = 0; i < NFOLDS; i++) 
			holeIndices.add(generateHoles(getTestingSetForFold(i)));
	}
	
	public boolean isSingle() {
		return isSingle;
	}

	public int getId() {
		return id;
	}

	public boolean isReadyForEvaluation() {
		return isReady;
	}
	
	private void buildDataset() {
		// temporary variable
		List<ObservationInteger> selectedSequence;
		// create initial training set
		for (MutablePair<ImmutableList<Integer>, Integer> pair: rawDataset) {
			for (Integer methodID: pair.getLeft()) {
				// if the method id is not captured, add sequence to initial training set, then break
				if (!methodIDs.containsKey(methodID)) {
					methodIDs.put(methodID, methodIDs.size());
					selectedSequence = new ArrayList<>();
					for (Integer methodID2: pair.getLeft()) {
						if (!methodIDs.containsKey(methodID2)) methodIDs.put(methodID2, methodIDs.size());
						selectedSequence.add(new ObservationInteger(methodIDs.get(methodID2)));
					}
					initialTrainingSet.add(selectedSequence);
					pair.setRight(pair.getRight()-1);
					break;
				}
			}
		}
		if (initialTrainingSet.size() == 0) isReady = false;
		// create the data set
		for (MutablePair<ImmutableList<Integer>, Integer> pair: rawDataset) {
			for (int i = 0; i < pair.getRight(); i++) {
				selectedSequence = new ArrayList<>();
				for (Integer methodID2: pair.getLeft()) selectedSequence.add(new ObservationInteger(methodIDs.get(methodID2)));
				dataset.add(selectedSequence);
			}
		}
		System.out.println(dataset.size());
		if (dataset.size() < 10) isReady = false;
		if (dataset.size() > 100000) isReady = false;
	}
	
	public List<List<ObservationInteger>> getTrainingSetForFold(int fold) {
		Pair<List<Integer>, List<Integer>> splits = trainValidIndices.get(fold);
		List<List<ObservationInteger>> trainSet = new ArrayList<>();
		for (Integer index: splits.getLeft()) trainSet.add(dataset.get(index));
		trainSet.addAll(initialTrainingSet);
		return trainSet;
	}
	
	public List<List<ObservationInteger>> getValidationSetForFold(int fold) {
		Pair<List<Integer>, List<Integer>> splits = trainValidIndices.get(fold);
		List<List<ObservationInteger>> validSet = new ArrayList<>();
		for (Integer index: splits.getRight()) validSet.add(dataset.get(index));
		return validSet;
	}
	
	public List<List<ObservationInteger>> getFullTrainingSetForFold(int fold) {
		Pair<List<Integer>, List<Integer>> splits = crossValidationIndices.get(fold);
		List<List<ObservationInteger>> trainingSet = new ArrayList<>();
		for (Integer index: splits.getLeft()) trainingSet.add(dataset.get(index));
		trainingSet.addAll(initialTrainingSet);
		return trainingSet;
	}
	
	public List<List<ObservationInteger>> getTestingSetForFold(int fold) {
		Pair<List<Integer>, List<Integer>> splits = crossValidationIndices.get(fold);
		List<List<ObservationInteger>> testSet = new ArrayList<>();
		for (Integer index: splits.getRight()) testSet.add(dataset.get(index));
		return testSet;
	}
	
	List<Integer> generateHoles(List<List<ObservationInteger>> testSet) {
		List<Integer> result = new ArrayList<>();
		Random rand = new Random();
		for (List<ObservationInteger> sequence: testSet) {
			result.add(rand.nextInt(sequence.size()));
		}
		return result;
	}
	
	public List<List<ObservationInteger>> getInitialTrainingSet() {
		return initialTrainingSet;
	}

	public List<List<ObservationInteger>> getDataset() {
		return dataset;
	}
	
	public BiMap<Integer, Integer> getMethodIDs() {
		return methodIDs;
	}
	
	public List<List<Integer>> getHoleIndices() {
		return holeIndices;
	}
}
