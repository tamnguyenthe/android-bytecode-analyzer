package usu.useal.salad.evaluation;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.MutablePair;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableList;
import com.opencsv.CSVReader;

import edu.usu.androidpm.util.Constants;

public class DatabaseLoader {
	private BiMap<Integer, String> objectsBiMap;
	private BiMap<Integer, String> methodNamesBiMap;
	private BiMap<Integer, ImmutableList<Integer>> multipleObjectsBiMap;
	private int loadID;
	
	public BiMap<Integer, String> getObjectsBiMap() {
		return objectsBiMap;
	}

	public BiMap<Integer, String> getMethodNamesBiMap() {
		return methodNamesBiMap;
	}

	public BiMap<Integer, ImmutableList<Integer>> getMultipleObjectsBiMap() {
		return multipleObjectsBiMap;
	}

	public DatabaseLoader() throws Exception {
		String[] entries;

		//load the object id map
		objectsBiMap = HashBiMap.create();
		CSVReader objectsCsvReader = new CSVReader(new FileReader(Constants.OBJECTS_FILE), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		while ((entries = objectsCsvReader.readNext()) != null) {
			objectsBiMap.put(Integer.valueOf(entries[1]), entries[0]);
		}
		objectsCsvReader.close();

		//load the method name id map
		methodNamesBiMap = HashBiMap.create();
		CSVReader methodNamesCsvReader = new CSVReader(new FileReader(Constants.METHOD_NAMES_FILE), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		while ((entries = methodNamesCsvReader.readNext()) != null) {
			methodNamesBiMap.put(Integer.valueOf(entries[1]), entries[0]);
		}
		methodNamesCsvReader.close();
		
		if (methodNamesBiMap.inverse().containsKey(Constants.LOAD)) 
			loadID = methodNamesBiMap.inverse().get(Constants.LOAD);
		else 
			loadID = -1;

		//load the multiple objects id map
		ImmutableList<Integer> multipleObjectsIDs;
		multipleObjectsBiMap = HashBiMap.create();
		CSVReader multipleObjectsCsvReader = new CSVReader(new FileReader(Constants.MULTIPLE_OBJECTS_FILE), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		while ((entries = multipleObjectsCsvReader.readNext()) != null) {
			multipleObjectsIDs = getListFromString(entries[0]);
			multipleObjectsBiMap.put(Integer.valueOf(entries[1]), multipleObjectsIDs);
		}
		multipleObjectsCsvReader.close();
	}

	private ImmutableList<Integer> getListFromString(String strArr) {
		String[] parts = strArr.substring(1, strArr.length()-1).split(", ");
		List<Integer> result = new ArrayList<>(parts.length);
		for (int i = 0; i < parts.length; i++) result.add(Integer.valueOf(parts[i]));
		return ImmutableList.copyOf(result);
	}

	public List<MutablePair<ImmutableList<Integer>, Integer>> loadDatasetForObject(int objectID) throws Exception {
		List<MutablePair<ImmutableList<Integer>, Integer>> dataset = new ArrayList<>();
		String fileName = Constants.SINGLE_OBJECT_USAGE_DIR + File.separator + objectID + ".csv";
		File file = new File(fileName);
		if (!file.exists()) return dataset;
		CSVReader singleObjectUsageCsvReader = new CSVReader(new FileReader(fileName), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		String[] entries;
		ImmutableList<Integer> sequence;
		Integer count;
		int sum = 0;
		while ((entries = singleObjectUsageCsvReader.readNext()) != null) {
			sequence = getListFromString(entries[0]);
			if (sequence.get(0) == this.loadID) {
				sequence = sequence.subList(1, sequence.size());
			}
			if (sequence.size() >= 2 && sequence.size() < 20) {	
				count = Integer.valueOf(entries[1]);
				sum += count;
				dataset.add(MutablePair.of(sequence, count));
			}
		}
		singleObjectUsageCsvReader.close();
		if (sum > 100000) return new ArrayList<>();
		return dataset;
	}

	public List<MutablePair<ImmutableList<Integer>, Integer>> loadDatasetForMultipleObjects(int multipleObjectsID) throws Exception {
		List<MutablePair<ImmutableList<Integer>, Integer>> dataset = new ArrayList<>();
		String fileName = Constants.MULTIPLE_OBJECTS_USAGE_DIR + File.separator + multipleObjectsID + ".csv";
		File file = new File(fileName);
		if (!file.exists()) return dataset;
		CSVReader multipleObjectsUsageCsvReader = new CSVReader(new FileReader(fileName), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
		String[] entries;
		ImmutableList<Integer> sequence;
		Integer count;
		int sum = 0;
		while ((entries = multipleObjectsUsageCsvReader.readNext()) != null) {
			sequence = getListFromString(entries[0]);
			if (sequence.get(0) == this.loadID) {
				sequence = sequence.subList(1, sequence.size());
			}
			if (sequence.size() >= 2 && sequence.size() < 20) {					
				count = Integer.valueOf(entries[1]);
				sum += count;
				dataset.add(MutablePair.of(sequence, count));
			}
		}
		multipleObjectsUsageCsvReader.close();
		if (sum > 100000) return new ArrayList<>();
		return dataset;
	}
	
	public static void main(String[] args) throws Exception {
		DatabaseLoader databaseLoader = new DatabaseLoader();
		List<MutablePair<ImmutableList<Integer>, Integer>> dataset = databaseLoader.loadDatasetForObject(742);
		
		for (MutablePair<ImmutableList<Integer>, Integer> pair: dataset) {
			System.out.println(pair);
		}
	}
}
