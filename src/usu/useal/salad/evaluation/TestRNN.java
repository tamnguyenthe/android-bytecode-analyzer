package usu.useal.salad.evaluation;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.lang3.tuple.Pair;

import be.ac.ulg.montefiore.run.jahmm.ObservationInteger;

public class TestRNN {
	private DatasetBuilder datasetBuilder;
	private List<Double> predicts;
	
	public TestRNN(DatasetBuilder datasetBuilder) {
		predicts = new ArrayList<>();
		this.datasetBuilder = datasetBuilder;
	}
	
	public void readTestResult(String fileName) throws Exception {
		predicts.clear();
		Scanner scanner = new Scanner(new FileReader(fileName));
		while (scanner.hasNext()) {
			predicts.add(scanner.nextDouble());
		}
		scanner.close();
	}


	public double[] produceFinalResult(List<List<ObservationInteger>> testSet) {
		int [] topHitTable = new int[10];
		double[] finalTopHitTable =  new double[10];
		try {
			readTestResult("test.score.txt");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Pair<Integer, Double>> rank;
		int baseIndex = 0;
		int numPredict = 0;

		for (List<ObservationInteger> seq: testSet) {
			for (int i = 0; i < seq.size(); i++) {
				rank = new ArrayList<>();
				for (int elem = 0; elem < datasetBuilder.getMethodIDs().size(); elem++) {
					rank.add(Pair.of(elem, predicts.get(baseIndex+elem)));
				}

				Collections.sort(rank, new Comparator<Pair<Integer, Double>>() {
					@Override
					public int compare(Pair<Integer, Double> o1,
							Pair<Integer, Double> o2) {
						return -Double.compare(o1.getRight(), o2.getRight());
					}	
				});

				for (int j = 0; j < rank.size(); j++) {
					if ((j < 10) && (seq.get(i).value == rank.get(j).getLeft().intValue())) {
						for (int k = j; k < topHitTable.length; k++) topHitTable[k]++;
						break;
					}
				}
				numPredict++;				
				baseIndex+=datasetBuilder.getMethodIDs().size();
			}
		}
//		System.out.println(baseIndex);
//		System.out.println(predicts.size());
		for (int i = 0; i < topHitTable.length; i++) {
			finalTopHitTable[i] = (double)topHitTable[i]/numPredict;
		}
		return finalTopHitTable;
	}
	
	public double[] produceFinalResultHoles(List<List<ObservationInteger>> testSet, List<Integer> holes) {
		int [] topHitTable = new int[10];
		double[] finalTopHitTable =  new double[10];
		try {
			readTestResult("test.hole.score.txt");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Pair<Integer, Double>> rank;
		int baseIndex = 0;
		int numPredict = 0;
		for (int i = 0; i < testSet.size(); i++) {
			rank = new ArrayList<>();
			for (int elem = 0; elem < datasetBuilder.getMethodIDs().size(); elem++) {
				rank.add(Pair.of(elem, predicts.get(baseIndex+elem)));
			}
			Collections.sort(rank, new Comparator<Pair<Integer, Double>>() {
				@Override
				public int compare(Pair<Integer, Double> o1,
						Pair<Integer, Double> o2) {
					return -Double.compare(o1.getRight(), o2.getRight());
				}	
			});
			for (int j = 0; j < rank.size(); j++) {
				if ((j < 10) && (testSet.get(i).get(holes.get(i)).value == rank.get(j).getLeft().intValue())) {
					for (int k = j; k < topHitTable.length; k++) topHitTable[k]++;
					break;
				}
			}
			numPredict++;
			baseIndex+=datasetBuilder.getMethodIDs().size();
		}
//		System.out.println(baseIndex);
//		System.out.println(predicts.size());
		for (int i = 0; i < topHitTable.length; i++) {
			finalTopHitTable[i] = (double)topHitTable[i]/numPredict;
		}
		return finalTopHitTable;
	}

}
