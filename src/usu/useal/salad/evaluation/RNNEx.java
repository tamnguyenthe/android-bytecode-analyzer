package usu.useal.salad.evaluation;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import com.google.common.base.Stopwatch;

import be.ac.ulg.montefiore.run.jahmm.ObservationInteger;

public class RNNEx extends Experiment {
	private List<List<ObservationInteger>> validationSet;
	private List<List<ObservationInteger>> trainingSet;

	public RNNEx(DatasetBuilder datasetBuilder) {
		super(datasetBuilder);
	}

	@Override
	protected void train(List<List<ObservationInteger>> fullTrainingSet) {
		writeTrainingSet(trainingSet);
		writeValidSet(validationSet);
		ProcessBuilder processBuilder = new ProcessBuilder("/bin/bash", "/home/useallab1/Documents/workspace/androidpm-hmm/rnntrain.sh");
		Process process;
		try {
			process = processBuilder.start();
			process.waitFor();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected List<Pair<Integer, Double>> predict(
			List<ObservationInteger> context, int currentPosition) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected double[] test(List<List<ObservationInteger>> testSet) {
		writeTestSetForRNNLM(testSet);
		ProcessBuilder processBuilder = new ProcessBuilder("/bin/bash", "/home/useallab1/Documents/workspace/androidpm-hmm/rnntest.sh");
		Process process;
		try {
			process = processBuilder.start();
			process.waitFor();
		} catch (Exception e) {
			e.printStackTrace();
		}
		TestRNN testRNN = new TestRNN(datasetBuilder);
		return testRNN.produceFinalResult(testSet);
	}
	
	@Override
	protected double[] testFill(List<List<ObservationInteger>> testSet, List<Integer> holes) {
		writeTestHoles(testSet, holes);
		ProcessBuilder processBuilder = new ProcessBuilder("/bin/bash", "/home/useallab1/Documents/workspace/androidpm-hmm/rnntesthole.sh");
		Process process;
		try {
			process = processBuilder.start();
			process.waitFor();
		} catch (Exception e) {
			e.printStackTrace();
		}
		TestRNN testRNN = new TestRNN(datasetBuilder);
		return testRNN.produceFinalResultHoles(testSet, holes);
	}

	private void writeTrainingSet(List<List<ObservationInteger>> trainingSet) {
		//save training set
		PrintWriter trainPrintWriter;
		try {
			trainPrintWriter = new PrintWriter("train.txt");
			for (List<ObservationInteger> sequence: trainingSet) {
				for (int i = 0; i < sequence.size()-1; i++) {
					trainPrintWriter.print(sequence.get(i).value + " ");
				}
				trainPrintWriter.println(sequence.get(sequence.size()-1));
			}
			trainPrintWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void writeValidSet(List<List<ObservationInteger>> validationSet) {
		PrintWriter validPrintWriter;
		try {
			validPrintWriter = new PrintWriter("valid.txt");
			for (List<ObservationInteger> sequence: validationSet) {
				for (int i = 0; i < sequence.size()-1; i++) {
					validPrintWriter.print(sequence.get(i).value + " ");
				}
				validPrintWriter.println(sequence.get(sequence.size()-1));
			}
			validPrintWriter.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private void writeTestSetForRNNLM(List<List<ObservationInteger>> testSet) {
		//save test set for rnnlm
		int nPredict = 0;
		List<ObservationInteger> predictSeq;
		PrintWriter testRNNPrintWriter;
		try {
			testRNNPrintWriter = new PrintWriter("test.rnnlm.txt");
			for (List<ObservationInteger> sequence: testSet) {
				for (int i = 0; i < sequence.size(); i++) {
					nPredict++;
					for (int elem = 0; elem < datasetBuilder.getMethodIDs().size(); elem++) {
						predictSeq = new ArrayList<>(sequence.subList(0, i));
						predictSeq.add(new ObservationInteger(elem));
						testRNNPrintWriter.print(nPredict);
						for (ObservationInteger oi: predictSeq) testRNNPrintWriter.print(" " + oi.value);
						testRNNPrintWriter.println();
					}
				}
			}
			testRNNPrintWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void writeTestHoles(List<List<ObservationInteger>> testSet, List<Integer> holes) {
		int nPredict = 0;
		List<ObservationInteger> predictSeq;
		PrintWriter testRNNPrintWriter;
		try {
			testRNNPrintWriter = new PrintWriter("test.hole.rnnlm.txt");
			for (int i = 0; i < testSet.size(); i++) {
				nPredict++;
				for (int elem = 0; elem < datasetBuilder.getMethodIDs().size(); elem++) {
					predictSeq = new ArrayList<>(testSet.get(i).subList(0, holes.get(i)));
					predictSeq.add(new ObservationInteger(elem));
					testRNNPrintWriter.print(nPredict);
					for (ObservationInteger oi: predictSeq) testRNNPrintWriter.print(" " + oi.value);
					testRNNPrintWriter.println();
				}
			}
			testRNNPrintWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

	@Override
	public void crossVal() {
		double[] topHitTable = new double[10];
		double[] topHitTableHole = new double[10];
		long trainTime = 0;
		long testTime = 0;
		long testTimeHole = 0;
		for (int i = 0; i < DatasetBuilder.NFOLDS; i++) {
			trainingSet = datasetBuilder.getTrainingSetForFold(i);
			validationSet = datasetBuilder.getValidationSetForFold(i);
			
			Stopwatch stopwatch = Stopwatch.createStarted();
			train(datasetBuilder.getFullTrainingSetForFold(i));
			stopwatch.stop();
			trainTime += stopwatch.elapsed(TimeUnit.MILLISECONDS);
			stopwatch = Stopwatch.createStarted();
			double[] topHitCrossVal = test(datasetBuilder.getTestingSetForFold(i));
			stopwatch.stop();
			testTime += stopwatch.elapsed(TimeUnit.MILLISECONDS);
			stopwatch = Stopwatch.createStarted();
			double[] topHitCrossValHole = testFill(datasetBuilder.getTestingSetForFold(i), datasetBuilder.getHoleIndices().get(i));
			stopwatch.stop();
			testTimeHole += stopwatch.elapsed(TimeUnit.MILLISECONDS);
			for (int j = 0; j < 10; j++) topHitTable[j]+=topHitCrossVal[j];
			for (int j = 0; j < 10; j++) topHitTableHole[j]+=topHitCrossValHole[j];
		}
//		System.out.println("RNN: ");
		for (int i = 0; i < topHitTable.length; i++) {
			topHitTable[i] = topHitTable[i] / DatasetBuilder.NFOLDS;
			topHitTableHole[i] = topHitTableHole[i] / DatasetBuilder.NFOLDS;
//			System.out.println(i + " : " + topHitTable[i]);
		}
		times[0] = trainTime / DatasetBuilder.NFOLDS;
		times[1] = testTime / DatasetBuilder.NFOLDS;
		times[2] = testTimeHole / DatasetBuilder.NFOLDS;
		BytecodeComparison.getResultCollector().getRnnlmTestResults().add(topHitTable);
		BytecodeComparison.getResultCollector().getRnnlmTestHoleResults().add(topHitTableHole);
		BytecodeComparison.getResultCollector().getRnnlmTimes().add(Triple.of(times[0], times[1], times[2]));
	}
}
