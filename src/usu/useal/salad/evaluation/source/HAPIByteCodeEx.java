package usu.useal.salad.evaluation.source;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableList;

import be.ac.ulg.montefiore.run.jahmm.Hmm;
import be.ac.ulg.montefiore.run.jahmm.ObservationInteger;
import be.ac.ulg.montefiore.run.jahmm.Opdf;
import be.ac.ulg.montefiore.run.jahmm.OpdfInteger;
import be.ac.ulg.montefiore.run.jahmm.OpdfIntegerFactory;
import be.ac.ulg.montefiore.run.jahmm.learn.BaumWelchLearner;
import be.ac.ulg.montefiore.run.jahmm.learn.KMeansLearner;
import usu.useal.salad.evaluation.DatasetBuilder;
import usu.useal.salad.evaluation.Experiment;

public class HAPIByteCodeEx extends Experiment {
	private static final double ALPHA = 0.001;
	private Hmm<ObservationInteger> finalHmm;
	List<MutablePair<ImmutableList<Integer>, Integer>> byteCodeDataset;
	private List<List<ObservationInteger>> convertedSet;
	private List<List<ObservationInteger>> validationSet;
	private List<List<ObservationInteger>> trainingSet;
	
	
	public HAPIByteCodeEx(DatasetBuilder datasetBuilder) {
		super(datasetBuilder);
		// TODO Auto-generated constructor stub
		
	}
	
	private int maxIndex(List<Double> seqs) {
		double max = Double.NEGATIVE_INFINITY;
		int maxIndex = -1;
		for (int i = 0; i < seqs.size(); i++) {
			if (max < seqs.get(i)) {
				max = seqs.get(i);
				maxIndex = i;
			}
		}
		return maxIndex;
	}
	
	private void convertByteCodeDataset() {
		List<ObservationInteger> newSequence;
		for (MutablePair<ImmutableList<Integer>, Integer> pair: byteCodeDataset) {
			newSequence = new ArrayList<>();
			boolean valid = true;
			for (Integer methodID: pair.getLeft()) {
				if (!datasetBuilder.getMethodIDs().containsKey(methodID)) {
					valid = false;
					break;
				}
				newSequence.add(new ObservationInteger(datasetBuilder.getMethodIDs().get(methodID)));
			}
			if (!valid) continue;
			if (newSequence.size() >= 2) {
				for (int i = 0; i < pair.getRight(); i++) {
					convertedSet.add(newSequence);
				}
			}
		}
	}
	
	/*
	private void train() {
//		convertedSet = new ArrayList<>();
//		convertByteCodeDataset();
		
//		for (List<ObservationInteger> sequence: convertedSet) {
//			System.out.println(sequence);
//		}
//		System.out.println();
//		for (List<ObservationInteger> sequence: datasetBuilder.getDataset()) {
//			System.out.println(sequence);
//		}
		convertedSet.addAll(datasetBuilder.getInitialTrainingSet());
		trainingSet = datasetBuilder.getValidationSetForFold(0);
		List<Double> lnLikelihoods = new ArrayList<>();
		int minNbStates = 2;
		int maxNbStates = 20;
		double lnLikelihood;
		for (int i = minNbStates; i <= maxNbStates; i++) {
			lnLikelihood = 0;
			KMeansLearner<ObservationInteger> kml = new KMeansLearner<ObservationInteger>(i, new OpdfIntegerFactory(datasetBuilder.getMethodIDs().size()), convertedSet);
			Hmm<ObservationInteger> hmm = kml.iterate();
			BaumWelchLearner bwl = new BaumWelchLearner();
			hmm = bwl.learn(hmm, convertedSet);		
			smoothHmm(hmm);
			for (List<ObservationInteger> seq: trainingSet) {
				lnLikelihood += hmm.lnProbability(seq);
			}
			lnLikelihoods.add(lnLikelihood);
		}
		int bestNbStates = minNbStates + maxIndex(lnLikelihoods);
		KMeansLearner<ObservationInteger> kml = new KMeansLearner<ObservationInteger>(bestNbStates, new OpdfIntegerFactory(datasetBuilder.getMethodIDs().size()), convertedSet);
		Hmm<ObservationInteger> hmm = kml.iterate();
		BaumWelchLearner bwl = new BaumWelchLearner();
		Stopwatch stopwatch = Stopwatch.createStarted();
		hmm = bwl.learn(hmm, convertedSet);
		stopwatch.stop();
		times[0] = stopwatch.elapsed(TimeUnit.MILLISECONDS);
		smoothHmm(hmm);
		finalHmm = hmm;
	}
	*/
	
	public void smoothHmm(Hmm<ObservationInteger> hmm) {
		double prob;
		int nbStates = hmm.nbStates();
		int nbObservations = datasetBuilder.getMethodIDs().size();
		for (int i = 0; i < nbStates; i++) {
			prob = hmm.getPi(i);
			hmm.setPi(i, (prob + ALPHA)/(1. + nbStates*ALPHA));
		}

		for (int i = 0; i < nbStates; i++) {
			for (int j = 0; j < nbStates; j++) {
				prob = hmm.getAij(i, j);
				hmm.setAij(i, j, (prob + ALPHA)/(1. + nbStates*ALPHA));
			}
		}

		Opdf<ObservationInteger> opdf;
		double[] probs;
		for (int i = 0; i < nbStates; i++) {
			opdf = hmm.getOpdf(i);
			probs = new double[nbObservations];
			for (int j = 0; j < nbObservations; j++) {
				prob = opdf.probability(new ObservationInteger(j));
				probs[j] = (prob + ALPHA)/(1. + nbObservations*ALPHA);
			}
			hmm.setOpdf(i, new OpdfInteger(probs));
		}
	}


	@Override
	protected void train(List<List<ObservationInteger>> fullTrainingSet) {
		List<List<ObservationInteger>> newTrainingSet = new ArrayList<>();
		newTrainingSet.addAll(convertedSet);
		newTrainingSet.addAll(datasetBuilder.getInitialTrainingSet());
		List<Double> lnLikelihoods = new ArrayList<>();
		int minNbStates = 2;
		int maxNbStates = 20;
		double lnLikelihood;
		for (int i = minNbStates; i <= maxNbStates; i++) {
			lnLikelihood = 0;
			KMeansLearner<ObservationInteger> kml = new KMeansLearner<ObservationInteger>(i, new OpdfIntegerFactory(datasetBuilder.getMethodIDs().size()), newTrainingSet);
			Hmm<ObservationInteger> hmm = kml.iterate();
			BaumWelchLearner bwl = new BaumWelchLearner();
			hmm = bwl.learn(hmm, newTrainingSet);		
			smoothHmm(hmm);
			for (List<ObservationInteger> seq: validationSet) {
				lnLikelihood += hmm.lnProbability(seq);
			}
			lnLikelihoods.add(lnLikelihood);
		}
		int bestNbStates = minNbStates + maxIndex(lnLikelihoods);
		KMeansLearner<ObservationInteger> kml = new KMeansLearner<ObservationInteger>(bestNbStates, new OpdfIntegerFactory(datasetBuilder.getMethodIDs().size()), newTrainingSet);
		Hmm<ObservationInteger> hmm = kml.iterate();
		BaumWelchLearner bwl = new BaumWelchLearner();
		Stopwatch stopwatch = Stopwatch.createStarted();
		hmm = bwl.learn(hmm, newTrainingSet);
		stopwatch.stop();
		times[0] = stopwatch.elapsed(TimeUnit.MILLISECONDS);
		smoothHmm(hmm);
		finalHmm = hmm;
	}

	@Override
	protected List<Pair<Integer, Double>> predict(List<ObservationInteger> context, int currentPosition) {
		List<Pair<Integer, Double>> result = new ArrayList<>();
		List<ObservationInteger> predictSeq;
		for (int elem = 0; elem < datasetBuilder.getMethodIDs().size(); elem++) {
			predictSeq = new ArrayList<>(context.subList(0, currentPosition));
			predictSeq.add(new ObservationInteger(elem));
			result.add(Pair.of(elem, finalHmm.lnProbability(predictSeq)));
		}
		
		//sort results
		Collections.shuffle(result);
		Collections.sort(result, new Comparator<Pair<Integer, Double>>() {
			@Override
			public int compare(Pair<Integer, Double> o1,
					Pair<Integer, Double> o2) {
				return -Double.compare(o1.getRight(), o2.getRight());
			}	
		});
		return result;
	}

	@Override
	protected double[] test(List<List<ObservationInteger>> testSet) {
		double[] topHitTable = new double[10];
		int nPredicts = 0;
		for (List<ObservationInteger> testSequence: testSet) {
			for (int i = 0; i < testSequence.size(); i++) {
				nPredicts++;
				List<Pair<Integer, Double>> predictionRanking = predict(testSequence, i);
				for (int index = 0; index < predictionRanking.size(); index++) {
					if ((index < 10) && (predictionRanking.get(index).getLeft() == testSequence.get(i).value)) {
						for (int j = index; j < 10; j++) topHitTable[j] = topHitTable[j] + 1;
						break;
					}
				}
			}
		}
		for (int i = 0; i < topHitTable.length; i++) topHitTable[i] = topHitTable[i] / nPredicts;
		return topHitTable;
	}
	
	@Override
	protected double[] testFill(List<List<ObservationInteger>> testSet, List<Integer> holes) {
		double[] topHitTable = new double[10];
		for (int i = 0; i < testSet.size(); i++) {
			List<Pair<Integer, Double>> predictionRanking = predict(testSet.get(i), holes.get(i));
			for (int index = 0; index < predictionRanking.size(); index++) {
				if ((index < 10) && (predictionRanking.get(index).getLeft() == testSet.get(i).get(holes.get(i)).value)) {
					for (int j = index; j < 10; j++) topHitTable[j] = topHitTable[j] + 1;
					break;
				}
			}
		}
		for (int i = 0; i < topHitTable.length; i++) topHitTable[i] = topHitTable[i] / testSet.size();
		return topHitTable;
	}

	@Override
	public void crossVal() {
		double[] topHitTable = new double[10];
		double[] topHitTableHole = new double[10];
		long testTime = 0;
		long testTimeHole = 0;
		for (int i = 0; i < DatasetBuilder.NFOLDS; i++) {
			validationSet = datasetBuilder.getValidationSetForFold(i);
			trainingSet = datasetBuilder.getTrainingSetForFold(i);
			train(datasetBuilder.getFullTrainingSetForFold(i));
			Stopwatch stopwatch = Stopwatch.createStarted();
			double[] topHitCrossVal = test(datasetBuilder.getTestingSetForFold(i));
			stopwatch.stop();
			testTime += stopwatch.elapsed(TimeUnit.MILLISECONDS);
			stopwatch = Stopwatch.createStarted();
			double[] topHitCrossValHole = testFill(datasetBuilder.getTestingSetForFold(i), datasetBuilder.getHoleIndices().get(i));
			stopwatch.stop();
			testTimeHole += stopwatch.elapsed(TimeUnit.MILLISECONDS);
			for (int j = 0; j < 10; j++) topHitTable[j]+=topHitCrossVal[j];
			for (int j = 0; j < 10; j++) topHitTableHole[j]+=topHitCrossValHole[j];
		}
//		System.out.println("HAPI Bytecode: ");
		for (int i = 0; i < topHitTable.length; i++) {
			topHitTable[i] = topHitTable[i] / DatasetBuilder.NFOLDS;
			topHitTableHole[i] = topHitTableHole[i] / DatasetBuilder.NFOLDS;
//			System.out.println(i + " : " + topHitTable[i]);
		}
		times[1] = testTime / DatasetBuilder.NFOLDS;
		times[2] = testTimeHole / DatasetBuilder.NFOLDS;
		SourceCodeComparison.getResultCollector().getHapiTestResults().add(topHitTable);
		SourceCodeComparison.getResultCollector().getHapiTestHoleResults().add(topHitTableHole);
//		SourceCodeComparison.getResultCollector().getHapiTimes().add(Triple.of(times[0], times[1], times[2]));
	}

	public void setByteCodeDataset(List<MutablePair<ImmutableList<Integer>, Integer>> byteCodeDataset) {
		this.byteCodeDataset = byteCodeDataset;
		convertedSet = new ArrayList<>();
		convertByteCodeDataset();
	}
}
