package usu.useal.salad.evaluation.source;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.MutablePair;

import com.google.common.collect.ImmutableList;

import usu.useal.salad.evaluation.DatabaseLoader;
import usu.useal.salad.evaluation.DatasetBuilder;
import usu.useal.salad.evaluation.ResultCollector;

public class SourceCodeComparison {
	private DatabaseLoader databaseLoader;
	private DatabaseLoader2 databaseLoader2;
	private static ResultCollector resultCollector;
	
	public static ResultCollector getResultCollector() {
		if (resultCollector == null) resultCollector = new ResultCollector();
		return resultCollector;
	}

	public SourceCodeComparison() {
		try {
			databaseLoader = new DatabaseLoader();
			databaseLoader2 = new DatabaseLoader2();
		} catch (Exception e) {
			System.out.println("there is a problem when creating the database loader");
			e.printStackTrace();
		}
		File singleFolder = new File("Single");
		if (!singleFolder.exists()) singleFolder.mkdirs();
		try {
			FileUtils.cleanDirectory(singleFolder);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		File setFolder = new File("Set");
		if (!setFolder.exists()) setFolder.mkdirs();
		try {
			FileUtils.cleanDirectory(setFolder);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		File resultsFolder = new File("Results");
		if (!resultsFolder.exists()) resultsFolder.mkdirs();
		try {
			FileUtils.cleanDirectory(resultsFolder);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void testObject(int objectID) {
		DatasetBuilder datasetBuilder = null;
		List<MutablePair<ImmutableList<Integer>, Integer>> newDataset = null;
		List<MutablePair<ImmutableList<Integer>, Integer>> byteCodeDataset = null;
		try {
			// preprocess bytecode dataset
			Map<ImmutableList<Integer>, Integer> sequenceBiMap = new HashMap<>();
			for (MutablePair<ImmutableList<Integer>, Integer> pair: databaseLoader2.loadDatasetForObject(objectID)) {
//				System.out.println(pair);
				sequenceBiMap.put(pair.getLeft(), pair.getRight());
			}
			byteCodeDataset = databaseLoader.loadDatasetForObject(objectID);
			for (MutablePair<ImmutableList<Integer>, Integer> pair: byteCodeDataset) {
				if (sequenceBiMap.containsKey(pair.getLeft())) 
					sequenceBiMap.put(pair.getLeft(), pair.getRight());
			}
			newDataset = new ArrayList<>();
			for (Map.Entry<ImmutableList<Integer>, Integer> entry: sequenceBiMap.entrySet()) {
				newDataset.add(MutablePair.of(entry.getKey(), entry.getValue()));
			}
			
			// load source code dataset
			datasetBuilder = new DatasetBuilder(databaseLoader2.loadDatasetForObject(objectID), true, objectID);
			//datasetBuilder = new DatasetBuilder(newDataset, true, objectID);
//			
//			System.out.println("--------------------------------------------");
//			for (MutablePair<ImmutableList<Integer>, Integer> pair: byteCodeDataset) {
//				System.out.println(pair);
//			}
			
			
		} catch (Exception e) {
			System.out.println("There is a problem when loading dataset for object " + objectID);
			e.printStackTrace();
		}
		if (datasetBuilder.isReadyForEvaluation() && newDataset.size() > 0) {
			HAPIByteCodeEx hapiByteCodeEx = new HAPIByteCodeEx(datasetBuilder);
			hapiByteCodeEx.setByteCodeDataset(newDataset);
			hapiByteCodeEx.crossVal();
			HAPISourceEx hapiEx = new HAPISourceEx(datasetBuilder);
			hapiEx.crossVal();
		}

	}


	public void testObjectSet(int objectSetID) {
		DatasetBuilder datasetBuilder = null;
		try {
			datasetBuilder = new DatasetBuilder(databaseLoader.loadDatasetForMultipleObjects(objectSetID), false, objectSetID);
		} catch (Exception e) {
			System.out.println("There is a problem when loading dataset for object set " + objectSetID);
			e.printStackTrace();
		}
		if (datasetBuilder.isReadyForEvaluation()) {
			HAPISourceEx hapiEx = new HAPISourceEx(datasetBuilder);
			hapiEx.crossVal();
			NGRAMSourceEx ngramEx = new NGRAMSourceEx(datasetBuilder);
			ngramEx.crossVal();
		}
	}

	public static void main(String[] args) {
		SourceCodeComparison sourceCodeComparison = new SourceCodeComparison();
		int i = 0;
		for (Map.Entry<Integer, String> entry: sourceCodeComparison.databaseLoader2.getObjectsBiMap().entrySet()) {
			System.out.println(i++);
			System.out.println(entry);
			sourceCodeComparison.testObject(entry.getKey());
			if (i % 100 == 0) {
				getResultCollector().writeResults();
			}
		}
//		sourceCodeComparison.testObject(1590);
		getResultCollector().writeResults();
	}
}
