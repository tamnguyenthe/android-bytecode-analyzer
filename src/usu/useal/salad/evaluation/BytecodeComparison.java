package usu.useal.salad.evaluation;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.commons.io.FileUtils;

public class BytecodeComparison {
	private DatabaseLoader databaseLoader;
	private static ResultCollector resultCollector;
	
	public static ResultCollector getResultCollector() {
		if (resultCollector == null) resultCollector = new ResultCollector();
		return resultCollector;
	}

	public BytecodeComparison() {
		try {
			databaseLoader = new DatabaseLoader();
		} catch (Exception e) {
			System.out.println("there is a problem when creating the database loader");
			e.printStackTrace();
		}
		File singleFolder = new File("Single");
		if (!singleFolder.exists()) singleFolder.mkdirs();
		try {
			FileUtils.cleanDirectory(singleFolder);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		File setFolder = new File("Set");
		if (!setFolder.exists()) setFolder.mkdirs();
		try {
			FileUtils.cleanDirectory(setFolder);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		File resultsFolder = new File("Results");
		if (!resultsFolder.exists()) resultsFolder.mkdirs();
		try {
			FileUtils.cleanDirectory(resultsFolder);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void testObject(int objectID) {
		DatasetBuilder datasetBuilder = null;
		try {
			datasetBuilder = new DatasetBuilder(databaseLoader.loadDatasetForObject(objectID), true, objectID);
		} catch (Exception e) {
			System.out.println("There is a problem when loading dataset for object " + objectID);
			e.printStackTrace();
		}
		if (datasetBuilder.isReadyForEvaluation()) {
			HAPIEx hapiEx = new HAPIEx(datasetBuilder);
			hapiEx.crossVal();
			NGramEx ngramEx = new NGramEx(datasetBuilder);
			ngramEx.crossVal();
//			RNNEx rnnEx = new RNNEx(datasetBuilder);
//			rnnEx.crossVal();
		}

	}


	public void testObjectSet(int objectSetID) {
		DatasetBuilder datasetBuilder = null;
		try {
			datasetBuilder = new DatasetBuilder(databaseLoader.loadDatasetForMultipleObjects(objectSetID), false, objectSetID);
		} catch (Exception e) {
			System.out.println("There is a problem when loading dataset for object set " + objectSetID);
			e.printStackTrace();
		}
		if (datasetBuilder.isReadyForEvaluation()) {
			HAPIEx hapiEx = new HAPIEx(datasetBuilder);
			hapiEx.crossVal();
			NGramEx ngramEx = new NGramEx(datasetBuilder);
			ngramEx.crossVal();
			RNNEx rnnEx = new RNNEx(datasetBuilder);
			rnnEx.crossVal();
		}
	}

	public static void main(String[] args) {
		BytecodeComparison bytecodeComparison = new BytecodeComparison();
//		for (Map.Entry<Integer, String> entry: bytecodeComparison.databaseLoader.getObjectsBiMap().entrySet()) {
//			System.out.println(entry);
//			bytecodeComparison.testObject(entry.getKey());
//		}
		bytecodeComparison.testObject(1490);
		getResultCollector().writeResults();
	}
}
