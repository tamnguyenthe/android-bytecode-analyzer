package usu.useal.salad.evaluation;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.Triple;

import com.opencsv.CSVWriter;

public class ResultCollector {
	private List<double[]> hapiTestResults;
	private List<double[]> ngramTestResults;
	private List<double[]> rnnlmTestResults;
	private List<double[]> hapiTestHoleResults;
	private List<double[]> ngramTestHoleResults;
	private List<double[]> rnnlmTestHoleResults;
	private List<Triple<Long, Long, Long>> hapiTimes;
	private List<Triple<Long, Long, Long>> ngramTimes;
	private List<Triple<Long, Long, Long>> rnnlmTimes;
	
	private List<double[]> hapiSourceTestResults;
	private List<double[]> hapiSourceTestResultsHole;
	
	private void writeTestResults(List<double[]> testResults, String fileName) {
		try {
			String[] entries = new String[testResults.get(0).length];
			CSVWriter csvWriter = new CSVWriter(new FileWriter(fileName));
			for (double[] testResult: testResults) {
				for (int i = 0; i < testResult.length;i++) entries[i] = Double.toString(testResult[i]);
				csvWriter.writeNext(entries);
			}
			csvWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void writeTimeResults(List<Triple<Long, Long, Long>> timeResults, String fileName) {
		try {
			String[] entries = new String[3];
			CSVWriter csvWriter = new CSVWriter(new FileWriter(fileName));
			for (Triple<Long, Long, Long> timeResult: timeResults) {
				entries[0] = timeResult.getLeft().toString();
				entries[1] = timeResult.getMiddle().toString();
				entries[2] = timeResult.getRight().toString();
				csvWriter.writeNext(entries);
			}
			csvWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public ResultCollector() {
		hapiTestResults = new ArrayList<>();
		ngramTestResults = new ArrayList<>();
		rnnlmTestResults = new ArrayList<>();
		hapiTestHoleResults = new ArrayList<>();
		ngramTestHoleResults = new ArrayList<>();
		rnnlmTestHoleResults = new ArrayList<>();
		hapiTimes = new ArrayList<>();
		ngramTimes = new ArrayList<>();
		rnnlmTimes = new ArrayList<>();
		
		hapiSourceTestResults = new ArrayList<>();
		hapiSourceTestResultsHole = new ArrayList<>();
	}

	public List<double[]> getHapiTestResults() {
		return hapiTestResults;
	}

	public List<double[]> getNgramTestResults() {
		return ngramTestResults;
	}

	public List<double[]> getRnnlmTestResults() {
		return rnnlmTestResults;
	}

	public List<double[]> getHapiTestHoleResults() {
		return hapiTestHoleResults;
	}

	public List<double[]> getNgramTestHoleResults() {
		return ngramTestHoleResults;
	}

	public List<double[]> getRnnlmTestHoleResults() {
		return rnnlmTestHoleResults;
	}

	public List<Triple<Long, Long, Long>> getHapiTimes() {
		return hapiTimes;
	}

	public List<Triple<Long, Long, Long>> getNgramTimes() {
		return ngramTimes;
	}

	public List<Triple<Long, Long, Long>> getRnnlmTimes() {
		return rnnlmTimes;
	}
	
	public void writeResults() {
		File resultsFolder = new File("Results");
		if (!resultsFolder.exists()) resultsFolder.mkdirs();
		try {
			FileUtils.cleanDirectory(resultsFolder);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		writeTestResults(hapiTestResults, "Results" + File.separator + "HAPITestResults.csv");
//		writeTestResults(ngramTestResults, "Results" + File.separator + "NGRAMTestResults.csv");
//		writeTestResults(rnnlmTestResults, "Results" + File.separator + "RNNTestResults.csv");
		writeTestResults(hapiTestHoleResults, "Results" + File.separator + "HOLE.HAPITestResults.csv");
//		writeTestResults(ngramTestHoleResults, "Results" + File.separator + "HOLE.NGRAMTestResults.csv");
//		writeTestResults(rnnlmTestHoleResults, "Results" + File.separator + "HOLE.RNNTestResults.csv");
//		writeTimeResults(hapiTimes,"Results" + File.separator +  "HAPITimes.csv");
//		writeTimeResults(ngramTimes,"Results" + File.separator +  "NGRAMTimes.csv");
//		writeTimeResults(rnnlmTimes, "Results" + File.separator + "RNNTimes.csv");
		writeTestResults(hapiSourceTestResults, "Results" + File.separator + "SOURCE.HAPITestResults.csv");
		writeTestResults(hapiSourceTestResultsHole, "Results" + File.separator + "SOURCE.HOLE.HAPITestResults.csv");
	}

	public List<double[]> getHapiSourceTestResults() {
		return hapiSourceTestResults;
	}

	public List<double[]> getHapiSourceTestResultsHole() {
		return hapiSourceTestResultsHole;
	}
}
