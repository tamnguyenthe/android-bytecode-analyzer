package usu.useal.salad.evaluation;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import be.ac.ulg.montefiore.run.jahmm.ObservationInteger;

public abstract class Experiment {
	protected DatasetBuilder datasetBuilder;
	protected long[] times;
	public Experiment(DatasetBuilder datasetBuilder) {
		this.datasetBuilder = datasetBuilder;
		times = new long[3];
	}
	protected abstract void train(List<List<ObservationInteger>> trainingSet);
	protected abstract List<Pair<Integer, Double>> predict(List<ObservationInteger> context, int currentPosition);
	protected abstract double[] test(List<List<ObservationInteger>> testSet);
	protected abstract double[] testFill(List<List<ObservationInteger>> testSet, List<Integer> holes);
	public abstract void crossVal();
}
