package usu.useal.salad.evaluation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.tuple.Pair;

public class CrossValidation {
	public static final long DEFAULT_SEED = 7;
	public static final double VALIDATION_PORTION = 0.125;
	
	public static List<Pair<List<Integer>, List<Integer>>> kFold(int n, int nFolds) {
		//create an array to hold indices
		List<Integer> indices = new ArrayList<>();
		for (int i = 0; i < n; i++) indices.add(i);
		Random random = new Random(DEFAULT_SEED);
		//shuffle the indices two times (just for fun)
		Collections.shuffle(indices, random);
		Collections.shuffle(indices, random);
		//pre-compute some values
		int foldLength = n / nFolds;
		List<Pair<List<Integer>, List<Integer>>> result = new ArrayList<>();
		//generate folds
		for (int k = 0; k < nFolds; k++) {
			List<Integer> trainingPart = new ArrayList<>();
			List<Integer> testingPart = new ArrayList<>();
			if (k < nFolds - 1) {
				for (int i = 0; i < k * foldLength; i++) trainingPart.add(indices.get(i));
				for (int i = k*foldLength; i < (k+1)*foldLength; i++) testingPart.add(indices.get(i));
				for (int i = (k+1) * foldLength; i < n; i++) trainingPart.add(indices.get(i));
				result.add(Pair.of (trainingPart, testingPart));
			} else {
				for (int i = 0; i < k * foldLength; i++) trainingPart.add(indices.get(i));
				for (int i = k*foldLength; i < n; i++) testingPart.add(indices.get(i));
				result.add(Pair.of(trainingPart, testingPart));
			}
		}
		return result;
	}
	
	public static Pair<List<Integer>, List<Integer>> splitTrainingAndValidSet(List<Integer> trainingIndices) {
		//copy to a temporary indices array and shuffle that
		List<Integer> tempIndices = new ArrayList<>(trainingIndices);
		Random random = new Random(DEFAULT_SEED);
		Collections.shuffle(tempIndices, random);
		//split training and validation sets
		List<Integer> trainingPart = new ArrayList<>();
		List<Integer> validationPart = new ArrayList<>();
		int trainingSize = trainingIndices.size();
		long threshold = Math.round( trainingSize * VALIDATION_PORTION);
		for (int i = 0; i < threshold; i++) validationPart.add(tempIndices.get(i));
		for (int i = (int)threshold; i < trainingSize; i++) trainingPart.add(tempIndices.get(i));
		return Pair.of(trainingPart, validationPart);
	}
	
	public static void main(String[] args) {
		System.out.println(CrossValidation.kFold(17, 4));
		System.out.println(CrossValidation.splitTrainingAndValidSet(CrossValidation.kFold(17, 5).get(0).getLeft()));
	}
	
	
}
