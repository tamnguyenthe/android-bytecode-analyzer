package usu.useal.salad.evaluation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.xml.crypto.dsig.spec.TransformParameterSpec;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableList;

import be.ac.ulg.montefiore.run.jahmm.ObservationInteger;

public class NGramEx extends Experiment {
	
	private Map<ImmutableList<Integer>, Map<Integer, Double>> nGramModel;
	private Map<ImmutableList<Integer>, Integer> triGramMap;
	private Map<ImmutableList<Integer>, Integer> duoGramMap;
	
	public NGramEx(DatasetBuilder datasetBuilder) {
		super(datasetBuilder);
	}

	@Override
	protected void train(List<List<ObservationInteger>> trainingSet) {
		triGramMap = new HashMap<>();
		duoGramMap = new HashMap<>();
		nGramModel = new HashMap<>();
		int twoPrev, onePrev, it;
		for (List<ObservationInteger> sequence: trainingSet) {
			for (int i = 1; i < sequence.size(); i++) {
				if (i == 0) {
					twoPrev = -1;
					onePrev = -1;
				} else if (i == 1) {
					twoPrev = -1;
					onePrev = sequence.get(i-1).value;
				} else {
					twoPrev = sequence.get(i-2).value;
					onePrev= sequence.get(i-1).value;
				}
				it = sequence.get(i).value;
				//collect 3-gram
				ImmutableList<Integer> triGram = ImmutableList.of(twoPrev, onePrev, it);				
				Integer trigramCount = triGramMap.get(triGram);
				if (trigramCount == null) triGramMap.put(triGram, 1);
				else triGramMap.put(triGram, trigramCount+1);

				//collect 2-gram
				ImmutableList<Integer> duoGram = ImmutableList.of(onePrev, it);
				Integer duoGramCount = duoGramMap.get(duoGram);
				if (duoGramCount == null) duoGramMap.put(duoGram, 1);
				else duoGramMap.put(duoGram, duoGramCount+1);
				if (i == 1) {
					duoGram = ImmutableList.of(twoPrev, onePrev);
					duoGramCount = duoGramMap.get(duoGram);
					if (duoGramCount == null) duoGramMap.put(duoGram, 1);
					else duoGramMap.put(duoGram, duoGramCount+1);
				}
			}
		}
		for (ImmutableList<Integer> triGram: triGramMap.keySet()) {
			ImmutableList<Integer> duoGram = ImmutableList.copyOf(triGram.subList(0, triGram.size()-1));
			if (!nGramModel.containsKey(duoGram)) nGramModel.put(duoGram, new HashMap<Integer, Double>());
			int triGramCount = triGramMap.get(triGram);
			int duoGramCount = duoGramMap.get(duoGram);
			nGramModel.get(duoGram).put(triGram.get(triGram.size()-1),(double)triGramCount/duoGramCount);
		}
	}

	@Override
	protected List<Pair<Integer, Double>> predict(List<ObservationInteger> context, int currentPosition) {
		List<Pair<Integer, Double>> result = new ArrayList<>();
		int twoPrev, onePrev, it;
		if (currentPosition == 0) {
			twoPrev = -1;
			onePrev = -1;
		} else if (currentPosition == 1) {
			twoPrev = -1;
			onePrev = context.get(currentPosition-1).value;
		} else {
			twoPrev = context.get(currentPosition-2).value;
			onePrev= context.get(currentPosition-1).value;
		}
		ImmutableList<Integer> duoGram = ImmutableList.of(twoPrev, onePrev);
		Map<Integer, Double> pMap = nGramModel.get(duoGram);
		for (int elem = 0; elem < datasetBuilder.getMethodIDs().size(); elem++) {
			it = elem;
			if (pMap == null) result.add(Pair.of(it, -0.1));
			else {
				if (pMap.containsKey(it)) result.add(Pair.of(it, pMap.get(it)));
				else result.add(Pair.of(it, -0.1));
			}
		}
		Collections.shuffle(result);
		Collections.sort(result, new Comparator<Pair<Integer, Double>>() {
			@Override
			public int compare(Pair<Integer, Double> o1,
					Pair<Integer, Double> o2) {
				return -Double.compare(o1.getRight(), o2.getRight());
			}	
		});
		return result;
	}

	@Override
	protected double[] test(List<List<ObservationInteger>> testSet) {
		double[] topHitTable = new double[10];
		int nPredicts = 0;
		for (List<ObservationInteger> testSequence: testSet) {
//			nPredicts++; //this is a penalty because this method cannot suggest the first location
			for (int i = 0; i < testSequence.size(); i++) {
				nPredicts++;
				List<Pair<Integer, Double>> predictionRanking = predict(testSequence, i);
				for (int index = 0; index < predictionRanking.size(); index++) {
					if ((index < 10) && (predictionRanking.get(index).getLeft() == testSequence.get(i).value)) {
						for (int j = index; j < 10; j++) topHitTable[j] = topHitTable[j] + 1;
						break;
					}
				}
			}
		}
		for (int i = 0; i < topHitTable.length; i++) topHitTable[i] = topHitTable[i] / nPredicts;
	
		return topHitTable;
	}

	@Override
	protected double[] testFill(List<List<ObservationInteger>> testSet, List<Integer> holes) {
		double[] topHitTable = new double[10];
		for (int i = 0; i < testSet.size(); i++) {
			List<Pair<Integer, Double>> predictionRanking = predict(testSet.get(i), holes.get(i));
			for (int index = 0; index < predictionRanking.size(); index++) {
				if ((index < 10) && (predictionRanking.get(index).getLeft() == testSet.get(i).get(holes.get(i)).value)) {
					for (int j = index; j < 10; j++) topHitTable[j] = topHitTable[j] + 1;
					break;
				}
			}
		}
		for (int i = 0; i < topHitTable.length; i++) topHitTable[i] = topHitTable[i] / testSet.size();
		return topHitTable;
	}
	
	
	@Override
	public void crossVal() {
		double[] topHitTable = new double[10];
		double[] topHitTableHole = new double[10];
		long trainTime = 0;
		long testTime = 0;
		long testTimeHole = 0;
		for (int i = 0; i < DatasetBuilder.NFOLDS; i++) {
			Stopwatch stopwatch = Stopwatch.createStarted();
			train(datasetBuilder.getFullTrainingSetForFold(i));
			stopwatch.stop();
			trainTime += stopwatch.elapsed(TimeUnit.MILLISECONDS);
			stopwatch = Stopwatch.createStarted();
			double[] topHitCrossVal = test(datasetBuilder.getTestingSetForFold(i));
			stopwatch.stop();
			testTime += stopwatch.elapsed(TimeUnit.MILLISECONDS);
			stopwatch = Stopwatch.createStarted();
			double[] topHitCrossValHole = testFill(datasetBuilder.getTestingSetForFold(i), datasetBuilder.getHoleIndices().get(i));
			stopwatch.stop();
			testTimeHole += stopwatch.elapsed(TimeUnit.MILLISECONDS);
			for (int j = 0; j < 10; j++) topHitTable[j]+=topHitCrossVal[j];
			for (int j = 0; j < 10; j++) topHitTableHole[j]+=topHitCrossValHole[j];
		}
		System.out.println("NGRAM: ");
		for (int i = 0; i < topHitTable.length; i++) {
			topHitTable[i] = topHitTable[i] / DatasetBuilder.NFOLDS;
			topHitTableHole[i] = topHitTableHole[i] / DatasetBuilder.NFOLDS;
			System.out.println(i + " : " + topHitTable[i]);
		}
		times[0] = trainTime / DatasetBuilder.NFOLDS;
		times[1] = testTime / DatasetBuilder.NFOLDS;
		times[2] = testTimeHole / DatasetBuilder.NFOLDS;
		BytecodeComparison.getResultCollector().getNgramTestResults().add(topHitTable);
		BytecodeComparison.getResultCollector().getNgramTestHoleResults().add(topHitTableHole);
		BytecodeComparison.getResultCollector().getNgramTimes().add(Triple.of(times[0], times[1], times[2]));
	}
}
